﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-08-10
 * Class     : CST-227 Enterprise Computer Promgramming II
 * Professor : James Shinevar
 * Assignment: Milestone 1
 * ---------------------------------------------------------------
 * Description:
 * Create classes to represent Cells and Game Board for the 
 * Mine Sweeper game.
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Milestone01
{
    class Program
    {
        static void Main(string[] args)
        {
            // Note: For testing - no entry validation
            Console.Out.Write("Enter size of board (10-20): ");
            string size = Console.ReadLine();

            Console.Out.Write("Enter game difficulty percentage (1-100): ");
            string difficulty = Console.ReadLine();

            Console.Out.WriteLine();

            Board board = new Board(int.Parse(size));
            board.Difficulty = int.Parse(difficulty);
            board.setupLiveNeighbors();
            board.calculateLiveNeighbors();
            printBoard(board);

            Console.Out.Write("\nPress Enter to Exit.");
            Console.ReadLine();
        }

        // Helper method to display game board
        static void printBoard(Board board)
        {
            // Print Column Header
            Console.Out.Write("+");
            for (int col = 0; col < board.Size; col++)
                Console.Out.Write(" {0,-2}+", col);
            Console.Out.WriteLine();

            // Print Top Separation Line
            Console.Out.Write("+");
            for (int col = 0; col < board.Size; col++)
                Console.Out.Write("---+");
            Console.Out.WriteLine();

            // Double Loop to print board contents
            for (int row = 0; row < board.Size; row++)
            {
                Console.Out.Write("|");
                for (int col = 0; col < board.Size; col++)
                {
                    if (board.grid[row,col].Live)
                        Console.Out.Write(" * |"); // Live bombs display an asterisk
                    else
                        Console.Out.Write(" {0} |", board.grid[row, col].Neighbors);
                }

                // Print Row Header
                Console.Out.WriteLine(" {0}", row);

                // Print Body Separation Lines
                Console.Out.Write("+");
                for (int col = 0; col < board.Size; col++)
                    Console.Out.Write("---+");
                Console.Out.WriteLine();
            }
        }
    }
}
