﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-08-10
 * Class     : CST-227 Enterprise Computer Promgramming II
 * Professor : James Shinevar
 * Assignment: Milestone 1
 * ---------------------------------------------------------------
 * Description:
 * Create classes to represent Cells and Game Board for the 
 * Mine Sweeper game. 
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Milestone01
{
    public class Board
    {
        private Random random;

        public Cell[,] grid;
        public int Size { get; set; } // Size of square grid
        public int Difficulty { get; set; } // Percentage (0-100) of live bombs

        // Default constructor - chain to next constructor
        // Default size = 10
        public Board() : this(10)
        {
        }

        public Board(int size)
        {
            Size = size;
            grid = new Cell[size, size];
            for (int row = 0; row < Size; row++)
                for (int col = 0; col < Size; col++)
                {
                    grid[row, col] = new Cell(row, col);
                }
 
            Difficulty = 10; // Default to 10 percent

            // Create random number generator - this method of seeding for variety
            // is the only method on my system that worked appropriately.
            random = new Random((int)Guid.NewGuid().GetHashCode());
        }

        public void setupLiveNeighbors()
        {
            int cellCount = Size * Size;
            int bombCount = (int) (cellCount * (Difficulty / 100.0));
            for (int i = 0; i < bombCount; i++)
            {
                int x = random.Next(Size);
                int y = random.Next(Size);
                while (grid[x,y].Live)
                {
                    x = random.Next(Size);
                    y = random.Next(Size);
                }
                grid[x, y].Live = true;
                grid[x, y].Neighbors = 9;
            }
        }

        public void calculateLiveNeighbors()
        {
            int Size1 = Size - 1;
            for (int row = 0; row < Size; row++)
            {
                for (int col = 0; col < Size; col++)
                {
                    if (grid[row,col].Live)
                    {
                        if (row > 0 && col > 0) grid[row - 1, col - 1].incrementNeighbors();
                        if (row > 0) grid[row - 1, col].incrementNeighbors();
                        if (row > 0 && col < Size1) grid[row - 1, col + 1].incrementNeighbors();

                        if (col > 0) grid[row, col - 1].incrementNeighbors();
                        if (col < Size1) grid[row, col + 1].incrementNeighbors();

                        if (row < Size1 && col > 0) grid[row + 1, col - 1].incrementNeighbors();
                        if (row < Size1) grid[row + 1, col].incrementNeighbors();
                        if (row < Size1 && col < Size1) grid[row + 1, col + 1].incrementNeighbors();
                    }
                }
            }
        }

        // Reset game board should we want to play again
        public void reset()
        {
            for (int row = 0; row < Size; row++)
                for (int col = 0; col < Size; col++)
                    grid[row, col].reset();
        }
    }
}
