/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-21
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Milestone 7
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Modify existing code with services and Level object
 * 2. OptionsView Form - Select Game options (Level)
 * 3. GameView Form - Contains Game board and Status Panel 
 * 4. HighScoreView Form - Contains Data Grid View and Entry Panel
 * 5. PlayerStats,PlayerStatsManager for Persistent Data
 * Objective:
 * Fully functional game play with high score capture
 * ---------------------------------------------------------------
 */

using Milestone07.Models.Implementation;
using Milestone07.Services.Implementations;
using Milestone07.Services.Interfaces;
using System;
using System.Windows.Forms;

namespace Milestone07
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // Define service used to control the minesweeper game
            // Attempt Dependency Injection through service
            IGameService gameService = new GameService(
                                           new PlayerStatsManager(new FileAccessService(@".\", "MineSweeperScores.txt"), new Level()),
                                           new Level(),
                                           new Board(),
                                           30);

            Application.Run(new GameViewForm(gameService));
        }
    }
}
