/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-21
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Milestone 7
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Modify existing code with services and Level object
 * 2. OptionsView Form - Select Game options (Level)
 * 3. GameView Form - Contains Game board and Status Panel 
 * 4. HighScoreView Form - Contains Data Grid View and Entry Panel
 * 5. PlayerStats,PlayerStatsManager for Persistent Data
 * Objective:
 * Fully functional game play with high score capture
 * ---------------------------------------------------------------
 */

namespace Milestone07
{
    partial class GameViewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GameViewForm));
            this.GameMenu = new System.Windows.Forms.MenuStrip();
            this.MenuItemFile = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemFile_Options = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemFile_Hint = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemFile_Separator2 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuItemFile_HighScore = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemFile_Separator1 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuItemFile_Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemHelp_Help = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuItemHelp_About = new System.Windows.Forms.ToolStripMenuItem();
            this.GameMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // GameMenu
            // 
            this.GameMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItemFile,
            this.MenuItemHelp});
            this.GameMenu.Location = new System.Drawing.Point(0, 0);
            this.GameMenu.Name = "GameMenu";
            this.GameMenu.Size = new System.Drawing.Size(334, 24);
            this.GameMenu.TabIndex = 2;
            this.GameMenu.Text = "menuStrip1";
            // 
            // MenuItemFile
            // 
            this.MenuItemFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItemFile_Options,
            this.MenuItemFile_Hint,
            this.MenuItemFile_Separator2,
            this.MenuItemFile_HighScore,
            this.MenuItemFile_Separator1,
            this.MenuItemFile_Exit});
            this.MenuItemFile.Name = "MenuItemFile";
            this.MenuItemFile.Size = new System.Drawing.Size(37, 20);
            this.MenuItemFile.Text = "&File";
            // 
            // MenuItemFile_Options
            // 
            this.MenuItemFile_Options.Name = "MenuItemFile_Options";
            this.MenuItemFile_Options.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.MenuItemFile_Options.Size = new System.Drawing.Size(189, 22);
            this.MenuItemFile_Options.Text = "&Options ...";
            this.MenuItemFile_Options.Click += new System.EventHandler(this.MenuItemOptions_Click);
            // 
            // MenuItemFile_Hint
            // 
            this.MenuItemFile_Hint.Name = "MenuItemFile_Hint";
            this.MenuItemFile_Hint.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.H)));
            this.MenuItemFile_Hint.Size = new System.Drawing.Size(189, 22);
            this.MenuItemFile_Hint.Text = "&Hint";
            this.MenuItemFile_Hint.Click += new System.EventHandler(this.MenuItemHint_Click);
            // 
            // MenuItemFile_Separator2
            // 
            this.MenuItemFile_Separator2.Name = "MenuItemFile_Separator2";
            this.MenuItemFile_Separator2.Size = new System.Drawing.Size(186, 6);
            // 
            // MenuItemFile_HighScore
            // 
            this.MenuItemFile_HighScore.Name = "MenuItemFile_HighScore";
            this.MenuItemFile_HighScore.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.MenuItemFile_HighScore.Size = new System.Drawing.Size(189, 22);
            this.MenuItemFile_HighScore.Text = "High &Scores ...";
            this.MenuItemFile_HighScore.Click += new System.EventHandler(this.MenuItemHighScores_Click_1);
            // 
            // MenuItemFile_Separator1
            // 
            this.MenuItemFile_Separator1.Name = "MenuItemFile_Separator1";
            this.MenuItemFile_Separator1.Size = new System.Drawing.Size(186, 6);
            // 
            // MenuItemFile_Exit
            // 
            this.MenuItemFile_Exit.Name = "MenuItemFile_Exit";
            this.MenuItemFile_Exit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.MenuItemFile_Exit.Size = new System.Drawing.Size(189, 22);
            this.MenuItemFile_Exit.Text = "E&xit";
            this.MenuItemFile_Exit.Click += new System.EventHandler(this.MenuItemExit_Click);
            // 
            // MenuItemHelp
            // 
            this.MenuItemHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItemHelp_Help,
            this.toolStripSeparator1,
            this.MenuItemHelp_About});
            this.MenuItemHelp.Name = "MenuItemHelp";
            this.MenuItemHelp.Size = new System.Drawing.Size(44, 20);
            this.MenuItemHelp.Text = "Help";
            // 
            // MenuItemHelp_Help
            // 
            this.MenuItemHelp_Help.Name = "MenuItemHelp_Help";
            this.MenuItemHelp_Help.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F1)));
            this.MenuItemHelp_Help.Size = new System.Drawing.Size(222, 22);
            this.MenuItemHelp_Help.Text = "Help";
            this.MenuItemHelp_Help.Click += new System.EventHandler(this.MenuItemHelp_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(219, 6);
            // 
            // MenuItemHelp_About
            // 
            this.MenuItemHelp_About.Name = "MenuItemHelp_About";
            this.MenuItemHelp_About.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.MenuItemHelp_About.Size = new System.Drawing.Size(222, 22);
            this.MenuItemHelp_About.Text = "&About Minesweeper";
            this.MenuItemHelp_About.Click += new System.EventHandler(this.MenuItemAbout_Click);
            // 
            // GameViewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 339);
            this.Controls.Add(this.GameMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.GameMenu;
            this.MaximizeBox = false;
            this.Name = "GameViewForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Minesweeper";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.GameViewForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.GameViewForm_FormClosed);
            this.GameMenu.ResumeLayout(false);
            this.GameMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip GameMenu;
        private System.Windows.Forms.ToolStripMenuItem MenuItemFile;
        private System.Windows.Forms.ToolStripMenuItem MenuItemFile_Options;
        private System.Windows.Forms.ToolStripMenuItem MenuItemFile_HighScore;
        private System.Windows.Forms.ToolStripSeparator MenuItemFile_Separator1;
        private System.Windows.Forms.ToolStripMenuItem MenuItemFile_Exit;
        private System.Windows.Forms.ToolStripMenuItem MenuItemHelp;
        private System.Windows.Forms.ToolStripMenuItem MenuItemFile_Hint;
        private System.Windows.Forms.ToolStripSeparator MenuItemFile_Separator2;
        private System.Windows.Forms.ToolStripMenuItem MenuItemHelp_Help;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem MenuItemHelp_About;
    }
}