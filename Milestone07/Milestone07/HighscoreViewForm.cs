/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-21
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Milestone 7
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Modify existing code with services and Level object
 * 2. OptionsView Form - Select Game options (Level)
 * 3. GameView Form - Contains Game board and Status Panel 
 * 4. HighScoreView Form - Contains Data Grid View and Entry Panel
 * 5. PlayerStats,PlayerStatsManager for Persistent Data
 * Objective:
 * Fully functional game play with high score capture
 * ---------------------------------------------------------------
 */

using Milestone07.Models.Interfaces;
using Milestone07.Services.Interfaces;
using System;
using System.Windows.Forms;

namespace Milestone07
{
    public partial class HighscoreViewForm : Form
    {
        private IGameService gameService;
        private IPlayerStats highScoreEntry;
        private int position = 0;
        private int level = 0;
        public HighscoreViewForm(IGameService gameService, bool showPanel, IPlayerStats newEntry, int position, int level)
        {
            InitializeComponent();
            panel_EnterName.Visible = showPanel;
            this.gameService = gameService;
            lbl_headerLevel.Text = gameService.getLevel().Description(level);
            highScoreEntry = newEntry;
            this.position = position;
            this.level = level;
            BindHighScoreDataGrid();

            if (showPanel)
            {
                lbl_ScoreValue.Text = highScoreEntry.Score.ToString();
                lbl_LevelDescription.Text = gameService.getLevel().Description();
                tb_PlayerName.Focus();
            }
        }

        private void btn_Okay_Click(object sender, EventArgs e)
        {
            highScoreEntry.PlayerName = tb_PlayerName.Text;
            if (tb_PlayerName.Text.Trim().Equals("")) highScoreEntry.PlayerName = "IForgot";
            panel_EnterName.Visible = false;
            gameService.getPlayerStatsManager().Add(highScoreEntry);
            gameService.getPlayerStatsManager().Sort();
            BindHighScoreDataGrid();
        }

        private void BindHighScoreDataGrid()
        {
            dgv_HighScores.DataSource = gameService.getPlayerStatsManager().GetHighScores(5, (Levels)level);
            dgv_HighScores.Columns["Level"].Visible = false;
            dgv_HighScores.Columns["AdjustedScore"].Visible = false;
            dgv_HighScores.Rows[position].Selected = true;
            dgv_HighScores.CurrentCell = dgv_HighScores.Rows[position].Cells[0];
            dgv_HighScores.Rows[position].Selected = true;
        }
    }
}
