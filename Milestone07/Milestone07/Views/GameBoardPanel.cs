/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-21
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Milestone 7
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Modify existing code with services and Level object
 * 2. OptionsView Form - Select Game options (Level)
 * 3. GameView Form - Contains Game board and Status Panel 
 * 4. HighScoreView Form - Contains Data Grid View and Entry Panel
 * 5. PlayerStats,PlayerStatsManager for Persistent Data
 * Objective:
 * Fully functional game play with high score capture
 * 
 * NOTE:Custom Panel - Will not load in designer (only view code)
 * ---------------------------------------------------------------
 */

using Milestone07.Models.Interfaces;
using Milestone07.Services.Interfaces;
using System.Drawing;
using System.Windows.Forms;

namespace Milestone07.Views
{
    public class GameBoardPanel : Panel, IGameResize, IGameBoardPanel
    {
        private Button[,] BoardButtonGrid;
        private IGameService gameService;
        private Point LastButton = new Point(0, 0);

        public GameBoardPanel(IGameService gameService)
        {
            this.gameService = gameService;
            InitializeComponent();
        }
        private void InitializeComponent()
        {
            Visible = false;
            int ButtonSize = gameService.getButtonSize();

            BorderStyle = BorderStyle.FixedSingle;
            Location = new Point(0, 85);
            Margin = new Padding(3, 3, 3, 3);

            int size = gameService.getLevel().BoardSize;
            // Resize panel to a square - could set panel1.autosize
            Size = new Size(size * ButtonSize, size * ButtonSize);

            Controls.Clear();
            BoardButtonGrid = new Button[size, size];

            // Nested loop. Create buttons and place them in the Panel
            for (int r = 0; r < size; r++)
            {
                for (int c = 0; c < size; c++)
                {
                    Button tmpButton = new Button();

                    // Make each button square
                    tmpButton.Width = ButtonSize;
                    tmpButton.Height = ButtonSize;

                    // Define default button behaviors
                    tmpButton.Text = "";
                    tmpButton.BackColor = Color.Silver;
                    tmpButton.MouseDown += BoardGrid_ButtonMouseDown;
                    tmpButton.MouseUp += BoardGrid_ButtonMouseUp;
                    tmpButton.Location = new Point(ButtonSize * c, ButtonSize * r); // Position it in x,y
                    tmpButton.Font = new Font(tmpButton.Font.FontFamily, 14, FontStyle.Bold);
                    tmpButton.Padding = new Padding(0);
                    tmpButton.TextAlign = ContentAlignment.TopCenter;
                    tmpButton.BackgroundImageLayout = ImageLayout.Zoom;

                    // The Tag attribute will hold the row and column number in a string
                    tmpButton.Tag = r.ToString() + "|" + c.ToString();

                    // Add to the game board button panel
                    Controls.Add(tmpButton);
                    BoardButtonGrid[r, c] = tmpButton;
                }
            }
            Visible = true;
        }

        private void BoardGrid_ButtonMouseUp(object sender, MouseEventArgs me)
        {
            gameService.HandleBoardMouseUp(sender, me);
        }

        private void BoardGrid_ButtonMouseDown(object sender, MouseEventArgs me)
        {
            // Get the row and column number of the button just clicked.
            string[] position = (sender as Button).Tag.ToString().Split('|');
            int row = int.Parse(position[0]);
            int col = int.Parse(position[1]);

            LastButton = new Point(col, row);
            BoardButtonGrid[row, col].BackgroundImage = null;

            gameService.HandleBoardMouseDown(sender, me);
        }

        public void Draw()
        {
            Color[] gameColors = { Color.White,  Color.Blue,   Color.Green,
                                   Color.Red,    Color.Orange, Color.Yellow,
                                   Color.Purple, Color.Cyan,   Color.Brown, Color.White};

            IBoard board = gameService.getGameBoard();
            int size = gameService.getLevel().BoardSize;

            for (int row = 0; row < size; row++)
            {
                for (int col = 0; col < size; col++)
                {
                    if (board.getBoardGrid()[row, col].Flagged)
                    {
                        BoardButtonGrid[row, col].BackgroundImage = Properties.Resources.cell_flag;
                    }
                    else if (board.getBoardGrid()[row, col].Visited)
                    {
                        int value = board.getBoardGrid()[row, col].Neighbors;
                        if (value > 0)
                        {
                            BoardButtonGrid[row, col].Text = value.ToString();
                            BoardButtonGrid[row, col].ForeColor = gameColors[value];
                        }
                        BoardButtonGrid[row, col].BackColor = Color.Gainsboro;
                    }
                }
            }
        }

        public void ExposeBombs()
        {
            IBoard board = gameService.getGameBoard();
            int size = gameService.getLevel().BoardSize;

            for (int row = 0; row < size; row++)
            {
                for (int col = 0; col < size; col++)
                {
                    if (LastButton.X == col && LastButton.Y == row && board.getBoardGrid()[row, col].Live)
                    {
                        BoardButtonGrid[row, col].BackgroundImage = Properties.Resources.cell_bombhit;
                        BoardButtonGrid[row, col].Text = "";
                    }
                    else if (board.getBoardGrid()[row, col].Flagged && !board.getBoardGrid()[row, col].Live)
                    {
                        BoardButtonGrid[row, col].BackgroundImage = Properties.Resources.cell_bombmiss;
                    }
                    else if (board.getBoardGrid()[row, col].Live)
                    {
                        BoardButtonGrid[row, col].BackgroundImage = Properties.Resources.cell_bomb;
                    }
                }
            }
        }

        public void GameResize()
        {
            InitializeComponent();
        }

        public void HintCellClick(int row, int col)
        {
            var hintEvent = new MouseEventArgs(Control.MouseButtons, 0, col, row, 0);
            BoardGrid_ButtonMouseDown(BoardButtonGrid[row, col], hintEvent);
            BoardGrid_ButtonMouseUp(BoardButtonGrid[row, col], hintEvent);
            BoardButtonGrid[row, col].Focus();
        }

        public void Reset()
        {
            InitializeComponent();
        }
    }
}
