/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-21
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Milestone 7
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Modify existing code with services and Level object
 * 2. OptionsView Form - Select Game options (Level)
 * 3. GameView Form - Contains Game board and Status Panel 
 * 4. HighScoreView Form - Contains Data Grid View and Entry Panel
 * 5. PlayerStats,PlayerStatsManager for Persistent Data
 * Objective:
 * Fully functional game play with high score capture
 *  
 * NOTE:Custom Panel - Will not load in designer (only view code)
 * ---------------------------------------------------------------
 */

using Milestone07.Services.Interfaces;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace Milestone07.Views
{
    class GameStatusPanel : Panel, IGameResize, IGameStatusPanel
    {
        private Label LabelFlagCount;
        private Label LabelSecondCount;
        private PictureBox PictureBoxSmiley;
        private Timer TimerSeconds;
        private Stopwatch watch;

        private IGameService gameService;

        public GameStatusPanel(IGameService gameService)
        {
            this.gameService = gameService;
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            // Set Properties for Panel
            BackColor = Color.Gainsboro;
            Size = new Size(334, 60);
            BorderStyle = BorderStyle.FixedSingle;
            Location = new Point(0, 25);
            Margin = new Padding(3, 3, 3, 3);

            // Set Properties for Label contain flag count
            LabelFlagCount = new Label();
            LabelFlagCount.BackColor = Color.Black;
            LabelFlagCount.ForeColor = Color.Red;
            LabelFlagCount.Size = new Size(58, 33);
            LabelFlagCount.Margin = new Padding(3, 0, 3, 0);
            LabelFlagCount.BorderStyle = BorderStyle.Fixed3D;
            LabelFlagCount.Font = new Font("Goudy Old Style", 20);
            LabelFlagCount.Location = new Point(12, 12);
            LabelFlagCount.Text = "010";

            // Set Properties for Label contain second count
            LabelSecondCount = new Label();
            LabelSecondCount.BackColor = Color.Black;
            LabelSecondCount.ForeColor = Color.Red;
            LabelSecondCount.Size = new Size(58, 33);
            LabelSecondCount.Margin = new Padding(3, 0, 3, 0);
            LabelSecondCount.BorderStyle = BorderStyle.Fixed3D;
            LabelSecondCount.Font = new Font("Goudy Old Style", 20);
            LabelSecondCount.Location = new Point(263, 12);
            LabelSecondCount.Text = "000";

            // Set properties for PictureBox containing Smiley character
            PictureBoxSmiley = new PictureBox();
            PictureBoxSmiley.BackColor = Color.Gainsboro;
            PictureBoxSmiley.Size = new Size(33, 33);
            PictureBoxSmiley.Location = new Point(150, 12);
            PictureBoxSmiley.BackgroundImageLayout = ImageLayout.Stretch;
            PictureBoxSmiley.Margin = new Padding(3, 0, 3, 0);
            PictureBoxSmiley.Image = Properties.Resources.status_smilehappy;
            PictureBoxSmiley.Click += PictureBoxSmiley_Click;

            Controls.Add(LabelFlagCount);
            Controls.Add(PictureBoxSmiley);
            Controls.Add(LabelSecondCount);

            TimerSeconds = new Timer();
            TimerSeconds.Enabled = false;
            TimerSeconds.Interval = 1000;
            TimerSeconds.Tick += TimerSeconds_Tick;

            watch = new Stopwatch();
            Reset();
        }

        //
        // Timed event to update seconds elasped in game play
        //
        private void TimerSeconds_Tick(object sender, EventArgs e)
        {
            // Apply a TimeSpan to format the output 
            TimeSpan ts = watch.Elapsed;

            int elapsed = (ts.Minutes * 60) + ts.Seconds;

            // Use String.Format option for easy reading
            string elapsedTime = String.Format("{0:D3}", elapsed);

            LabelSecondCount.Text = elapsedTime;

            // Time Limit Exceeded - End Game
            if (elapsed == 999)
            {
                //GameStatus = GameState.GameOverLose;
                //GameOver();
            }
        }

        //
        // Click smiley to reset the game
        //
        private void PictureBoxSmiley_Click(object sender, EventArgs e)
        {
            gameService.HandleSmileyClick(sender, e);
        }

        public void Reset()
        {
            TimerSeconds.Enabled = false;
            watch.Stop();
            watch.Reset();
            Width = gameService.getLevel().BoardSize * gameService.getButtonSize();
            LabelSecondCount.Left = Width - (LabelSecondCount.Width + LabelFlagCount.Left);
            PictureBoxSmiley.Left = (Width / 2) - 16;
            LabelSecondCount.Text = "000";
            SetSmileyHappy();
            UpdateFlagCounter();
        }

        public void Start()
        {
            if (TimerSeconds.Enabled == false)
            {
                watch.Start();
                TimerSeconds.Enabled = true;
            }
        }

        public void Stop()
        {
            TimerSeconds.Enabled = false;
            watch.Stop();
        }

        public void SetSmileyWin()
        {
            PictureBoxSmiley.Image = Properties.Resources.status_smilewin;
        }

        public void SetSmileyLose()
        {
            PictureBoxSmiley.Image = Properties.Resources.status_smilelose;
        }

        public void SetSmileyScared()
        {
            PictureBoxSmiley.Image = Properties.Resources.status_smilescared;
        }

        public void SetSmileyHappy()
        {
            PictureBoxSmiley.Image = Properties.Resources.status_smilehappy;
        }

        public void UpdateFlagCounter()
        {
            int amount = gameService.getGameBoard().BombCount - gameService.getGameBoard().FlagCount;
            LabelFlagCount.Text = String.Format("{0:D3}", amount);
        }

        public int GetScore()
        {
            return int.Parse(LabelSecondCount.Text);
        }

        public void GameResize()
        {
            Reset();
        }
    }
}
