/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-21
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Milestone 7
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Modify existing code with services and Level object
 * 2. OptionsView Form - Select Game options (Level)
 * 3. GameView Form - Contains Game board and Status Panel 
 * 4. HighScoreView Form - Contains Data Grid View and Entry Panel
 * 5. PlayerStats,PlayerStatsManager for Persistent Data
 * Objective:
 * Fully functional game play with high score capture
 * ---------------------------------------------------------------
 */

namespace Milestone07
{
    partial class HighscoreViewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HighscoreViewForm));
            this.lbl_HallOfFame = new System.Windows.Forms.Label();
            this.dgv_HighScores = new System.Windows.Forms.DataGridView();
            this.panel_EnterName = new System.Windows.Forms.Panel();
            this.lbl_Name = new System.Windows.Forms.Label();
            this.lbl_LevelDescription = new System.Windows.Forms.Label();
            this.lbl_ScoreValue = new System.Windows.Forms.Label();
            this.lbl_Level = new System.Windows.Forms.Label();
            this.lbl_Score = new System.Windows.Forms.Label();
            this.tb_PlayerName = new System.Windows.Forms.TextBox();
            this.lbl_Congrats = new System.Windows.Forms.Label();
            this.btn_Okay = new System.Windows.Forms.Button();
            this.lbl_headerLevel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_HighScores)).BeginInit();
            this.panel_EnterName.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_HallOfFame
            // 
            this.lbl_HallOfFame.AutoSize = true;
            this.lbl_HallOfFame.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_HallOfFame.Location = new System.Drawing.Point(85, 19);
            this.lbl_HallOfFame.Name = "lbl_HallOfFame";
            this.lbl_HallOfFame.Size = new System.Drawing.Size(308, 31);
            this.lbl_HallOfFame.TabIndex = 0;
            this.lbl_HallOfFame.Text = "High Score Hall of Fame";
            // 
            // dgv_HighScores
            // 
            this.dgv_HighScores.AllowUserToAddRows = false;
            this.dgv_HighScores.AllowUserToDeleteRows = false;
            this.dgv_HighScores.AllowUserToResizeRows = false;
            this.dgv_HighScores.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_HighScores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_HighScores.Location = new System.Drawing.Point(20, 107);
            this.dgv_HighScores.MultiSelect = false;
            this.dgv_HighScores.Name = "dgv_HighScores";
            this.dgv_HighScores.ReadOnly = true;
            this.dgv_HighScores.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_HighScores.ShowCellErrors = false;
            this.dgv_HighScores.ShowEditingIcon = false;
            this.dgv_HighScores.ShowRowErrors = false;
            this.dgv_HighScores.Size = new System.Drawing.Size(438, 134);
            this.dgv_HighScores.TabIndex = 1;
            // 
            // panel_EnterName
            // 
            this.panel_EnterName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_EnterName.Controls.Add(this.lbl_Name);
            this.panel_EnterName.Controls.Add(this.lbl_LevelDescription);
            this.panel_EnterName.Controls.Add(this.lbl_ScoreValue);
            this.panel_EnterName.Controls.Add(this.lbl_Level);
            this.panel_EnterName.Controls.Add(this.lbl_Score);
            this.panel_EnterName.Controls.Add(this.tb_PlayerName);
            this.panel_EnterName.Controls.Add(this.lbl_Congrats);
            this.panel_EnterName.Controls.Add(this.btn_Okay);
            this.panel_EnterName.Location = new System.Drawing.Point(20, 262);
            this.panel_EnterName.Name = "panel_EnterName";
            this.panel_EnterName.Size = new System.Drawing.Size(438, 100);
            this.panel_EnterName.TabIndex = 2;
            // 
            // lbl_Name
            // 
            this.lbl_Name.AutoSize = true;
            this.lbl_Name.Location = new System.Drawing.Point(13, 35);
            this.lbl_Name.Name = "lbl_Name";
            this.lbl_Name.Size = new System.Drawing.Size(38, 13);
            this.lbl_Name.TabIndex = 7;
            this.lbl_Name.Text = "Name:";
            // 
            // lbl_LevelDescription
            // 
            this.lbl_LevelDescription.AutoSize = true;
            this.lbl_LevelDescription.Location = new System.Drawing.Point(57, 75);
            this.lbl_LevelDescription.Name = "lbl_LevelDescription";
            this.lbl_LevelDescription.Size = new System.Drawing.Size(30, 13);
            this.lbl_LevelDescription.TabIndex = 6;
            this.lbl_LevelDescription.Text = "Easy";
            // 
            // lbl_ScoreValue
            // 
            this.lbl_ScoreValue.AutoSize = true;
            this.lbl_ScoreValue.Location = new System.Drawing.Point(57, 56);
            this.lbl_ScoreValue.Name = "lbl_ScoreValue";
            this.lbl_ScoreValue.Size = new System.Drawing.Size(13, 13);
            this.lbl_ScoreValue.TabIndex = 5;
            this.lbl_ScoreValue.Text = "0";
            // 
            // lbl_Level
            // 
            this.lbl_Level.AutoSize = true;
            this.lbl_Level.Location = new System.Drawing.Point(13, 75);
            this.lbl_Level.Name = "lbl_Level";
            this.lbl_Level.Size = new System.Drawing.Size(36, 13);
            this.lbl_Level.TabIndex = 4;
            this.lbl_Level.Text = "Level:";
            // 
            // lbl_Score
            // 
            this.lbl_Score.AutoSize = true;
            this.lbl_Score.Location = new System.Drawing.Point(13, 56);
            this.lbl_Score.Name = "lbl_Score";
            this.lbl_Score.Size = new System.Drawing.Size(38, 13);
            this.lbl_Score.TabIndex = 3;
            this.lbl_Score.Text = "Score:";
            // 
            // tb_PlayerName
            // 
            this.tb_PlayerName.Location = new System.Drawing.Point(60, 32);
            this.tb_PlayerName.MaxLength = 20;
            this.tb_PlayerName.Name = "tb_PlayerName";
            this.tb_PlayerName.Size = new System.Drawing.Size(130, 20);
            this.tb_PlayerName.TabIndex = 2;
            // 
            // lbl_Congrats
            // 
            this.lbl_Congrats.AutoSize = true;
            this.lbl_Congrats.Location = new System.Drawing.Point(147, 10);
            this.lbl_Congrats.Name = "lbl_Congrats";
            this.lbl_Congrats.Size = new System.Drawing.Size(145, 13);
            this.lbl_Congrats.TabIndex = 1;
            this.lbl_Congrats.Text = "Congratulations! Enter Name:";
            // 
            // btn_Okay
            // 
            this.btn_Okay.Location = new System.Drawing.Point(345, 65);
            this.btn_Okay.Name = "btn_Okay";
            this.btn_Okay.Size = new System.Drawing.Size(75, 23);
            this.btn_Okay.TabIndex = 0;
            this.btn_Okay.Text = "Okay";
            this.btn_Okay.UseVisualStyleBackColor = true;
            this.btn_Okay.Click += new System.EventHandler(this.btn_Okay_Click);
            // 
            // lbl_headerLevel
            // 
            this.lbl_headerLevel.AutoSize = true;
            this.lbl_headerLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_headerLevel.Location = new System.Drawing.Point(229, 61);
            this.lbl_headerLevel.Name = "lbl_headerLevel";
            this.lbl_headerLevel.Size = new System.Drawing.Size(105, 31);
            this.lbl_headerLevel.TabIndex = 3;
            this.lbl_headerLevel.Text = "Difficult";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(136, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 31);
            this.label1.TabIndex = 4;
            this.label1.Text = "Level:";
            // 
            // HighscoreViewForm
            // 
            this.AcceptButton = this.btn_Okay;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(479, 381);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbl_headerLevel);
            this.Controls.Add(this.panel_EnterName);
            this.Controls.Add(this.dgv_HighScores);
            this.Controls.Add(this.lbl_HallOfFame);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "HighscoreViewForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Minesweeper - High Scores";
            ((System.ComponentModel.ISupportInitialize)(this.dgv_HighScores)).EndInit();
            this.panel_EnterName.ResumeLayout(false);
            this.panel_EnterName.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_HallOfFame;
        private System.Windows.Forms.DataGridView dgv_HighScores;
        private System.Windows.Forms.Panel panel_EnterName;
        private System.Windows.Forms.Label lbl_Name;
        private System.Windows.Forms.Label lbl_LevelDescription;
        private System.Windows.Forms.Label lbl_ScoreValue;
        private System.Windows.Forms.Label lbl_Level;
        private System.Windows.Forms.Label lbl_Score;
        private System.Windows.Forms.TextBox tb_PlayerName;
        private System.Windows.Forms.Label lbl_Congrats;
        private System.Windows.Forms.Button btn_Okay;
        private System.Windows.Forms.Label lbl_headerLevel;
        private System.Windows.Forms.Label label1;
    }
}