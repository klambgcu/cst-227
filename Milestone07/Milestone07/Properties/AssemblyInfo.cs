using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Minesweeper")]
[assembly: AssemblyDescription("GCU CST-227: Milestone Project - Final")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("KelSoft (Kelly E. Lamb Software) Inc.")]
[assembly: AssemblyProduct("Minesweeper")]
[assembly: AssemblyCopyright("Copyright © KelSoft 2020")]
[assembly: AssemblyTrademark("KelSoft - Free Use")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("f859cfe6-a195-44fe-a21a-472ed3565428")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("7.0.0.0")]
[assembly: AssemblyFileVersion("7.0.0.0")]
