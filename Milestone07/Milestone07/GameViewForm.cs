/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-21
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Milestone 7
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Modify existing code with services and Level object
 * 2. OptionsView Form - Select Game options (Level)
 * 3. GameView Form - Contains Game board and Status Panel 
 * 4. HighScoreView Form - Contains Data Grid View and Entry Panel
 * 5. PlayerStats,PlayerStatsManager for Persistent Data
 * Objective:
 * Fully functional game play with high score capture
 * ---------------------------------------------------------------
 */

using Milestone07.Services.Interfaces;
using Milestone07.Views;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Milestone07
{
    public partial class GameViewForm : Form, IGameResize
    {
        //
        // Initialize Form variables
        //
        private IGameService gameService;
        private GameBoardPanel gameBoardPanel;
        private GameStatusPanel gameStatusPanel;

        // Constructor
        public GameViewForm(IGameService gameService)
        {
            InitializeComponent();

            this.gameService = gameService;

            // Add custom panels
            gameStatusPanel = new GameStatusPanel(gameService);
            gameBoardPanel = new GameBoardPanel(gameService);
            Controls.Add(gameStatusPanel);
            Controls.Add(gameBoardPanel);

            // Register views with service
            gameService.MainView = this;
            gameService.GameBoardView = gameBoardPanel;
            gameService.GameStatusView = gameStatusPanel;

            gameService.Run();
        }

        public void GameResize()
        {
            Hide();
            int ButtonSize = gameService.getButtonSize();
            int size = gameService.getLevel().BoardSize;
            int height = gameStatusPanel.Height + gameStatusPanel.Location.Y;
            ClientSize = new Size(size * ButtonSize + 1, size * ButtonSize + height + 1);
            gameStatusPanel.GameResize();
            gameBoardPanel.GameResize();
            CenterForm();
            Show();
        }

        private void CenterForm()
        {
            // Reposition form at center of screen
            Screen myScreen = Screen.FromControl(this);
            Rectangle area = myScreen.WorkingArea;

            Top = (area.Height - Height) / 2;
            Left = (area.Width - Width) / 2;
        }

        // --------------------------------------------
        // Handle Menu Events
        // --------------------------------------------
        private void MenuItemOptions_Click(object sender, EventArgs e)
        {
            gameService.HandleMenuOptions(sender, e);
        }

        private void MenuItemHint_Click(object sender, EventArgs e)
        {
            gameService.HandleMenuHint(sender, e);
        }

        private void MenuItemHighScores_Click_1(object sender, EventArgs e)
        {
            gameService.HandleMenuHighScore(sender, e);
        }

        private void MenuItemExit_Click(object sender, EventArgs e)
        {
            gameService.HandleMenuExit(sender, e);
        }

        private void MenuItemHelp_Click(object sender, EventArgs e)
        {
            gameService.HandleMenuHelp(sender, e);
        }

        private void MenuItemAbout_Click(object sender, EventArgs e)
        {
            gameService.HandleMenuAbout(sender, e);
        }

        // --------------------------------------------
        // Handle Closing Events
        // --------------------------------------------
        private void GameViewForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            gameService.HandleFormClosed(sender, e);
        }

        private void GameViewForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            gameService.HandleFormClosing(sender, e);
        }
    }
}
