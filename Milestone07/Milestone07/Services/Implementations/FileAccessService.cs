/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-21
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Milestone 7
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Modify existing code with services and Level object
 * 2. OptionsView Form - Select Game options (Level)
 * 3. GameView Form - Contains Game board and Status Panel 
 * 4. HighScoreView Form - Contains Data Grid View and Entry Panel
 * 5. PlayerStats,PlayerStatsManager for Persistent Data
 * Objective:
 * Fully functional game play with high score capture
 * ---------------------------------------------------------------
 */

using Milestone07.Models.Implementation;
using Milestone07.Models.Interfaces;
using Milestone07.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;

namespace Milestone07.Services.Implementations
{
    // Service to retrieve data from tab delimited text file
    public class FileAccessService : IDataAccessService
    {
        private string fileName;
        private string directory;

        public FileAccessService(string directory, string fileName)
        {
            this.directory = directory;
            this.fileName = fileName;
        }

        public bool Read(IList<IPlayerStats> list, IList<String> errMsgs, ILevel level)
        {
            bool results = true;
            list.Clear();
            errMsgs.Clear();
            string file = directory + fileName;

            if (!File.Exists(file))
            {
                errMsgs.Add("File not found: " + file);
                results = false;
            }
            else
            {
                StreamReader inputFile = File.OpenText(file);
                try
                {
                    //int count = int.Parse(inputFile.ReadLine());
                    string line;
                    int lineNo = 0;

                    //for (int i = 0; i < count; i++)
                    while ((line = inputFile.ReadLine()) != null)
                    {
                        bool foundError = false;
                        string[] values = line.Split('\t');

                        if (values.Length == 3)
                        {
                            string name = values[0];
                            int levelId = int.Parse(values[1]);
                            int score = int.Parse(values[2]);

                            if (levelId < level.ConvertLevelToInt(Levels.Easy) || levelId >= level.ConvertLevelToInt(Levels.All))
                            {
                                errMsgs.Add(String.Format("Incorrect range for level (0-2) at record: {0}", lineNo + 1));
                                foundError = true;
                            }

                            if (score < 0 || score > 999)
                            {
                                errMsgs.Add(String.Format("Incorrect range for score (0-999) at record: {0}", lineNo + 1));
                                foundError = true;
                            }

                            if (!foundError)
                            {
                                level.Id = levelId;
                                list.Add(new PlayerStats(name, levelId, score, level.BombCount, level.Description()));
                            }
                        }
                        else
                        {
                            errMsgs.Add(String.Format("Incorrect field count at record: {0}. Should be 3 tab delimited fields.", lineNo + 1));
                        }
                        lineNo++;
                    }
                }
                catch (Exception ex)
                {
                    errMsgs.Add(String.Format("Error reading highscore data file ({0} {1}.", file, ex.Message));
                }
                finally
                {
                    inputFile.Close();
                }
            }

            // Set results to false - indicate to calling to display messages
            if (errMsgs.Count > 0) results = false;

            return results;
        }

        public bool Write(IList<IPlayerStats> list, IList<String> errMsgs)
        {
            bool results = true;
            errMsgs.Clear();

            string file = directory + fileName;

            try
            {
                using (System.IO.StreamWriter outFile = new System.IO.StreamWriter(file))
                {
                    foreach (PlayerStats stat in list)
                    {
                        outFile.WriteLine(stat.ToTabDelimitedString());
                    }
                }
            }
            catch (Exception ex)
            {
                results = false;
                errMsgs.Add(String.Format("Error writing highscore data file ({0} {1}.", file, ex.Message));
            }

            return results;
        }
    }
}
