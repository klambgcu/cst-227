/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-21
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Milestone 7
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Modify existing code with services and Level object
 * 2. OptionsView Form - Select Game options (Level)
 * 3. GameView Form - Contains Game board and Status Panel 
 * 4. HighScoreView Form - Contains Data Grid View and Entry Panel
 * 5. PlayerStats,PlayerStatsManager for Persistent Data
 * Objective:
 * Fully functional game play with high score capture
 * ---------------------------------------------------------------
 */

using Milestone07.Models.Interfaces;
using Milestone07.Services.Interfaces;
using System;
using System.Collections.Generic;

namespace Milestone07.Services.Implementations
{
    // Service to retrieve data from database
    public class DatabaseAccessService : IDataAccessService
    {
        private string server;
        private string catalog;
        private string user;
        private string password;

        public DatabaseAccessService(string server,
                                     string catalog,
                                     string user,
                                     string password)
        {
            this.server = server;
            this.catalog = catalog;
            this.user = user;
            this.password = password;
        }

        public bool Read(IList<IPlayerStats> list, IList<string> errMsgs, ILevel level)
        {
            throw new NotImplementedException();
        }

        public bool Write(IList<IPlayerStats> list, IList<string> errMsgs)
        {
            throw new NotImplementedException();
        }
    }
}
