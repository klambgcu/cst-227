﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-21
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Milestone 7
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Modify existing code with services and Level object
 * 2. OptionsView Form - Select Game options (Level)
 * 3. GameView Form - Contains Game board and Status Panel 
 * 4. HighScoreView Form - Contains Data Grid View and Entry Panel
 * 5. PlayerStats,PlayerStatsManager for Persistent Data
 * Objective:
 * Fully functional game play with high score capture
 * ---------------------------------------------------------------
 */

using Milestone07.Models.Implementation;
using Milestone07.Models.Interfaces;
using Milestone07.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Milestone07.Services.Interfaces
{
    public class GameService : IGameService
    {
        public IWin32Window MainView { get; set; }
        public IGameBoardPanel GameBoardView { get; set; }
        public IGameStatusPanel GameStatusView { get; set; }

        private ILevel level;
        private IPlayerStatsManager manager;
        private IBoard board;

        private int ButtonSize = 30;
        private GameState gameState = GameState.GamePlaying;

        public GameService(IPlayerStatsManager manager, ILevel level, IBoard board, int ButtonSize)
        {
            this.manager = manager;
            this.level = level;
            this.board = board;
            this.ButtonSize = ButtonSize;
        }

        public ILevel getLevel()
        {
            return level;
        }

        public IPlayerStatsManager getPlayerStatsManager()
        {
            return manager;
        }

        public IBoard getGameBoard()
        {
            return board;
        }

        public int getButtonSize()
        {
            return ButtonSize;
        }

        // -------------------------------------
        // Menu Event Handlers
        // -------------------------------------
        public void HandleMenuAbout(object sender, EventArgs e)
        {
            GameStatusView.Stop();

            using (GameAboutViewForm about = new GameAboutViewForm())
            {
                about.ShowDialog(MainView);
            }

            GameStatusView.Start();
        }

        public void HandleMenuOptions(object sender, EventArgs e)
        {
            GameStatusView.Stop();

            int previousId = getLevel().Id;
            using (OptionsViewForm option = new OptionsViewForm(this))
            {
                option.ShowDialog(MainView);
            }
            if (previousId != getLevel().Id)
            {
                ConfigureBoard();
                gameState = GameState.GamePlaying;
                // New level selection - call resize on MainView
                ((IGameResize)MainView).GameResize();
            }
            else
            {
                GameStatusView.Start();
            }
        }

        public void HandleMenuHint(object sender, EventArgs e)
        {
            int size = getLevel().BoardSize;
            int row = 0;
            int col = 0;
            bool found = false;
            do
            {
                col = 0;
                do
                {
                    if ((!board.getBoardGrid()[row, col].Flagged) &&
                         (!board.getBoardGrid()[row, col].Visited) &&
                         (!board.getBoardGrid()[row, col].Live))
                    {
                        found = true;
                        break;
                    }
                    else
                    {
                        col++;
                    }

                } while (col < size && !found);

                if (!found) row++;
            } while (row < size && !found);

            if (found)
            {
                GameBoardView.HintCellClick(row, col);
            }
        }

        public void HandleMenuHighScore(object sender, EventArgs e)
        {
            GameStatusView.Stop();

            using (HighscoreViewForm score = new HighscoreViewForm(this,
                                                                   false,
                                                                   (IPlayerStats)null,
                                                                   0,
                                                                   (int)Levels.All))
            {
                score.ShowDialog(MainView);
            }

            GameStatusView.Start();
        }

        public void HandleMenuExit(object sender, EventArgs e)
        {
            // User click exit application - confirm closing
            Application.Exit();
        }

        public void HandleMenuHelp(object sender, EventArgs e)
        {
            GameStatusView.Stop();

            StringBuilder message = new StringBuilder();
            message.AppendLine("Welcome to Minesweeper.");
            message.AppendLine();
            message.AppendLine("Left click on playfield to expose what is beneath it.");
            message.AppendLine("Right click on playfield to toggle a flag to indicate a mine.");
            message.AppendLine("Click on smiley face to reset the game at any time.");
            message.AppendLine("Clicking an empty playfield position will expose all surrounding empty areas.");
            message.AppendLine("A playfield containing a number indicates the number of mines it touches (sides & corners)");
            message.AppendLine();
            message.Append("Game ends when all mines are flagged and remaining playfield is cleared ");
            message.AppendLine("or when a mine is detonated or time limit is exceeded (999 seconds).");
            message.AppendLine("Menu options allow you to change levels, view top scores, see help and about info.");
            message.AppendLine("If you get stuck, the Hint menu will click a safe place, if one exists.");
            message.AppendLine();
            message.AppendLine("Hope you enjoy the game.");

            MessageBox.Show(message.ToString(),
                             "Minesweeper - Help",
                             MessageBoxButtons.OK,
                             MessageBoxIcon.Question);
            GameStatusView.Start();
        }

        // -------------------------------------
        // Form Closing Evemt Handlers
        // -------------------------------------
        public void HandleFormClosed(object sender, FormClosedEventArgs e)
        {
            // User click exit application - confirm closing
            Application.Exit();
        }

        public void HandleFormClosing(object sender, FormClosingEventArgs e)
        {
            GameStatusView.Stop();
            DialogResult result = MessageBox.Show("Please Confirm ?",
                                                  "Exit Application",
                                                  MessageBoxButtons.YesNo,
                                                  MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                // User confirms close application - write highscore data
                getPlayerStatsManager().Write();
            }
            else
            {
                e.Cancel = true;
                GameStatusView.Start();
            }
        }

        // -------------------------------------
        // Game Play Event Handlers
        // -------------------------------------
        public void HandleSmileyClick(object sender, EventArgs e)
        {
            // Reset Custom Game Views (Panels)
            GameStatusView.Reset();
            GameBoardView.Reset();
            ConfigureBoard();
            gameState = GameState.GamePlaying;
        }

        public void HandleBoardMouseDown(object sender, MouseEventArgs me)
        {
            if (gameState != GameState.GamePlaying) return;
            GameStatusView.Start();

            // Get the row and column number of the button just clicked.
            string[] position = (sender as Button).Tag.ToString().Split('|');
            int row = int.Parse(position[0]);
            int col = int.Parse(position[1]);

            // Check Right Click to toggle flag placement
            if (me.Button == MouseButtons.Right)
            {
                board.toggleFlag(row, col);
                GameStatusView.UpdateFlagCounter();
                GameBoardView.Draw();
            }
            else
            {
                // Ignore click if grid is flagged or visited.
                if (board.getBoardGrid()[row, col].Flagged || board.getBoardGrid()[row, col].Visited) return;

                // Set smiley face to ohoh.
                //PictureBoxSmiley.Image = Properties.Resources.status_smilescared;
                GameStatusView.SetSmileyScared();

                // Set cell chosen/visited
                if (board.getBoardGrid()[row, col].Neighbors == 0)
                {
                    board.floodFill(row, col);
                    GameBoardView.Draw();
                }
                else
                {
                    board.setCellVisited(row, col);
                    GameBoardView.Draw();
                }

                // Check game completed - Bomb Chosen
                if (board.getBoardGrid()[row, col].Live)
                {
                    gameState = GameState.GameOverLose;
                    GameOver();
                }
            }

            // Check game completed - Board cleared
            if (gameState == GameState.GamePlaying && board.boardCompleted())
            {
                gameState = GameState.GameOverWin;
                GameOver();
            }
        }

        private void GameOver()
        {
            GameStatusView.Stop();

            string msg;
            int position = 0;
            bool madeHighScore = false;
            IPlayerStats newEntry = getPlayerStatsManager().CreatePlayerStats("", GameStatusView.GetScore(), getLevel());

            if (gameState == GameState.GameOverWin)
            {
                msg = "    Congratulations! " + Environment.NewLine + "           You Win! ";
                GameStatusView.SetSmileyWin();

                // Inform player they won
                msg = msg + Environment.NewLine + "Seconds Elapsed: " + GameStatusView.GetScore();
                MessageBox.Show(msg, "Game Over", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                // Check if score is good enough for hall of fame 
                int gameLevel = getLevel().Id;
                newEntry.Score = GameStatusView.GetScore();
                position = getPlayerStatsManager().GetInsertPosition((Levels)gameLevel, 5, newEntry);
                if (position > -1)
                {
                    madeHighScore = true;
                }
                else
                {
                    position = 0;
                    madeHighScore = false;
                }
            }
            else
            {
                GameBoardView.ExposeBombs();

                msg = "          Oh no!     " + Environment.NewLine + "     Sorry you lose.";
                GameStatusView.SetSmileyLose(); ;

                // Inform player they lost
                msg = msg + Environment.NewLine + "Seconds Elapsed: " + GameStatusView.GetScore();
                MessageBox.Show(msg, "Game Over", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            // Display Highscore view after every game
            // If player wins the round, determine if score is in top five and allow name entry
            // Otherwise, just display 
            HighscoreViewForm highscoreForm = new HighscoreViewForm(this, madeHighScore, newEntry, position, getLevel().Id);
            highscoreForm.ShowDialog(MainView);
        }

        public void HandleBoardMouseUp(object sender, MouseEventArgs me)
        {
            if (gameState != GameState.GamePlaying) return;

            GameStatusView.Start();
            GameStatusView.SetSmileyHappy();
        }

        // -------------------------------------
        // Handle Data File Errors (Highscore)
        // -------------------------------------
        public void showFileFormatErrors()
        {
            StringBuilder messages = new StringBuilder();

            messages.AppendLine("Highscore file contains formatting errors:" + Environment.NewLine);

            IList<string> errors = getPlayerStatsManager().GetErrorMessages();
            foreach (string error in errors)
            {
                messages.AppendLine(error);
            }

            MessageBox.Show(messages.ToString(),
                            "Highscore File Format Errors",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Warning);
        }

        // -------------------------------------
        // Handle Game Logic
        // -------------------------------------

        private void ConfigureBoard()
        {
            board = new Board(getLevel().BoardSize);
            board.Difficulty = getLevel().BombPercentage;
            board.setupLiveNeighbors();
            board.calculateLiveNeighbors();
        }

        public void Run()
        {
            // Read Highscore stats file, show errors on false
            if (!getPlayerStatsManager().Read())
            {
                showFileFormatErrors();
            }
            ConfigureBoard();
            ((IGameResize)MainView).GameResize();
            gameState = GameState.GamePlaying;
        }
    }
}
