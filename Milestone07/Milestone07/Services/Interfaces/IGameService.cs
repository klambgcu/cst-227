﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-21
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Milestone 7
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Modify existing code with services and Level object
 * 2. OptionsView Form - Select Game options (Level)
 * 3. GameView Form - Contains Game board and Status Panel 
 * 4. HighScoreView Form - Contains Data Grid View and Entry Panel
 * 5. PlayerStats,PlayerStatsManager for Persistent Data
 * Objective:
 * Fully functional game play with high score capture
 * ---------------------------------------------------------------
 */

using Milestone07.Models.Interfaces;
using Milestone07.Views;
using System;
using System.Windows.Forms;

namespace Milestone07.Services.Interfaces
{
    // Define states for game loop logic
    public enum GameState
    {
        GamePlaying,
        GameOverWin,
        GameOverLose
    }

    public interface IGameService
    {
        ILevel getLevel();
        IPlayerStatsManager getPlayerStatsManager();
        IBoard getGameBoard();
        int getButtonSize();

        IWin32Window MainView { get; set; }
        IGameBoardPanel GameBoardView { get; set; }
        IGameStatusPanel GameStatusView { get; set; }


        // -------------------------------------
        // Menu Event Handlers
        // -------------------------------------

        void HandleMenuOptions(object sender, EventArgs e);

        void HandleMenuHint(object sender, EventArgs e);

        void HandleMenuHighScore(object sender, EventArgs e);

        void HandleMenuExit(object sender, EventArgs e);

        void HandleMenuHelp(object sender, EventArgs e);

        void HandleMenuAbout(object sender, EventArgs e);


        // -------------------------------------
        // Form Closing Event Handlers
        // -------------------------------------
        void HandleFormClosed(object sender, FormClosedEventArgs e);

        void HandleFormClosing(object sender, FormClosingEventArgs e);

        // -------------------------------------
        // Game Play Event Handlers
        // -------------------------------------
        void HandleSmileyClick(object sender, EventArgs e);

        void HandleBoardMouseDown(object sender, MouseEventArgs me);

        void HandleBoardMouseUp(object sender, MouseEventArgs me);


        // -------------------------------------
        // Handle Data File Errors (Highscore)
        // -------------------------------------
        void showFileFormatErrors();

        // -------------------------------------
        // Handle Game Logic
        // -------------------------------------
        void Run();



    }
}