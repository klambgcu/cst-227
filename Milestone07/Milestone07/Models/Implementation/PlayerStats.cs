/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-21
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Milestone 7
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Modify existing code with services and Level object
 * 2. OptionsView Form - Select Game options (Level)
 * 3. GameView Form - Contains Game board and Status Panel 
 * 4. HighScoreView Form - Contains Data Grid View and Entry Panel
 * 5. PlayerStats,PlayerStatsManager for Persistent Data
 * Objective:
 * Fully functional game play with high score capture
 * ---------------------------------------------------------------
 */

using Milestone07.Models.Interfaces;
using System;
using System.Collections.Generic;

namespace Milestone07.Models.Implementation
{
    public class PlayerStats : IPlayerStats, IComparable<IPlayerStats>, IComparer<IPlayerStats>
    {
        private int bombCount = 0;

        public PlayerStats() : this("No Name", 0, 999, 10, "Easy") { }

        public PlayerStats(string name, int level, int score, int bombCount, string description)
        {
            this.PlayerName = name;
            this.Level = level;
            this.Score = score;
            this.LevelDescription = description;
            this.bombCount = bombCount;
        }
        public string PlayerName { get; set; }

        public int Score { get; set; }

        public string LevelDescription { get; set; }

        public int Level { get; set; }

        public void setBombCount(int bombCount)
        {
            this.bombCount = bombCount;
        }

        public double AdjustedScore
        {
            get
            {
                // adjusted = score divided by bombs in level + 1.0 (convert double)
                return Score / (bombCount + 1.0);
            }
        }

        public int Compare(IPlayerStats x, IPlayerStats y)
        {
            int result;

            result = x.AdjustedScore.CompareTo(y.AdjustedScore);
            result = (result == 0) ? x.Level.CompareTo(y.Level) : result;
            result = (result == 0) ? x.Score.CompareTo(y.Score) : result;
            result = (result == 0) ? x.PlayerName.CompareTo(y.PlayerName) : result;

            return result;
        }

        // Sorting routing
        // Compare adjusted, if same check level, 
        // if same check score, lastly if same check name
        public int CompareTo(IPlayerStats other)
        {
            int result;

            result = this.AdjustedScore.CompareTo(other.AdjustedScore);
            result = (result == 0) ? this.Level.CompareTo(other.Level) : result;
            result = (result == 0) ? this.Score.CompareTo(other.Score) : result;
            result = (result == 0) ? this.PlayerName.CompareTo(other.PlayerName) : result;

            return result;
        }

        // Display formatted contents - include adjusted score for testing
        public override string ToString()
        {
            return String.Format("{0,-20} {1,-10} {2:d3} {3,5:F2}", PlayerName, LevelDescription, Score, AdjustedScore);
        }

        // Convert to tab delimited string - ease writing/reading score file
        public string ToTabDelimitedString()
        {
            return PlayerName + "\t" + Level.ToString() + "\t" + Score.ToString();
        }
    }
}
