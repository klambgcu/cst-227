/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-21
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Milestone 7
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Modify existing code with services and Level object
 * 2. OptionsView Form - Select Game options (Level)
 * 3. GameView Form - Contains Game board and Status Panel 
 * 4. HighScoreView Form - Contains Data Grid View and Entry Panel
 * 5. PlayerStats,PlayerStatsManager for Persistent Data
 * Objective:
 * Fully functional game play with high score capture
 * ---------------------------------------------------------------
 */

using Milestone07.Models.Interfaces;

namespace Milestone07.Models.Implementation
{
    // Define Cell class used to represent cell in a Mine Sweeper game
    public class Cell : ICell
    {
        // Default constructor - chain to next constructor
        public Cell() : this(0, 0) { }

        // Constructor for setting row, col
        public Cell(int row, int col)
        {
            Row = row;
            Col = col;
            Live = false;
            Visited = false;
            Flagged = false;
            Neighbors = 0;
        }

        // Row Cell resides within container grid
        public int Row { get; set; }

        // Column Cell resides within container grid
        public int Col { get; set; }

        // Count of neighbors containing bombs
        public int Neighbors { get; set; }

        // Live bomb = true, false no a bomb
        public bool Live { get; set; }

        // Determine if this cell has been visited
        public bool Visited { get; set; }

        public bool Flagged { get; set; }

        // Helper routine to ease setting Neighbor count
        public void incrementNeighbors()
        {
            Neighbors++;
            if (Live) Neighbors = 9; // Restrict to nine
        }

        // Reset properties should we want to play again
        public void reset()
        {
            Neighbors = 0;
            Live = false;
            Visited = false;
        }
    }
}
