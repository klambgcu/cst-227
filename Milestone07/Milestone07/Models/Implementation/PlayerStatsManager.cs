/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-21
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Milestone 7
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Modify existing code with services and Level object
 * 2. OptionsView Form - Select Game options (Level)
 * 3. GameView Form - Contains Game board and Status Panel 
 * 4. HighScoreView Form - Contains Data Grid View and Entry Panel
 * 5. PlayerStats,PlayerStatsManager for Persistent Data
 * Objective:
 * Fully functional game play with high score capture
 * ---------------------------------------------------------------
 */

using Milestone07.Models.Interfaces;
using Milestone07.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Milestone07.Models.Implementation
{
    public class PlayerStatsManager : IPlayerStatsManager
    {
        private IList<IPlayerStats> list = new List<IPlayerStats>();
        private IList<String> errMessages = new List<String>();
        IDataAccessService data;
        ILevel level;

        public PlayerStatsManager(IDataAccessService data, ILevel level)
        {
            this.data = data;
            this.level = level;
        }

        //
        // Read scores from file, if no file, then create one with default data
        // Returns true = success, false (default list) with error messages
        //
        public bool Read()
        {
            bool results = true;
            results = data.Read(list, errMessages, level);

            // Create blank entries if first time running (ie. no high score file)
            if (results == false && errMessages.Count == 1 && errMessages[0].Contains("File not found"))
            {
                // Create a default list and save it
                results = CreateDefaultFile();
            }
            else
            {
                // Examine list for any level containing less than five entries - add item if needed
                for (int i = (int)Levels.Easy; i < (int)Levels.All; i++)
                {
                    level.Id = i;
                    var amount = list.Where(p => p.Level == i).Count();
                    for (int j = amount; j < 5; j++)
                        list.Add(new PlayerStats($"No Name{j}", i, 999, level.BombCount, level.Description()));
                }

            }

            // Sort list after read/create
            Sort();

            return results;
        }

        //
        // Write list to file - count, then data
        //
        public bool Write()
        {
            bool results = true;
            // Sort list before writing
            //((List<IPlayerStats>)list).Sort();
            Sort();

            results = data.Write(list, errMessages);

            return results;
        }

        //
        // Return complete list of scores
        //
        public IList<IPlayerStats> GetList()
        {
            return list;
        }

        //
        // Returns top amount of highscores in list sorted upon adjusted score
        //
        public List<IPlayerStats> GetHighScores(int amount, Levels queryLevel)
        {
            IEnumerable<IPlayerStats> highscores;
            if (queryLevel == Levels.All)
            {
                highscores = list.OrderBy(stat => stat.AdjustedScore)
                                 .Take(amount)
                                 .ToList();
            }
            else
            {
                highscores = list.Where(stat => stat.Level == (int)queryLevel)
                                 .OrderBy(stat => stat.AdjustedScore)
                                 .Take(amount)
                                 .ToList();
            }
            return (List<IPlayerStats>)highscores;
        }

        //
        // Adds a new PlayerStat record to the end of the list
        //
        public void Add(IPlayerStats stat)
        {
            list.Add(stat);
        }

        //
        // Return a list of error messages
        //
        public IList<String> GetErrorMessages()
        {
            return errMessages;
        }

        // Sort the list of PlayerStats records
        public void Sort()
        {
            // Take advantage of LINQ syntax to sort elements
            list = list.OrderBy(stat => stat.AdjustedScore)
                       .ThenBy(stat => stat.Level)
                       .ThenBy(stat => stat.Score)
                       .ThenBy(stat => stat.PlayerName)
                       .ToList();
        }

        // Return a position to insert based upon level and score
        public int GetInsertPosition(Levels queryLevel, int highScoreAmount, IPlayerStats player)
        {
            int results = -1;
            List<IPlayerStats> templist = GetHighScores(highScoreAmount, queryLevel);

            for (int i = 0; i < templist.Count(); i++)
            {
                if (player.AdjustedScore < templist[i].AdjustedScore)
                {
                    results = i;
                    break;
                }
            }
            return results;
        }

        public IPlayerStats CreatePlayerStats(string name, int score, ILevel level)
        {
            return new PlayerStats(name, level.Id, score, level.BombCount, level.Description());
        }
        //
        // Create a default file with five records for each level
        //
        private bool CreateDefaultFile()
        {
            // Add 5 player stats for each level
            for (int i = (int)Levels.Easy; i < (int)Levels.All; i++)
            {
                level.Id = i;
                for (int j = 0; j < 5; j++)
                    list.Add(new PlayerStats($"No Name{j}", i, 999, level.BombCount, level.Description()));
            }

            return Write();
        }
    }
}
