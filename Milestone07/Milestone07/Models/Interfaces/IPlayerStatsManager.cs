/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-21
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Milestone 7
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Modify existing code with services and Level object
 * 2. OptionsView Form - Select Game options (Level)
 * 3. GameView Form - Contains Game board and Status Panel 
 * 4. HighScoreView Form - Contains Data Grid View and Entry Panel
 * 5. PlayerStats,PlayerStatsManager for Persistent Data
 * Objective:
 * Fully functional game play with high score capture
 * ---------------------------------------------------------------
 */

using System.Collections.Generic;

namespace Milestone07.Models.Interfaces
{
    public interface IPlayerStatsManager
    {
        void Add(IPlayerStats stat);
        IPlayerStats CreatePlayerStats(string name, int score, ILevel level);
        IList<string> GetErrorMessages();
        List<IPlayerStats> GetHighScores(int amount, Levels queryLevel);
        int GetInsertPosition(Levels queryLevel, int highScoreAmount, IPlayerStats player);
        IList<IPlayerStats> GetList();
        bool Read();
        void Sort();
        bool Write();
    }
}