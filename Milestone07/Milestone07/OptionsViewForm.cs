/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-21
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Milestone 7
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Modify existing code with services and Level object
 * 2. OptionsView Form - Select Game options (Level)
 * 3. GameView Form - Contains Game board and Status Panel 
 * 4. HighScoreView Form - Contains Data Grid View and Entry Panel
 * 5. PlayerStats,PlayerStatsManager for Persistent Data
 * Objective:
 * Fully functional game play with high score capture
 * ---------------------------------------------------------------
 */

using Milestone07.Services.Interfaces;
using System;
using System.Windows.Forms;

namespace Milestone07
{
    public partial class OptionsViewForm : Form
    {
        private IGameService gameService;

        // Constructor
        public OptionsViewForm(IGameService gameService)
        {
            InitializeComponent();
            // Display the form in the center of the screen.
            StartPosition = FormStartPosition.CenterScreen;

            this.gameService = gameService;

            int level = gameService.getLevel().Id;

            switch (level)
            {
                case 0: rb_EasyLevel.Checked = true; break;
                case 1: rb_ModerateLevel.Checked = true; break;
                case 2: rb_DifficultLevel.Checked = true; break;
                default: rb_EasyLevel.Checked = true; break;
            }
        }

        //
        // Obtain player game preference & return choice (level)
        //
        private void btn_PlayGame_Click(object sender, EventArgs e)
        {
            int level = 0;

            if (rb_EasyLevel.Checked) level = 0;
            if (rb_ModerateLevel.Checked) level = 1;
            if (rb_DifficultLevel.Checked) level = 2;

            gameService.getLevel().Id = level;

            this.Dispose();
        }
    }
}
