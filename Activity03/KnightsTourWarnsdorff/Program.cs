﻿/*
 * ---------------------------------------------------------------
 * BoardSizeame      : Kelly Lamb
 * Date      : 2020-08-24
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Activity 3
 * Disclaimer: This is my own work - Utilized Warnsdorff's
 * as described in GeekForGeek tutorials.
 * ---------------------------------------------------------------
 * Description:
 * Activities to strengthen knowledge and understanding of recursive
 * methods. We have four sections in this assignment, each
 * increasingly challenging. They are:
 * 1. CountToOne
 * 2. Factorial
 * 3. Greatest Common Denominator (GCD)
 * 4. Knight's Tour (Backtrack & Warnsdorff)
 * Objective:
 * Recursion is a problem-solving technique that is used in many 
 * computational problems. Students will explore the impact of 
 * recursion on memory and performance and learn to identify 
 * scenarios where recursive solutions are appropriate.
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnightsTourWarnsdorff
{
    class Program
    {
        static Random random = new Random();
        static int BoardSize = 8;

        // xMove[] and yMove[] defne next move of Knight
        // xMove[] is the for next value of x coordinate
        // yMove[] is the for next value of the y coordinate
        static int[] xMove = { 2, 1, -1, -2, -2, -1, 1, 2 };
        static int[] yMove = { 1, 2, 2, 1, -1, -2, -2, -1 };

        // boardGrid is an 8x8 array that contains -1 for an 
        // unvisited square or a move number between 0 and 63.
        static int[,] boardGrid = new int[BoardSize, BoardSize];

        // Driver Code
        static void Main(string[] args)
        {
            // Loop through each position (x,y) to find solution(s)
            for (int x = 0; x < BoardSize; x++)
            {
                for (int y = 0; y < BoardSize; y++)
                {
                    Console.Out.WriteLine("Starting Position: X = {0}, Y = {1}", x, y);
                    bool results = findClosedTour(x, y);
                    Console.Out.WriteLine("Results: {0}", results.ToString());
                }
            }
            Console.ReadLine();
        }

        // A utility function to check if i,j are valid indexes for N*N chessboard
        static bool isSquareSafe(int x, int y)
        {
            return (x >= 0 && x < BoardSize &&
                    y >= 0 && y < BoardSize &&
                    boardGrid[x, y] == -1);
        }

        // Returns the number of empty squares adjacent to (x, y)
        static int getDegree(int x, int y)
        {
            int count = 0;
            for (int i = 0; i < BoardSize; ++i)
            {
                if (isSquareSafe((x + xMove[i]), (y + yMove[i])))
                {
                    count++;
                }
            }
            return count;
        }

        // Picks next point using Warnsdorff's heuristic. 
        // Returns false if it is not possible to pick 
        // next point. 
        static bool findNextMove(ref int x, ref int y)
        {
            int min_deg_idx = -1, c, min_deg = (BoardSize + 1), nx, ny;

            // Try all BoardSize adjacent of (x, y) starting from a random adjacent.
            // Find the adjacent with minimum degree. 
            int start = random.Next(BoardSize);
            for (int count = 0; count < BoardSize; ++count)
            {
                int i = (start + count) % BoardSize;
                nx = x + xMove[i];
                ny = y + yMove[i];
                if ((isSquareSafe(nx, ny)) && (c = getDegree(nx, ny)) < min_deg)
                {
                    min_deg_idx = i;
                    min_deg = c;
                }
            }

            // IF we could not find a next cell 
            if (min_deg_idx == -1)
            {
                return false;
            }

            // Store coordinates of next point 
            nx = x + xMove[min_deg_idx];
            ny = y + yMove[min_deg_idx];

            // Mark next move 
            boardGrid[nx, ny] = boardGrid[x, y] + 1;

            // Update next point 
            x = nx;
            y = ny;

            return true;
        }

        // A utility function to print the solution matrix
        static void printSolution(int[,] solution)
        {
            for (int x = 0; x < BoardSize; x++)
            {
                for (int y = 0; y < BoardSize; y++)
                {
                    Console.Out.Write(" {0,3} ", solution[x, y]);
                }
                Console.Out.WriteLine();
            }
        }

        // checks its neighbouring squares.
        // If the knight ends on a square that is one knight's move from the beginning square, 
        // then tour is closed
        static bool CheckVisitedNeighbors(int x, int y, int xx, int yy)
        {
            for (int i = 0; i < BoardSize; ++i)
            {
                if (((x + xMove[i]) == xx) && ((y + yMove[i]) == yy))
                {
                    return true;
                }
            }
            return false;
        }

        // Generates the legal moves using Warnsdorff's heuristics. Returns false if not possible
        static bool findClosedTour(int startX, int startY)
        {
            for (int i = 0; i < BoardSize; ++i)
            {
                for (int j = 0; j < BoardSize; j++)
                {
                    boardGrid[i, j] = -1;
                }
            }

            // Starting Position
            int sx = startX;
            int sy = startY;

            // Current points are same as initial points 
            int x = sx, y = sy;
            boardGrid[x,y] = 1; // Mark first move. 

            // Keep picking next points using Warnsdorff's heuristic 
            for (int i = 0; i < BoardSize * BoardSize - 1; ++i)
            {
                if (!findNextMove(ref x, ref y))
                {
                    return false;
                }
            }

            // Check if tour is closed (Can end at starting point) 
            if (CheckVisitedNeighbors(x, y, sx, sy))
            {
                return false;
            }

            printSolution(boardGrid);
            return true;
        }
    }
}
