﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-08-24
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Activity 3
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * Activities to strengthen knowledge and understanding of recursive
 * methods. We have four sections in this assignment, each
 * increasingly challenging. They are:
 * 1. CountToOne
 * 2. Factorial
 * 3. Greatest Common Denominator (GCD)
 * 4. Knight's Tour
 * Objective:
 * Recursion is a problem-solving technique that is used in many 
 * computational problems. Students will explore the impact of 
 * recursion on memory and performance and learn to identify 
 * scenarios where recursive solutions are appropriate.
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountToOne
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Out.Write("Pease entr an integer. I will do some math and eventually arrive at 1: ");
            int startingNumber = int.Parse(Console.ReadLine());
            int x = CountToOne(startingNumber);
            Console.ReadLine();
        }

        //
        // Recursive method
        // Progress until n = 1
        // If n is even, divide n by 2 and recurse
        // If n is odd, add 1 to n and recurse
        //
        static public int CountToOne(int n)
        {
            Console.Out.WriteLine("N is {0}", n);
            if (n == 1)
            {
                // Reached our target/exit point - return
                return 1; 
            }
            else
            {
                if (n % 2 == 0)
                {
                    Console.Out.WriteLine("N is even. Divide by 2");
                    return CountToOne(n / 2);
                }
                else
                {
                    Console.Out.WriteLine("N is odd. Add 1");
                    return CountToOne(n + 1);
                }
            }
        }
    }
}
