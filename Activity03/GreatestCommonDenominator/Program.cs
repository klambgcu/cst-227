﻿using /*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-08-24
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Activity 3
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * Activities to strengthen knowledge and understanding of recursive
 * methods. We have four sections in this assignment, each
 * increasingly challenging. They are:
 * 1. CountToOne
 * 2. Factorial
 * 3. Greatest Common Denominator (GCD)
 * 4. Knight's Tour
 * Objective:
 * Recursion is a problem-solving technique that is used in many 
 * computational problems. Students will explore the impact of 
 * recursion on memory and performance and learn to identify 
 * scenarios where recursive solutions are appropriate.
 * ---------------------------------------------------------------
 */

System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreatestCommonDenominator
{
    class Program
    {
        static void Main(string[] args)
        {
            int number1 = 102425;
            int number2 = 51275;
            int answer = gcd(number1, number2);
            Console.Out.WriteLine("The gcd of {0} and {1} is {2}", number1, number2, answer);
            Console.ReadLine();
        }

        static int gcd(int n1, int n2)
        {
            if (n2 == 0)
            {
                return n1;
            }
            else
            {
                Console.Out.WriteLine("Not yet. Remainder is {0}", n1 % n2);
                return gcd(n2, n1 % n2);
            }
        }
    }
}
