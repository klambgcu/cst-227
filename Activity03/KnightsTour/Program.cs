﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-08-24
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Activity 3
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * Activities to strengthen knowledge and understanding of recursive
 * methods. We have four sections in this assignment, each
 * increasingly challenging. They are:
 * 1. CountToOne
 * 2. Factorial
 * 3. Greatest Common Denominator (GCD)
 * 4. Knight's Tour
 * Objective:
 * Recursion is a problem-solving technique that is used in many 
 * computational problems. Students will explore the impact of 
 * recursion on memory and performance and learn to identify 
 * scenarios where recursive solutions are appropriate.
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnightsTour
{
    class Program
    {
        static int BoardSize = 8;
        static int attemptedMoves = 0;

        // xMove[] and yMove[] defne next move of Knight
        // xMove[] is the for next value of x coordinate
        // yMove[] is the for next value of the y coordinate
        static int[] xMove = { 2, 1, -1, -2, -2, -1, 1, 2 };
        static int[] yMove = { 1, 2, 2, 1, -1, -2, -2, -1 };

        // boardGrid is an 8x8 array that contains -1 for an 
        // unvisited square or a move number between 0 and 63.
        static int[,] boardGrid = new int[BoardSize, BoardSize];

        // Driver Code
        static void Main(string[] args)
        {
            solveKT();
            Console.ReadLine();
        }

        /*
         * This function solves the Knight Tour problem using backtracking.
         * This function uses solveKTUtil() to solve the problem. It returns
         * if no complete tour is possible, otherwise it returns true and 
         * prints the tour. Please note that there may be more than one solution.
         */
         static void solveKT()
        {
            // Initialization of solution matrix. alue of -1 means "not visited yet"
            for (int x = 0; x < BoardSize; x++)
                for (int y = 0; y < BoardSize; y++)
                    boardGrid[x, y] = -1;

            // set starting position for the knight
            int startX = 3;
            int startY = 4;
            boardGrid[startX, startY] = 0;

            // Count the total nuber of guesses
            attemptedMoves = 0;

            // Explore all tours using solveKTUtil()
            if (!solveKTUtil(startX, startY, 1))
            {
                Console.Out.WriteLine("Solution does not exist for {0} {1}", startX, startY);
            }
            else
            {
                printSolution(boardGrid);
                Console.Out.WriteLine("Total attempted moves {0}", attemptedMoves);
            }
        }

        // A recursive utility function to solve Knight Tour problem
        static bool solveKTUtil(int x, int y, int moveCount)
        {
            attemptedMoves++;
            if (attemptedMoves % 1000000 == 0) Console.Out.WriteLine("Attempts: {0}", attemptedMoves);

            int k, next_x, next_y;

            // Check to see if we have reached a solution. 64 = moveCount
            if (moveCount == BoardSize * BoardSize)
                return true;

            // Try all next moves from the current coordinate x, y
            for (k = 0; k < 8; k++)
            {
                next_x = x + xMove[k];
                next_y = y + yMove[k];

                if (isSquareSafe(next_x, next_y))
                {
                    boardGrid[next_x, next_y] = moveCount;
                    if (solveKTUtil(next_x, next_y, moveCount + 1))
                        return true;
                    else
                    {
                        // Backtracking
                        boardGrid[next_x, next_y] = -1;
                    }
                }
            }
            return false;
        }

        // A utility function to check if i,j are valid indexes for N*N chessboard
        static bool isSquareSafe(int x, int y)
        {
            return (x >= 0 && x < BoardSize && 
                    y >= 0 && y < BoardSize &&
                    boardGrid[x, y] == -1);
        }

        // A utility function to print the solution matrix sol[N][N]
        static void printSolution(int[,] solution)
        {
            for (int x = 0; x < BoardSize; x++)
            {
                for (int y = 0; y < BoardSize; y++)
                    Console.Out.Write(" {0,3} ", solution[x, y]);

                Console.Out.WriteLine();
            }
        }
    }
}
