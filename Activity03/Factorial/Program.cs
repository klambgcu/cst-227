﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-08-24
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Activity 3
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * Activities to strengthen knowledge and understanding of recursive
 * methods. We have four sections in this assignment, each
 * increasingly challenging. They are:
 * 1. CountToOne
 * 2. Factorial
 * 3. Greatest Common Denominator (GCD)
 * 4. Knight's Tour
 * Objective:
 * Recursion is a problem-solving technique that is used in many 
 * computational problems. Students will explore the impact of 
 * recursion on memory and performance and learn to identify 
 * scenarios where recursive solutions are appropriate.
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factorial
{
    class Program
    {
        static void Main(string[] args)
        {
            int startingNumber = 17;
            Console.Out.WriteLine(factorial(startingNumber));
            Console.ReadLine();
        }

        //
        // Recursively call method, reducing parameter by 1
        // until it reaches 1. Each interation, we multiply
        // the value by the previous value and return the
        // results: 6! = 6x5x4x3x2x1 = 720.
        //
        static int factorial(int x)
        {
            Console.Out.WriteLine("x is {0}", x);
            if (x == 1)
            {
                return 1;
            }
            else
            {
                return x * factorial(x - 1);
            }
        }
    }
}
