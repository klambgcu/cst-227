﻿namespace StopWatch
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btn_Start = new System.Windows.Forms.Button();
            this.btn_Stop = new System.Windows.Forms.Button();
            this.btn_Reset = new System.Windows.Forms.Button();
            this.lbl_TimeValue = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lbl_Time = new System.Windows.Forms.Label();
            this.lbl_Hits = new System.Windows.Forms.Label();
            this.lbl_HitsValue = new System.Windows.Forms.Label();
            this.lbl_Miss = new System.Windows.Forms.Label();
            this.lbl_MissValue = new System.Windows.Forms.Label();
            this.lbl_High = new System.Windows.Forms.Label();
            this.lbl_HighValue = new System.Windows.Forms.Label();
            this.pb_Bomb = new System.Windows.Forms.PictureBox();
            this.pb_Target = new System.Windows.Forms.PictureBox();
            this.lbl_Level = new System.Windows.Forms.Label();
            this.lbl_LevelValue = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Bomb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Target)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_Start
            // 
            this.btn_Start.BackColor = System.Drawing.Color.Lime;
            this.btn_Start.Location = new System.Drawing.Point(39, 524);
            this.btn_Start.Name = "btn_Start";
            this.btn_Start.Size = new System.Drawing.Size(75, 36);
            this.btn_Start.TabIndex = 0;
            this.btn_Start.Text = "Start";
            this.btn_Start.UseVisualStyleBackColor = false;
            this.btn_Start.Click += new System.EventHandler(this.btn_Start_Click);
            // 
            // btn_Stop
            // 
            this.btn_Stop.BackColor = System.Drawing.Color.Red;
            this.btn_Stop.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Stop.ForeColor = System.Drawing.Color.White;
            this.btn_Stop.Location = new System.Drawing.Point(259, 524);
            this.btn_Stop.Name = "btn_Stop";
            this.btn_Stop.Size = new System.Drawing.Size(75, 36);
            this.btn_Stop.TabIndex = 1;
            this.btn_Stop.Text = "Stop";
            this.btn_Stop.UseVisualStyleBackColor = false;
            this.btn_Stop.Click += new System.EventHandler(this.btn_Stop_Click);
            // 
            // btn_Reset
            // 
            this.btn_Reset.BackColor = System.Drawing.Color.Yellow;
            this.btn_Reset.Location = new System.Drawing.Point(479, 524);
            this.btn_Reset.Name = "btn_Reset";
            this.btn_Reset.Size = new System.Drawing.Size(75, 36);
            this.btn_Reset.TabIndex = 2;
            this.btn_Reset.Text = "Reset";
            this.btn_Reset.UseVisualStyleBackColor = false;
            this.btn_Reset.Click += new System.EventHandler(this.btn_Reset_Click);
            // 
            // lbl_TimeValue
            // 
            this.lbl_TimeValue.AutoSize = true;
            this.lbl_TimeValue.Location = new System.Drawing.Point(36, 3);
            this.lbl_TimeValue.Name = "lbl_TimeValue";
            this.lbl_TimeValue.Size = new System.Drawing.Size(49, 13);
            this.lbl_TimeValue.TabIndex = 3;
            this.lbl_TimeValue.Text = "00:00:00";
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lbl_Time
            // 
            this.lbl_Time.AutoSize = true;
            this.lbl_Time.Location = new System.Drawing.Point(3, 3);
            this.lbl_Time.Name = "lbl_Time";
            this.lbl_Time.Size = new System.Drawing.Size(33, 13);
            this.lbl_Time.TabIndex = 5;
            this.lbl_Time.Text = "Time:";
            // 
            // lbl_Hits
            // 
            this.lbl_Hits.AutoSize = true;
            this.lbl_Hits.Location = new System.Drawing.Point(469, 3);
            this.lbl_Hits.Name = "lbl_Hits";
            this.lbl_Hits.Size = new System.Drawing.Size(28, 13);
            this.lbl_Hits.TabIndex = 6;
            this.lbl_Hits.Text = "Hits:";
            // 
            // lbl_HitsValue
            // 
            this.lbl_HitsValue.AutoSize = true;
            this.lbl_HitsValue.Location = new System.Drawing.Point(498, 3);
            this.lbl_HitsValue.Name = "lbl_HitsValue";
            this.lbl_HitsValue.Size = new System.Drawing.Size(13, 13);
            this.lbl_HitsValue.TabIndex = 7;
            this.lbl_HitsValue.Text = "0";
            // 
            // lbl_Miss
            // 
            this.lbl_Miss.AutoSize = true;
            this.lbl_Miss.Location = new System.Drawing.Point(524, 3);
            this.lbl_Miss.Name = "lbl_Miss";
            this.lbl_Miss.Size = new System.Drawing.Size(31, 13);
            this.lbl_Miss.TabIndex = 8;
            this.lbl_Miss.Text = "Miss:";
            // 
            // lbl_MissValue
            // 
            this.lbl_MissValue.AutoSize = true;
            this.lbl_MissValue.Location = new System.Drawing.Point(561, 3);
            this.lbl_MissValue.Name = "lbl_MissValue";
            this.lbl_MissValue.Size = new System.Drawing.Size(13, 13);
            this.lbl_MissValue.TabIndex = 9;
            this.lbl_MissValue.Text = "0";
            // 
            // lbl_High
            // 
            this.lbl_High.AutoSize = true;
            this.lbl_High.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_High.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_High.Location = new System.Drawing.Point(264, 3);
            this.lbl_High.Name = "lbl_High";
            this.lbl_High.Size = new System.Drawing.Size(51, 20);
            this.lbl_High.TabIndex = 10;
            this.lbl_High.Text = "High:";
            // 
            // lbl_HighValue
            // 
            this.lbl_HighValue.AutoSize = true;
            this.lbl_HighValue.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_HighValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_HighValue.Location = new System.Drawing.Point(315, 3);
            this.lbl_HighValue.Name = "lbl_HighValue";
            this.lbl_HighValue.Size = new System.Drawing.Size(19, 20);
            this.lbl_HighValue.TabIndex = 11;
            this.lbl_HighValue.Text = "0";
            // 
            // pb_Bomb
            // 
            this.pb_Bomb.Image = global::StopWatch.Properties.Resources.bomb;
            this.pb_Bomb.Location = new System.Drawing.Point(239, 197);
            this.pb_Bomb.Name = "pb_Bomb";
            this.pb_Bomb.Size = new System.Drawing.Size(100, 100);
            this.pb_Bomb.TabIndex = 13;
            this.pb_Bomb.TabStop = false;
            this.pb_Bomb.Click += new System.EventHandler(this.pb_Bomb_Click);
            // 
            // pb_Target
            // 
            this.pb_Target.Image = global::StopWatch.Properties.Resources.mole;
            this.pb_Target.Location = new System.Drawing.Point(239, 70);
            this.pb_Target.Name = "pb_Target";
            this.pb_Target.Size = new System.Drawing.Size(100, 100);
            this.pb_Target.TabIndex = 12;
            this.pb_Target.TabStop = false;
            this.pb_Target.Click += new System.EventHandler(this.pb_Target_Click);
            // 
            // lbl_Level
            // 
            this.lbl_Level.AutoSize = true;
            this.lbl_Level.Location = new System.Drawing.Point(106, 3);
            this.lbl_Level.Name = "lbl_Level";
            this.lbl_Level.Size = new System.Drawing.Size(36, 13);
            this.lbl_Level.TabIndex = 14;
            this.lbl_Level.Text = "Level:";
            // 
            // lbl_LevelValue
            // 
            this.lbl_LevelValue.AutoSize = true;
            this.lbl_LevelValue.Location = new System.Drawing.Point(148, 3);
            this.lbl_LevelValue.Name = "lbl_LevelValue";
            this.lbl_LevelValue.Size = new System.Drawing.Size(13, 13);
            this.lbl_LevelValue.TabIndex = 15;
            this.lbl_LevelValue.Text = "1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(584, 561);
            this.Controls.Add(this.lbl_LevelValue);
            this.Controls.Add(this.lbl_Level);
            this.Controls.Add(this.pb_Bomb);
            this.Controls.Add(this.pb_Target);
            this.Controls.Add(this.lbl_HighValue);
            this.Controls.Add(this.lbl_High);
            this.Controls.Add(this.lbl_MissValue);
            this.Controls.Add(this.lbl_Miss);
            this.Controls.Add(this.lbl_HitsValue);
            this.Controls.Add(this.lbl_Hits);
            this.Controls.Add(this.lbl_Time);
            this.Controls.Add(this.lbl_TimeValue);
            this.Controls.Add(this.btn_Reset);
            this.Controls.Add(this.btn_Stop);
            this.Controls.Add(this.btn_Start);
            this.Name = "Form1";
            this.Text = "Whack A Mole";
            this.Click += new System.EventHandler(this.Form1_Click);
            ((System.ComponentModel.ISupportInitialize)(this.pb_Bomb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Target)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Start;
        private System.Windows.Forms.Button btn_Stop;
        private System.Windows.Forms.Button btn_Reset;
        private System.Windows.Forms.Label lbl_TimeValue;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lbl_Time;
        private System.Windows.Forms.Label lbl_Hits;
        private System.Windows.Forms.Label lbl_HitsValue;
        private System.Windows.Forms.Label lbl_Miss;
        private System.Windows.Forms.Label lbl_MissValue;
        private System.Windows.Forms.Label lbl_High;
        private System.Windows.Forms.Label lbl_HighValue;
        private System.Windows.Forms.PictureBox pb_Target;
        private System.Windows.Forms.PictureBox pb_Bomb;
        private System.Windows.Forms.Label lbl_Level;
        private System.Windows.Forms.Label lbl_LevelValue;
    }
}

