﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-08-31
 * Class     : CST-227 Enterprise Computer Promgramming II
 * Professor : James Shinevar
 * Assignment: Activity 5
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create GUI Stopwatch 
 * 2. Modify it to Whack A Mole (Easy version) 
 * 3. Extend game to playable mode (Bells & Whistles) 
 * Objective:
 * Stopwatch and Timer functionality
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace StopWatch
{
    public partial class Form1 : Form
    {
        private Color[] gameColors = { Color.White, Color.Lavender, Color.LightBlue, Color.LightCoral, Color.Yellow, Color.Orange, Color.Red, Color.Purple, Color.Cyan};
        public static Stopwatch watch = new Stopwatch();
        private Random random = new Random();
        private int hits = 0;
        private int miss = 0;
        private int high = 0;
        private int level = 1;
        private int levelSpeed = 1000;
        private bool GameRunning = true;

        public Form1()
        {
            InitializeComponent();
            // Display the form in the center of the screen.
            StartPosition = FormStartPosition.CenterScreen;
        }

        // Get the game started
        private void btn_Start_Click(object sender, EventArgs e)
        {
            GameStart();
        }

        // Stop Game/Timer/Stopwatch
        private void btn_Stop_Click(object sender, EventArgs e)
        {
            watch.Stop();
            timer1.Enabled = false;
            pb_Bomb.Visible = false;
            pb_Target.Visible = false;
            GameRunning = false;
        }

        // Reset Game/Timer/Stopwatch
        private void btn_Reset_Click(object sender, EventArgs e)
        {
            Reset();
        }

        // Timed event to move/place game objects, update time
        private void timer1_Tick(object sender, EventArgs e)
        {
            // Apply a TimeSpan to format the output 
            TimeSpan ts = watch.Elapsed;

            // Use String.Format option for easy reading
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}",
                                                ts.Hours, ts.Minutes, ts.Seconds);

            lbl_TimeValue.Text = elapsedTime;

            // Random placement for the target button
            if (random.Next(0,10) < 8)
            {
                pb_Target.Left = random.Next(0, this.ClientSize.Width - pb_Target.Width);
                pb_Target.Top = random.Next(0, this.ClientSize.Height - pb_Target.Height);
                pb_Target.Visible = true;
            }

            // Random placement for the bomb button
            if (random.Next(0, 10) <= 7)
            {
                if (pb_Bomb.Visible)
                {
                    pb_Bomb.Visible = false; // Toggle show/hide bomb randomly
                }
                else
                {
                    pb_Bomb.Left = random.Next(0, this.ClientSize.Width - pb_Bomb.Width);
                    pb_Bomb.Top = random.Next(0, this.ClientSize.Height - pb_Bomb.Height);
                    pb_Bomb.Visible = true;
                }
            }
        }

        // Tally hits on target - increase level every 10 hits
        private void pb_Target_Click(object sender, EventArgs e)
        {
            pb_Target.Visible = false;
            hits++;
            lbl_HitsValue.Text = hits.ToString();
            if (hits > high)
            {
                high = hits;
                lbl_HighValue.Text = high.ToString();
            }

            // New level every 10 hits
            if (hits > 0 && (hits % 10) == 0)
            {
                level++;
                lbl_LevelValue.Text = level.ToString();
                levelSpeed -= 100; // new level means quicker movement

                if (levelSpeed < 500) levelSpeed = 500;
                timer1.Interval = levelSpeed;

                // Tell player they reached a new level
                MessageBox.Show("Level: " + level.ToString(), "New Level", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                // Change the form background color to signify new level
                int temp = level;
                if (temp > gameColors.Length - 1)
                    temp = gameColors.Length - 1;
                this.BackColor = gameColors[temp - 1];
            }
        }

        // Tally number of misses - not really important to the game
        // just logging some additional stats.
        private void Form1_Click(object sender, EventArgs e)
        {
            miss++;
            lbl_MissValue.Text = miss.ToString();
        }

        // Game ends if player hits a bomb
        private void pb_Bomb_Click(object sender, EventArgs e)
        {
            GameOver();
        }

        // Provide a game reset option
        private void Reset()
        {
            watch.Reset();
            timer1.Enabled = false;
            lbl_MissValue.Text = "0";
            lbl_HitsValue.Text = "0";
            lbl_LevelValue.Text = "1";
            lbl_TimeValue.Text = "00:00:00";
            hits = 0;
            miss = 0;
            level = 1;
            levelSpeed = 1000;
            timer1.Interval = levelSpeed;
            pb_Bomb.Visible = false;
            pb_Target.Visible = false;
            GameRunning = false;
            this.BackColor = gameColors[0];
        }

        // Set up variables to start the game
        private void GameStart()
        {
            watch.Start();
            timer1.Enabled = true;
            pb_Bomb.Visible = false;
            pb_Target.Visible = false;
            GameRunning = true;
        }

        // Provide a game over / Play Again message
        private void GameOver()
        {
            GameRunning = false;
            timer1.Enabled = false;
            pb_Bomb.Visible = false;
            pb_Target.Visible = false;
            watch.Stop();

            DialogResult result = MessageBox.Show("Score: Hits:" + hits + ", Miss:" + miss + ", Time: " + lbl_TimeValue.Text + "\nPlay Again?",
                                                  "Game Over-Bomb Hit",
                                                  MessageBoxButtons.YesNo,
                                                  MessageBoxIcon.Information);
            Reset();

            if (result == DialogResult.Yes)
            {
                GameStart();
            }
        }
    }
}
