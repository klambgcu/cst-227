﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-08-31
 * Class     : CST-227 Enterprise Computer Promgramming II
 * Professor : James Shinevar
 * Assignment: Activity 5
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create GUI Stopwatch 
 * 2. Modify it to Whack A Mole (Easy version) 
 * 3. Extend game to playable mode (Bells & Whistles) 
 * Objective:
 * Stopwatch and Timer functionality
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StopWatch
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
