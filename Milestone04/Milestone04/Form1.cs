﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-08-31
 * Class     : CST-227 Enterprise Computer Promgramming II
 * Professor : James Shinevar
 * Assignment: Milestone 4
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create GUI forms for the beginning of Minesweeper game
 * 2. Main Form - Select Game options 
 * 3. Game Form - Form with button grid based upon chosen level 
 * Objective:
 * Prepare GUI for transitioning game from console to GUI
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milestone04
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            // Display the form in the center of the screen.
            StartPosition = FormStartPosition.CenterScreen;
        }

        private void btn_PlayGame_Click(object sender, EventArgs e)
        {
            int level = 0;

            if (rb_EasyLevel.Checked) level = 0;
            if (rb_ModerateLevel.Checked) level = 1;
            if (rb_DifficultLevel.Checked) level = 2;

            Form2 gameForm = new Form2(level);
            gameForm.Show();
            //this.Hide();
        }
    }
}
