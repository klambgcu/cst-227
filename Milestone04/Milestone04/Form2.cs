﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-08-31
 * Class     : CST-227 Enterprise Computer Promgramming II
 * Professor : James Shinevar
 * Assignment: Milestone 4
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create GUI forms for the beginning of Minesweeper game
 * 2. Main Form - Select Game options 
 * 3. Game Form - Form with button grid based upon chosen level 
 * Objective:
 * Prepare GUI for transitioning game from console to GUI
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milestone04
{
    public partial class Form2 : Form
    {
        private int[,] gameLevels = { { 10, 10 }, { 15, 15 }, { 20, 20 } }; // Board Size, Bomb Percentage
        private Color[] gameColors = { Color.White, Color.Blue, Color.Green, Color.Yellow, Color.Orange, Color.Red, Color.Purple, Color.Cyan, Color.Brown, Color.White};
        private int bombPercentage = 10; 
        private int buttonSize = 30;
        private int difficultyLevel = 0;
        private int size = 10;
        private Button[,] btnGrid;

        public Form2(int level)
        {
            InitializeComponent();

            difficultyLevel = level;
            size = gameLevels[level, 0];
            bombPercentage = gameLevels[level, 1];
            btnGrid = new Button[size, size];

            // Dynamically set interior of form to hold game board
            ClientSize = new Size(size * buttonSize, size * buttonSize);

            // Display the form in the center of the screen.
            StartPosition = FormStartPosition.CenterScreen;

            populateGrid();
        }

        // Define the game board
        public void populateGrid()
        {
            // Resize panel to a square - could set panel1.autosize
            panel1.Height = (size * buttonSize);
            panel1.Width = (size * buttonSize);

            // Nested loop. Create buttons and place them in the Panel
            for (int r = 0; r < size; r++)
            {
                for (int c = 0; c < size; c++)
                {
                    btnGrid[r, c] = new Button();

                    // Make each button square
                    btnGrid[r, c].Width = buttonSize;
                    btnGrid[r, c].Height = buttonSize;

                    btnGrid[r, c].Click += Grid_Button_Click; // Add the same click event to each button.
                    panel1.Controls.Add(btnGrid[r, c]); // Place the button on the Panel
                    btnGrid[r, c].Location = new Point(buttonSize * r, buttonSize * c); // Position it in x,y

                    // For testing purposes. Remove later.
                    // btnGrid[r, c].Text = r.ToString() + "|" + c.ToString();

                    // The Tag attribute will hold the row and column number in a string
                    btnGrid[r, c].Tag = r.ToString() + "|" + c.ToString();

                    // The Text holds number clicked (rotates 1-9)
                    btnGrid[r, c].Text = "";
                    btnGrid[r, c].Font = new Font(btnGrid[r, c].Font, FontStyle.Bold);
                }
            }
        }

        private void Grid_Button_Click(object sender, EventArgs e)
        {
            // Update button text number and background color
            string btnValue = (sender as Button).Text;
            btnValue = (btnValue.Equals("")) ? "0" : btnValue;

            int newValue = int.Parse(btnValue);
            newValue = (newValue < 9) ? ++newValue : 1; // Clamp values 1-9
            (sender as Button).Text = newValue.ToString();
            (sender as Button).BackColor = gameColors[newValue];
        }
    }
}
