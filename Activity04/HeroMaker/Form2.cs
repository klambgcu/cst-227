﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeroMaker
{
    public partial class Form2 : Form
    {
        SuperHeroList superHeroList;
        BindingSource listBindingSource = new BindingSource();

        public Form2(SuperHeroList heroList)
        {
            InitializeComponent();
            superHeroList = heroList;
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            listBindingSource.DataSource = superHeroList.heroList;
            lb_heroes.DataSource = listBindingSource;
            lb_heroes.DisplayMember = "Name";
        }

        private void lb_heroes_SelectedIndexChanged(object sender, EventArgs e)
        {
            txt_dossier.Text = lb_heroes.SelectedItem.ToString();
            
        }
    }
}
