﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-08-31
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Activity 4
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * Create two forms - main contains all common visual components
 * and create objects and lists of objects pertaining to the
 * items on the form.
 * Objective:
 * Install and configure a wide range of form controls. Manage and
 * read the properties of each kind of control. Create a class
 * with properties that match each type of control. Second goal is
 * to create multiple forms and pass data between them.
 * ---------------------------------------------------------------
 */

namespace HeroMaker
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_heroes = new System.Windows.Forms.Label();
            this.lb_heroes = new System.Windows.Forms.ListBox();
            this.lbl_dossier = new System.Windows.Forms.Label();
            this.txt_dossier = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lbl_heroes
            // 
            this.lbl_heroes.AutoSize = true;
            this.lbl_heroes.Location = new System.Drawing.Point(13, 13);
            this.lbl_heroes.Name = "lbl_heroes";
            this.lbl_heroes.Size = new System.Drawing.Size(44, 13);
            this.lbl_heroes.TabIndex = 0;
            this.lbl_heroes.Text = "Heroes:";
            // 
            // lb_heroes
            // 
            this.lb_heroes.FormattingEnabled = true;
            this.lb_heroes.Location = new System.Drawing.Point(16, 29);
            this.lb_heroes.Name = "lb_heroes";
            this.lb_heroes.Size = new System.Drawing.Size(178, 251);
            this.lb_heroes.TabIndex = 1;
            this.lb_heroes.SelectedIndexChanged += new System.EventHandler(this.lb_heroes_SelectedIndexChanged);
            // 
            // lbl_dossier
            // 
            this.lbl_dossier.AutoSize = true;
            this.lbl_dossier.Location = new System.Drawing.Point(216, 13);
            this.lbl_dossier.Name = "lbl_dossier";
            this.lbl_dossier.Size = new System.Drawing.Size(45, 13);
            this.lbl_dossier.TabIndex = 2;
            this.lbl_dossier.Text = "Dossier:";
            // 
            // txt_dossier
            // 
            this.txt_dossier.AcceptsReturn = true;
            this.txt_dossier.AcceptsTab = true;
            this.txt_dossier.Location = new System.Drawing.Point(219, 29);
            this.txt_dossier.Multiline = true;
            this.txt_dossier.Name = "txt_dossier";
            this.txt_dossier.ReadOnly = true;
            this.txt_dossier.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txt_dossier.Size = new System.Drawing.Size(531, 251);
            this.txt_dossier.TabIndex = 3;
            this.txt_dossier.WordWrap = false;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(770, 308);
            this.Controls.Add(this.txt_dossier);
            this.Controls.Add(this.lbl_dossier);
            this.Controls.Add(this.lb_heroes);
            this.Controls.Add(this.lbl_heroes);
            this.Name = "Form2";
            this.Text = "Hero List Form";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_heroes;
        private System.Windows.Forms.ListBox lb_heroes;
        private System.Windows.Forms.Label lbl_dossier;
        private System.Windows.Forms.TextBox txt_dossier;
    }
}