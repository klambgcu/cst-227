﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-08-31
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Activity 4
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * Create two forms - main contains all common visual components
 * and create objects and lists of objects pertaining to the
 * items on the form.
 * Objective:
 * Install and configure a wide range of form controls. Manage and
 * read the properties of each kind of control. Create a class
 * with properties that match each type of control. Second goal is
 * to create multiple forms and pass data between them.
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeroMaker
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
