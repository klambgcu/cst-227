﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-08-31
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Activity 4
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * Create two forms - main contains all common visual components
 * and create objects and lists of objects pertaining to the
 * items on the form.
 * Objective:
 * Install and configure a wide range of form controls. Manage and
 * read the properties of each kind of control. Create a class
 * with properties that match each type of control. Second goal is
 * to create multiple forms and pass data between them.
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroMaker
{
    // Our Super Hero class implements IComparable 
    // so we can sort it by name if needed :)
    public class SuperHero : IComparable
    {
        public SuperHero() {  }
        public SuperHero(string name, bool[] specialAbilities, List<string> officeLocations, 
                         string preferredTransport, int speed, int stamina, int strength, 
                         DateTime birthdate, DateTime superPowerDiscoveryDate, DateTime fatefulDate,
                         decimal yearsExperience, string capeColor, int darkSidePropensity, string portrait)
        {
            Name = name;
            SpecialAbilities = specialAbilities;
            OfficeLocations = officeLocations;
            PreferredTransport = preferredTransport;
            Speed = speed;
            Stamina = stamina;
            Strength = strength;
            Birthdate = birthdate;
            SuperPowerDiscoveryDate = superPowerDiscoveryDate;
            FatefulDate = fatefulDate;
            YearsExperience = yearsExperience;
            CapeColor = capeColor;
            DarkSidePropensity = darkSidePropensity;
            Portrait = portrait;
        }

        public string Name { get; set; }
        public bool[] SpecialAbilities { get; set; }
        public List<string> OfficeLocations { get; set; }
        public string PreferredTransport { get; set; }
        public int Speed { get; set; }
        public int Stamina { get; set; }
        public int Strength { get; set; }
        public DateTime Birthdate { get; set; }
        public DateTime SuperPowerDiscoveryDate { get; set; }
        public DateTime FatefulDate { get; set; }
        public decimal YearsExperience { get; set; }
        public string CapeColor { get; set; }
        public int DarkSidePropensity { get; set; }
        public string Portrait { get; set; }

        // Sort by name
        public int CompareTo(object obj)
        {
            return Name.CompareTo( ((SuperHero)obj).Name );
        }

        // Override ToString to more fully represent our hero
        // Using AppendLine instead of Append("\n") so it works
        // in the multiple line text box correctly
        public override string ToString()
        {
            StringBuilder results = new StringBuilder();

            results.AppendLine("Hero's Name: " + Name + "");

            results.AppendLine("Special Abilities: ");
            if (SpecialAbilities[0]) results.AppendLine("  Super Strength");
            if (SpecialAbilities[1]) results.AppendLine("  Fly");
            if (SpecialAbilities[2]) results.AppendLine("  X-Ray Vision");
            if (SpecialAbilities[3]) results.AppendLine("  Invisibility");
            if (SpecialAbilities[4]) results.AppendLine("  Absorb Energy");
            if (SpecialAbilities[5]) results.AppendLine("  Telepathy");
            if (SpecialAbilities[6]) results.AppendLine("  Extreme Luck");
            if (SpecialAbilities[7]) results.AppendLine("  Force Field");
            if (SpecialAbilities[8]) results.AppendLine("  Explosive Farts");
            if (SpecialAbilities[9]) results.AppendLine("  Insect Control");
            if (SpecialAbilities[10]) results.AppendLine("  Water Breathing");
            if (SpecialAbilities[11]) results.AppendLine("  Time Changer");

            results.AppendLine("Office Location Cities:");
            foreach (string city in OfficeLocations)
            {
                results.AppendLine("  " + city + "");
            }

            results.AppendLine("Preferred Transport: " + PreferredTransport + "");

            results.AppendLine("Speed: " + Speed.ToString() + "");
            results.AppendLine("Stamina: " + Stamina.ToString() + "");
            results.AppendLine("Strength: " + Strength.ToString() + "");

            results.AppendLine("Birthday: " + Birthdate.ToString() + "");
            results.AppendLine("Super Power Discovery Date: " + SuperPowerDiscoveryDate.ToString() + "");
            results.AppendLine("Fateful Date: " + FatefulDate.ToString() + "");

            results.AppendLine("Years of Experience: " + YearsExperience.ToString() + "");
            results.AppendLine("Cape Color: " + CapeColor + "");
            results.AppendLine("Dark Side Propensity: " + DarkSidePropensity.ToString() + "");
            results.AppendLine("Portrait: " + Portrait + "");

            return results.ToString();
        }
    }
}
