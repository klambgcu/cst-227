﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-08-31
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Activity 4
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * Create two forms - main contains all common visual components
 * and create objects and lists of objects pertaining to the
 * items on the form.
 * Objective:
 * Install and configure a wide range of form controls. Manage and
 * read the properties of each kind of control. Create a class
 * with properties that match each type of control. Second goal is
 * to create multiple forms and pass data between them.
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeroMaker
{
    public partial class Form1 : Form
    {
        // Required to save filename location for hero creation
        string heroPortrait = "";

        SuperHeroList superHeroList = new SuperHeroList();

        public Form1()
        {
            InitializeComponent();
        }

        // Helper to set scroll bars evenly for combined value of 100
        private void setScrolls100(Label currentLabel, HScrollBar current,
                                   Label other1Label, HScrollBar other1,
                                   Label other2Label, HScrollBar other2)
        {
            int prevValue = int.Parse(currentLabel.Text);
            int currValue = current.Value;
            currentLabel.Text = currValue.ToString();
            int diff = currValue - prevValue;
            int diff2 = diff / 2;
            bool odd = ((diff & 1) == 1);
            int o1 = other1.Value - diff2;
            int o2 = other2.Value - diff2;

            if (odd) // Handle odd size movements
            {
                if (Math.Sign(diff) == -1)
                {
                    if (o1 < o2)
                        o1++;
                    else
                        o2++;
                }
                else
                {
                    if (o1 > o2)
                        o1--;
                    else
                        o2--;
                }
            }

            // Handle case when one is to small to handle diff2 (i.e negative)
            if (o1 < 0) { o2 += o1; o1 = 0; }
            if (o2 < 0) { o1 += o2; o2 = 0; }

            // Clamp others to correct range
            o1 = (o1 > 100) ? 100 : (o1 < 0) ? 0 : o1;
            o2 = (o2 > 100) ? 100 : (o2 < 0) ? 0 : o2;

            // Assign values to other scrollbars
            other1Label.Text = o1.ToString();
            other1.Value = o1;

            other2Label.Text = o2.ToString();
            other2.Value = o2;
        }
        private void sb_speed_Scroll(object sender, ScrollEventArgs e)
        {
            setScrolls100(lbl_SpeedValue, sb_Speed, lbl_StaminaValue, sb_Stamina, lbl_StrengthValue, sb_Strength);


        }

        private void sb_stamina_Scroll(object sender, ScrollEventArgs e)
        {
            setScrolls100(lbl_StaminaValue, sb_Stamina, lbl_StrengthValue, sb_Strength, lbl_SpeedValue, sb_Speed);
        }

        private void sb_strength_Scroll(object sender, ScrollEventArgs e)
        {
            setScrolls100(lbl_StrengthValue, sb_Strength, lbl_SpeedValue, sb_Speed, lbl_StaminaValue, sb_Stamina);
        }

        private void pb_capecolor_Click(object sender, EventArgs e)
        {
            ColorDialog colorDialog = new ColorDialog();
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                pb_CapeColor.BackColor = colorDialog.Color;
            }
        }

        private void tb_darksidepropensity_Scroll(object sender, EventArgs e)
        {
            lbl_DarkSidePropensityValue.Text = tb_DarkSidePropensity.Value.ToString();
        }

        private void pb_portrait_Click(object sender, EventArgs e)
        {
            // Open a file dialog   
            OpenFileDialog open = new OpenFileDialog();

            // Apply image filters  
            open.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";

            if (open.ShowDialog() == DialogResult.OK)
            {
                // Display the image in the picture box  
                pb_Portrait.Image = new Bitmap(open.FileName);
                heroPortrait = open.FileName;
            }
        }

        private void btn_createme_Click(object sender, EventArgs e)
        {
            // Gather information from the components to create a SuperHero object
            string name = tb_HerosName.Text;

            // Note: Must change these if you change the form
            bool[] abilities = new bool[12];
            abilities[0] = cb_SuperStrength.Checked;
            abilities[1] = cb_Fly.Checked;
            abilities[2] = cb_XrayVision.Checked;
            abilities[3] = cb_Invisibility.Checked;
            abilities[4] = cb_AbsorbEnergy.Checked;
            abilities[5] = cb_Telepathy.Checked;
            abilities[6] = cb_ExtremeLuck.Checked;
            abilities[7] = cb_ForceField.Checked;
            abilities[8] = cb_ExplosiveFarts.Checked;
            abilities[9] = cb_InsectControl.Checked;
            abilities[10] = cb_WaterBreathing.Checked;
            abilities[11] = cb_TimeChanger.Checked;

            // Multiple options allowed for Locations
            List<string> officeLocations = new List<string>();
            foreach(string s in lb_OfficeLocations.SelectedItems)
            {
                officeLocations.Add(s);
            }

            string preferredTransport = "";
            if (rb_JetPack.Checked) preferredTransport = "Jet Pack";
            if (rb_LandSpeeder.Checked) preferredTransport = "Landspeeder";
            if (rb_Teleport.Checked) preferredTransport = "Teleport";
            if (rb_Batmobile.Checked) preferredTransport = "Batmobile";

            // Store SSS
            int speed = sb_Speed.Value;
            int stamina = sb_Stamina.Value;
            int strength = sb_Strength.Value;

            // Store special dates
            DateTime birthday = dtp_Birthday.Value;
            DateTime superPowerDiscoveryDate = dtp_SuperPowerDiscovery.Value;
            DateTime fatefulDate = dtp_FatefulDay.Value;

            decimal yearsExperience = nud_YearsExperience.Value;
            string capeColor = pb_CapeColor.BackColor.ToString();
            int darkSidePropensity = tb_DarkSidePropensity.Value;

            if (speed + stamina + strength > 100)
            {
                MessageBox.Show("Speed, Stamina, and Strength cannot exceed 100 combined. Please readjust settings.",
                                "Change Settings", 
                                MessageBoxButtons.OK, 
                                MessageBoxIcon.Exclamation);
            }
            else
            {
                SuperHero hero = new SuperHero(name, abilities, officeLocations, preferredTransport, 
                                               speed, stamina, strength,
                                               birthday, superPowerDiscoveryDate, fatefulDate,
                                               yearsExperience,capeColor,darkSidePropensity,heroPortrait);

                superHeroList.heroList.Add(hero);

                Form2 f2 = new Form2(superHeroList);
                f2.ShowDialog();
            }
        }
    }
}
