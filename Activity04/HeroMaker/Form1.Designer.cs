﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-08-31
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Activity 4
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * Create two forms - main contains all common visual components
 * and create objects and lists of objects pertaining to the
 * items on the form.
 * Objective:
 * Install and configure a wide range of form controls. Manage and
 * read the properties of each kind of control. Create a class
 * with properties that match each type of control. Second goal is
 * to create multiple forms and pass data between them.
 * ---------------------------------------------------------------
 */

namespace HeroMaker
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_HerosName = new System.Windows.Forms.Label();
            this.tb_HerosName = new System.Windows.Forms.TextBox();
            this.gb_SpecialAbilities = new System.Windows.Forms.GroupBox();
            this.cb_TimeChanger = new System.Windows.Forms.CheckBox();
            this.cb_WaterBreathing = new System.Windows.Forms.CheckBox();
            this.cb_InsectControl = new System.Windows.Forms.CheckBox();
            this.cb_ExplosiveFarts = new System.Windows.Forms.CheckBox();
            this.cb_ForceField = new System.Windows.Forms.CheckBox();
            this.cb_ExtremeLuck = new System.Windows.Forms.CheckBox();
            this.cb_Telepathy = new System.Windows.Forms.CheckBox();
            this.cb_AbsorbEnergy = new System.Windows.Forms.CheckBox();
            this.cb_Invisibility = new System.Windows.Forms.CheckBox();
            this.cb_XrayVision = new System.Windows.Forms.CheckBox();
            this.cb_Fly = new System.Windows.Forms.CheckBox();
            this.cb_SuperStrength = new System.Windows.Forms.CheckBox();
            this.lbl_OfficeLocations = new System.Windows.Forms.Label();
            this.lb_OfficeLocations = new System.Windows.Forms.ListBox();
            this.gb_PreferredTransport = new System.Windows.Forms.GroupBox();
            this.rb_Batmobile = new System.Windows.Forms.RadioButton();
            this.rb_Teleport = new System.Windows.Forms.RadioButton();
            this.rb_LandSpeeder = new System.Windows.Forms.RadioButton();
            this.rb_JetPack = new System.Windows.Forms.RadioButton();
            this.gb_SpeedStaminaStrength = new System.Windows.Forms.GroupBox();
            this.lbl_StrengthValue = new System.Windows.Forms.Label();
            this.lbl_StaminaValue = new System.Windows.Forms.Label();
            this.lbl_SpeedValue = new System.Windows.Forms.Label();
            this.lbl_Strength = new System.Windows.Forms.Label();
            this.lbl_Stamina = new System.Windows.Forms.Label();
            this.lbl_Speed = new System.Windows.Forms.Label();
            this.sb_Strength = new System.Windows.Forms.HScrollBar();
            this.sb_Stamina = new System.Windows.Forms.HScrollBar();
            this.sb_Speed = new System.Windows.Forms.HScrollBar();
            this.gb_ImportantDates = new System.Windows.Forms.GroupBox();
            this.dtp_FatefulDay = new System.Windows.Forms.DateTimePicker();
            this.dtp_SuperPowerDiscovery = new System.Windows.Forms.DateTimePicker();
            this.dtp_Birthday = new System.Windows.Forms.DateTimePicker();
            this.lbl_FatefulDay = new System.Windows.Forms.Label();
            this.lbl_SuperPowerDiscovery = new System.Windows.Forms.Label();
            this.lbl_Birthday = new System.Windows.Forms.Label();
            this.lbl_YearsExperience = new System.Windows.Forms.Label();
            this.nud_YearsExperience = new System.Windows.Forms.NumericUpDown();
            this.lbl_CapeColor = new System.Windows.Forms.Label();
            this.pb_CapeColor = new System.Windows.Forms.PictureBox();
            this.lbl_DarkSidePropensity = new System.Windows.Forms.Label();
            this.tb_DarkSidePropensity = new System.Windows.Forms.TrackBar();
            this.lbl_Portrait = new System.Windows.Forms.Label();
            this.pb_Portrait = new System.Windows.Forms.PictureBox();
            this.lbl_DarkSidePropensityValue = new System.Windows.Forms.Label();
            this.btn_CreatMe = new System.Windows.Forms.Button();
            this.gb_SpecialAbilities.SuspendLayout();
            this.gb_PreferredTransport.SuspendLayout();
            this.gb_SpeedStaminaStrength.SuspendLayout();
            this.gb_ImportantDates.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_YearsExperience)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_CapeColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb_DarkSidePropensity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Portrait)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_HerosName
            // 
            this.lbl_HerosName.AutoSize = true;
            this.lbl_HerosName.Location = new System.Drawing.Point(13, 13);
            this.lbl_HerosName.Name = "lbl_HerosName";
            this.lbl_HerosName.Size = new System.Drawing.Size(71, 13);
            this.lbl_HerosName.TabIndex = 0;
            this.lbl_HerosName.Text = "Hero\'s Name:";
            // 
            // tb_HerosName
            // 
            this.tb_HerosName.Location = new System.Drawing.Point(87, 10);
            this.tb_HerosName.Name = "tb_HerosName";
            this.tb_HerosName.Size = new System.Drawing.Size(213, 20);
            this.tb_HerosName.TabIndex = 1;
            // 
            // gb_SpecialAbilities
            // 
            this.gb_SpecialAbilities.Controls.Add(this.cb_TimeChanger);
            this.gb_SpecialAbilities.Controls.Add(this.cb_WaterBreathing);
            this.gb_SpecialAbilities.Controls.Add(this.cb_InsectControl);
            this.gb_SpecialAbilities.Controls.Add(this.cb_ExplosiveFarts);
            this.gb_SpecialAbilities.Controls.Add(this.cb_ForceField);
            this.gb_SpecialAbilities.Controls.Add(this.cb_ExtremeLuck);
            this.gb_SpecialAbilities.Controls.Add(this.cb_Telepathy);
            this.gb_SpecialAbilities.Controls.Add(this.cb_AbsorbEnergy);
            this.gb_SpecialAbilities.Controls.Add(this.cb_Invisibility);
            this.gb_SpecialAbilities.Controls.Add(this.cb_XrayVision);
            this.gb_SpecialAbilities.Controls.Add(this.cb_Fly);
            this.gb_SpecialAbilities.Controls.Add(this.cb_SuperStrength);
            this.gb_SpecialAbilities.Location = new System.Drawing.Point(16, 43);
            this.gb_SpecialAbilities.Name = "gb_SpecialAbilities";
            this.gb_SpecialAbilities.Size = new System.Drawing.Size(284, 162);
            this.gb_SpecialAbilities.TabIndex = 2;
            this.gb_SpecialAbilities.TabStop = false;
            this.gb_SpecialAbilities.Text = " Special Abilities ";
            // 
            // cb_TimeChanger
            // 
            this.cb_TimeChanger.AutoSize = true;
            this.cb_TimeChanger.Location = new System.Drawing.Point(143, 135);
            this.cb_TimeChanger.Name = "cb_TimeChanger";
            this.cb_TimeChanger.Size = new System.Drawing.Size(92, 17);
            this.cb_TimeChanger.TabIndex = 11;
            this.cb_TimeChanger.Text = "Time Changer";
            this.cb_TimeChanger.UseVisualStyleBackColor = true;
            // 
            // cb_WaterBreathing
            // 
            this.cb_WaterBreathing.AutoSize = true;
            this.cb_WaterBreathing.Location = new System.Drawing.Point(143, 112);
            this.cb_WaterBreathing.Name = "cb_WaterBreathing";
            this.cb_WaterBreathing.Size = new System.Drawing.Size(103, 17);
            this.cb_WaterBreathing.TabIndex = 10;
            this.cb_WaterBreathing.Text = "Water Breathing";
            this.cb_WaterBreathing.UseVisualStyleBackColor = true;
            // 
            // cb_InsectControl
            // 
            this.cb_InsectControl.AutoSize = true;
            this.cb_InsectControl.Location = new System.Drawing.Point(143, 89);
            this.cb_InsectControl.Name = "cb_InsectControl";
            this.cb_InsectControl.Size = new System.Drawing.Size(91, 17);
            this.cb_InsectControl.TabIndex = 9;
            this.cb_InsectControl.Text = "Insect Control";
            this.cb_InsectControl.UseVisualStyleBackColor = true;
            // 
            // cb_ExplosiveFarts
            // 
            this.cb_ExplosiveFarts.AutoSize = true;
            this.cb_ExplosiveFarts.Location = new System.Drawing.Point(143, 66);
            this.cb_ExplosiveFarts.Name = "cb_ExplosiveFarts";
            this.cb_ExplosiveFarts.Size = new System.Drawing.Size(97, 17);
            this.cb_ExplosiveFarts.TabIndex = 8;
            this.cb_ExplosiveFarts.Text = "Explosive Farts";
            this.cb_ExplosiveFarts.UseVisualStyleBackColor = true;
            // 
            // cb_ForceField
            // 
            this.cb_ForceField.AutoSize = true;
            this.cb_ForceField.Location = new System.Drawing.Point(143, 43);
            this.cb_ForceField.Name = "cb_ForceField";
            this.cb_ForceField.Size = new System.Drawing.Size(78, 17);
            this.cb_ForceField.TabIndex = 7;
            this.cb_ForceField.Text = "Force Field";
            this.cb_ForceField.UseVisualStyleBackColor = true;
            // 
            // cb_ExtremeLuck
            // 
            this.cb_ExtremeLuck.AutoSize = true;
            this.cb_ExtremeLuck.Location = new System.Drawing.Point(143, 20);
            this.cb_ExtremeLuck.Name = "cb_ExtremeLuck";
            this.cb_ExtremeLuck.Size = new System.Drawing.Size(91, 17);
            this.cb_ExtremeLuck.TabIndex = 6;
            this.cb_ExtremeLuck.Text = "Extreme Luck";
            this.cb_ExtremeLuck.UseVisualStyleBackColor = true;
            // 
            // cb_Telepathy
            // 
            this.cb_Telepathy.AutoSize = true;
            this.cb_Telepathy.Location = new System.Drawing.Point(7, 135);
            this.cb_Telepathy.Name = "cb_Telepathy";
            this.cb_Telepathy.Size = new System.Drawing.Size(73, 17);
            this.cb_Telepathy.TabIndex = 5;
            this.cb_Telepathy.Text = "Telepathy";
            this.cb_Telepathy.UseVisualStyleBackColor = true;
            // 
            // cb_AbsorbEnergy
            // 
            this.cb_AbsorbEnergy.AutoSize = true;
            this.cb_AbsorbEnergy.Location = new System.Drawing.Point(7, 112);
            this.cb_AbsorbEnergy.Name = "cb_AbsorbEnergy";
            this.cb_AbsorbEnergy.Size = new System.Drawing.Size(95, 17);
            this.cb_AbsorbEnergy.TabIndex = 4;
            this.cb_AbsorbEnergy.Text = "Absorb Energy";
            this.cb_AbsorbEnergy.UseVisualStyleBackColor = true;
            // 
            // cb_Invisibility
            // 
            this.cb_Invisibility.AutoSize = true;
            this.cb_Invisibility.Location = new System.Drawing.Point(7, 89);
            this.cb_Invisibility.Name = "cb_Invisibility";
            this.cb_Invisibility.Size = new System.Drawing.Size(70, 17);
            this.cb_Invisibility.TabIndex = 3;
            this.cb_Invisibility.Text = "Invisibility";
            this.cb_Invisibility.UseVisualStyleBackColor = true;
            // 
            // cb_XrayVision
            // 
            this.cb_XrayVision.AutoSize = true;
            this.cb_XrayVision.Location = new System.Drawing.Point(7, 66);
            this.cb_XrayVision.Name = "cb_XrayVision";
            this.cb_XrayVision.Size = new System.Drawing.Size(86, 17);
            this.cb_XrayVision.TabIndex = 2;
            this.cb_XrayVision.Text = "X-Ray Vision";
            this.cb_XrayVision.UseVisualStyleBackColor = true;
            // 
            // cb_Fly
            // 
            this.cb_Fly.AutoSize = true;
            this.cb_Fly.Location = new System.Drawing.Point(7, 43);
            this.cb_Fly.Name = "cb_Fly";
            this.cb_Fly.Size = new System.Drawing.Size(39, 17);
            this.cb_Fly.TabIndex = 1;
            this.cb_Fly.Text = "Fly";
            this.cb_Fly.UseVisualStyleBackColor = true;
            // 
            // cb_SuperStrength
            // 
            this.cb_SuperStrength.AutoSize = true;
            this.cb_SuperStrength.Location = new System.Drawing.Point(7, 20);
            this.cb_SuperStrength.Name = "cb_SuperStrength";
            this.cb_SuperStrength.Size = new System.Drawing.Size(97, 17);
            this.cb_SuperStrength.TabIndex = 0;
            this.cb_SuperStrength.Text = "Super Strength";
            this.cb_SuperStrength.UseVisualStyleBackColor = true;
            // 
            // lbl_OfficeLocations
            // 
            this.lbl_OfficeLocations.AutoSize = true;
            this.lbl_OfficeLocations.Location = new System.Drawing.Point(16, 212);
            this.lbl_OfficeLocations.Name = "lbl_OfficeLocations";
            this.lbl_OfficeLocations.Size = new System.Drawing.Size(84, 13);
            this.lbl_OfficeLocations.TabIndex = 3;
            this.lbl_OfficeLocations.Text = "Office Locations";
            // 
            // lb_OfficeLocations
            // 
            this.lb_OfficeLocations.FormattingEnabled = true;
            this.lb_OfficeLocations.Items.AddRange(new object[] {
            "Moscow",
            "New York",
            "London",
            "Paris",
            "Tokyo",
            "Hong Kong",
            "Rome",
            "Sydney",
            "Dublin",
            "Los Angeles",
            "Chicago"});
            this.lb_OfficeLocations.Location = new System.Drawing.Point(16, 229);
            this.lb_OfficeLocations.Name = "lb_OfficeLocations";
            this.lb_OfficeLocations.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lb_OfficeLocations.Size = new System.Drawing.Size(140, 108);
            this.lb_OfficeLocations.TabIndex = 4;
            // 
            // gb_PreferredTransport
            // 
            this.gb_PreferredTransport.Controls.Add(this.rb_Batmobile);
            this.gb_PreferredTransport.Controls.Add(this.rb_Teleport);
            this.gb_PreferredTransport.Controls.Add(this.rb_LandSpeeder);
            this.gb_PreferredTransport.Controls.Add(this.rb_JetPack);
            this.gb_PreferredTransport.Location = new System.Drawing.Point(174, 212);
            this.gb_PreferredTransport.Name = "gb_PreferredTransport";
            this.gb_PreferredTransport.Size = new System.Drawing.Size(126, 125);
            this.gb_PreferredTransport.TabIndex = 5;
            this.gb_PreferredTransport.TabStop = false;
            this.gb_PreferredTransport.Text = " Preferred Transport ";
            // 
            // rb_Batmobile
            // 
            this.rb_Batmobile.AutoSize = true;
            this.rb_Batmobile.Location = new System.Drawing.Point(7, 89);
            this.rb_Batmobile.Name = "rb_Batmobile";
            this.rb_Batmobile.Size = new System.Drawing.Size(71, 17);
            this.rb_Batmobile.TabIndex = 3;
            this.rb_Batmobile.Text = "Batmobile";
            this.rb_Batmobile.UseVisualStyleBackColor = true;
            // 
            // rb_Teleport
            // 
            this.rb_Teleport.AutoSize = true;
            this.rb_Teleport.Location = new System.Drawing.Point(7, 66);
            this.rb_Teleport.Name = "rb_Teleport";
            this.rb_Teleport.Size = new System.Drawing.Size(64, 17);
            this.rb_Teleport.TabIndex = 2;
            this.rb_Teleport.Text = "Teleport";
            this.rb_Teleport.UseVisualStyleBackColor = true;
            // 
            // rb_LandSpeeder
            // 
            this.rb_LandSpeeder.AutoSize = true;
            this.rb_LandSpeeder.Location = new System.Drawing.Point(7, 43);
            this.rb_LandSpeeder.Name = "rb_LandSpeeder";
            this.rb_LandSpeeder.Size = new System.Drawing.Size(87, 17);
            this.rb_LandSpeeder.TabIndex = 1;
            this.rb_LandSpeeder.Text = "Landspeeder";
            this.rb_LandSpeeder.UseVisualStyleBackColor = true;
            // 
            // rb_JetPack
            // 
            this.rb_JetPack.AutoSize = true;
            this.rb_JetPack.Checked = true;
            this.rb_JetPack.Location = new System.Drawing.Point(7, 20);
            this.rb_JetPack.Name = "rb_JetPack";
            this.rb_JetPack.Size = new System.Drawing.Size(63, 17);
            this.rb_JetPack.TabIndex = 0;
            this.rb_JetPack.TabStop = true;
            this.rb_JetPack.Text = "Jetpack";
            this.rb_JetPack.UseVisualStyleBackColor = true;
            // 
            // gb_SpeedStaminaStrength
            // 
            this.gb_SpeedStaminaStrength.Controls.Add(this.lbl_StrengthValue);
            this.gb_SpeedStaminaStrength.Controls.Add(this.lbl_StaminaValue);
            this.gb_SpeedStaminaStrength.Controls.Add(this.lbl_SpeedValue);
            this.gb_SpeedStaminaStrength.Controls.Add(this.lbl_Strength);
            this.gb_SpeedStaminaStrength.Controls.Add(this.lbl_Stamina);
            this.gb_SpeedStaminaStrength.Controls.Add(this.lbl_Speed);
            this.gb_SpeedStaminaStrength.Controls.Add(this.sb_Strength);
            this.gb_SpeedStaminaStrength.Controls.Add(this.sb_Stamina);
            this.gb_SpeedStaminaStrength.Controls.Add(this.sb_Speed);
            this.gb_SpeedStaminaStrength.Location = new System.Drawing.Point(16, 353);
            this.gb_SpeedStaminaStrength.Name = "gb_SpeedStaminaStrength";
            this.gb_SpeedStaminaStrength.Size = new System.Drawing.Size(284, 164);
            this.gb_SpeedStaminaStrength.TabIndex = 6;
            this.gb_SpeedStaminaStrength.TabStop = false;
            this.gb_SpeedStaminaStrength.Text = " Speed-Stamina-Strength (100 Total)";
            // 
            // lbl_StrengthValue
            // 
            this.lbl_StrengthValue.AutoSize = true;
            this.lbl_StrengthValue.Location = new System.Drawing.Point(69, 108);
            this.lbl_StrengthValue.Name = "lbl_StrengthValue";
            this.lbl_StrengthValue.Size = new System.Drawing.Size(19, 13);
            this.lbl_StrengthValue.TabIndex = 8;
            this.lbl_StrengthValue.Text = "34";
            // 
            // lbl_staminavalue
            // 
            this.lbl_StaminaValue.AutoSize = true;
            this.lbl_StaminaValue.Location = new System.Drawing.Point(69, 65);
            this.lbl_StaminaValue.Name = "lbl_staminavalue";
            this.lbl_StaminaValue.Size = new System.Drawing.Size(19, 13);
            this.lbl_StaminaValue.TabIndex = 7;
            this.lbl_StaminaValue.Text = "33";
            // 
            // lbl_SpeedValue
            // 
            this.lbl_SpeedValue.AutoSize = true;
            this.lbl_SpeedValue.Location = new System.Drawing.Point(69, 20);
            this.lbl_SpeedValue.Name = "lbl_SpeedValue";
            this.lbl_SpeedValue.Size = new System.Drawing.Size(19, 13);
            this.lbl_SpeedValue.TabIndex = 6;
            this.lbl_SpeedValue.Text = "33";
            // 
            // lbl_Strength
            // 
            this.lbl_Strength.AutoSize = true;
            this.lbl_Strength.Location = new System.Drawing.Point(17, 108);
            this.lbl_Strength.Name = "lbl_Strength";
            this.lbl_Strength.Size = new System.Drawing.Size(50, 13);
            this.lbl_Strength.TabIndex = 5;
            this.lbl_Strength.Text = "Strength:";
            // 
            // lbl_Stamina
            // 
            this.lbl_Stamina.AutoSize = true;
            this.lbl_Stamina.Location = new System.Drawing.Point(17, 65);
            this.lbl_Stamina.Name = "lbl_Stamina";
            this.lbl_Stamina.Size = new System.Drawing.Size(48, 13);
            this.lbl_Stamina.TabIndex = 4;
            this.lbl_Stamina.Text = "Stamina:";
            // 
            // lbl_Speed
            // 
            this.lbl_Speed.AutoSize = true;
            this.lbl_Speed.Location = new System.Drawing.Point(17, 20);
            this.lbl_Speed.Name = "lbl_Speed";
            this.lbl_Speed.Size = new System.Drawing.Size(41, 13);
            this.lbl_Speed.TabIndex = 3;
            this.lbl_Speed.Text = "Speed:";
            // 
            // sb_Strength
            // 
            this.sb_Strength.Location = new System.Drawing.Point(7, 124);
            this.sb_Strength.Maximum = 109;
            this.sb_Strength.Name = "sb_Strength";
            this.sb_Strength.Size = new System.Drawing.Size(256, 17);
            this.sb_Strength.TabIndex = 2;
            this.sb_Strength.TabStop = true;
            this.sb_Strength.Value = 34;
            this.sb_Strength.Scroll += new System.Windows.Forms.ScrollEventHandler(this.sb_strength_Scroll);
            // 
            // sb_Stamina
            // 
            this.sb_Stamina.Location = new System.Drawing.Point(7, 82);
            this.sb_Stamina.Maximum = 109;
            this.sb_Stamina.Name = "sb_Stamina";
            this.sb_Stamina.Size = new System.Drawing.Size(256, 17);
            this.sb_Stamina.TabIndex = 1;
            this.sb_Stamina.TabStop = true;
            this.sb_Stamina.Value = 33;
            this.sb_Stamina.Scroll += new System.Windows.Forms.ScrollEventHandler(this.sb_stamina_Scroll);
            // 
            // sb_Speed
            // 
            this.sb_Speed.Location = new System.Drawing.Point(7, 37);
            this.sb_Speed.Maximum = 109;
            this.sb_Speed.Name = "sb_Speed";
            this.sb_Speed.Size = new System.Drawing.Size(256, 17);
            this.sb_Speed.TabIndex = 0;
            this.sb_Speed.TabStop = true;
            this.sb_Speed.Value = 33;
            this.sb_Speed.Scroll += new System.Windows.Forms.ScrollEventHandler(this.sb_speed_Scroll);
            // 
            // gb_ImportantDates
            // 
            this.gb_ImportantDates.Controls.Add(this.dtp_FatefulDay);
            this.gb_ImportantDates.Controls.Add(this.dtp_SuperPowerDiscovery);
            this.gb_ImportantDates.Controls.Add(this.dtp_Birthday);
            this.gb_ImportantDates.Controls.Add(this.lbl_FatefulDay);
            this.gb_ImportantDates.Controls.Add(this.lbl_SuperPowerDiscovery);
            this.gb_ImportantDates.Controls.Add(this.lbl_Birthday);
            this.gb_ImportantDates.Location = new System.Drawing.Point(320, 43);
            this.gb_ImportantDates.Name = "gb_ImportantDates";
            this.gb_ImportantDates.Size = new System.Drawing.Size(225, 162);
            this.gb_ImportantDates.TabIndex = 7;
            this.gb_ImportantDates.TabStop = false;
            this.gb_ImportantDates.Text = " Important Dates ";
            // 
            // dtp_FatefulDay
            // 
            this.dtp_FatefulDay.Location = new System.Drawing.Point(10, 127);
            this.dtp_FatefulDay.Name = "dtp_FatefulDay";
            this.dtp_FatefulDay.Size = new System.Drawing.Size(200, 20);
            this.dtp_FatefulDay.TabIndex = 5;
            // 
            // dtp_SuperPowerDiscovery
            // 
            this.dtp_SuperPowerDiscovery.Location = new System.Drawing.Point(10, 82);
            this.dtp_SuperPowerDiscovery.Name = "dtp_SuperPowerDiscovery";
            this.dtp_SuperPowerDiscovery.Size = new System.Drawing.Size(200, 20);
            this.dtp_SuperPowerDiscovery.TabIndex = 4;
            // 
            // dtp_Birthday
            // 
            this.dtp_Birthday.Location = new System.Drawing.Point(10, 37);
            this.dtp_Birthday.Name = "dtp_Birthday";
            this.dtp_Birthday.Size = new System.Drawing.Size(200, 20);
            this.dtp_Birthday.TabIndex = 3;
            // 
            // lbl_FatefulDay
            // 
            this.lbl_FatefulDay.AutoSize = true;
            this.lbl_FatefulDay.Location = new System.Drawing.Point(10, 112);
            this.lbl_FatefulDay.Name = "lbl_FatefulDay";
            this.lbl_FatefulDay.Size = new System.Drawing.Size(61, 13);
            this.lbl_FatefulDay.TabIndex = 2;
            this.lbl_FatefulDay.Text = "Fateful Day";
            // 
            // lbl_SuperPowerDiscovery
            // 
            this.lbl_SuperPowerDiscovery.AutoSize = true;
            this.lbl_SuperPowerDiscovery.Location = new System.Drawing.Point(10, 67);
            this.lbl_SuperPowerDiscovery.Name = "lbl_SuperPowerDiscovery";
            this.lbl_SuperPowerDiscovery.Size = new System.Drawing.Size(118, 13);
            this.lbl_SuperPowerDiscovery.TabIndex = 1;
            this.lbl_SuperPowerDiscovery.Text = "Super Power Discovery";
            // 
            // lbl_Birthday
            // 
            this.lbl_Birthday.AutoSize = true;
            this.lbl_Birthday.Location = new System.Drawing.Point(10, 20);
            this.lbl_Birthday.Name = "lbl_Birthday";
            this.lbl_Birthday.Size = new System.Drawing.Size(45, 13);
            this.lbl_Birthday.TabIndex = 0;
            this.lbl_Birthday.Text = "Birthday";
            // 
            // lbl_YearsExperience
            // 
            this.lbl_YearsExperience.AutoSize = true;
            this.lbl_YearsExperience.Location = new System.Drawing.Point(327, 214);
            this.lbl_YearsExperience.Name = "lbl_YearsExperience";
            this.lbl_YearsExperience.Size = new System.Drawing.Size(92, 13);
            this.lbl_YearsExperience.TabIndex = 8;
            this.lbl_YearsExperience.Text = "Years experience:";
            // 
            // nud_YearsExperience
            // 
            this.nud_YearsExperience.Location = new System.Drawing.Point(425, 212);
            this.nud_YearsExperience.Name = "nud_YearsExperience";
            this.nud_YearsExperience.Size = new System.Drawing.Size(120, 20);
            this.nud_YearsExperience.TabIndex = 9;
            // 
            // lbl_CapeColor
            // 
            this.lbl_CapeColor.AutoSize = true;
            this.lbl_CapeColor.Location = new System.Drawing.Point(327, 256);
            this.lbl_CapeColor.Name = "lbl_CapeColor";
            this.lbl_CapeColor.Size = new System.Drawing.Size(62, 13);
            this.lbl_CapeColor.TabIndex = 10;
            this.lbl_CapeColor.Text = "Cape Color:";
            // 
            // pb_CapeColor
            // 
            this.pb_CapeColor.BackColor = System.Drawing.Color.Fuchsia;
            this.pb_CapeColor.Location = new System.Drawing.Point(392, 255);
            this.pb_CapeColor.Name = "pb_CapeColor";
            this.pb_CapeColor.Size = new System.Drawing.Size(153, 50);
            this.pb_CapeColor.TabIndex = 11;
            this.pb_CapeColor.TabStop = false;
            this.pb_CapeColor.Click += new System.EventHandler(this.pb_capecolor_Click);
            // 
            // lbl_DarkSidePropensity
            // 
            this.lbl_DarkSidePropensity.AutoSize = true;
            this.lbl_DarkSidePropensity.Location = new System.Drawing.Point(327, 317);
            this.lbl_DarkSidePropensity.Name = "lbl_DarkSidePropensity";
            this.lbl_DarkSidePropensity.Size = new System.Drawing.Size(109, 13);
            this.lbl_DarkSidePropensity.TabIndex = 12;
            this.lbl_DarkSidePropensity.Text = "Dark Side Propensity:";
            // 
            // tb_DarkSidePropensity
            // 
            this.tb_DarkSidePropensity.Location = new System.Drawing.Point(330, 335);
            this.tb_DarkSidePropensity.Minimum = 1;
            this.tb_DarkSidePropensity.Name = "tb_DarkSidePropensity";
            this.tb_DarkSidePropensity.Size = new System.Drawing.Size(215, 45);
            this.tb_DarkSidePropensity.TabIndex = 13;
            this.tb_DarkSidePropensity.Value = 1;
            this.tb_DarkSidePropensity.Scroll += new System.EventHandler(this.tb_darksidepropensity_Scroll);
            // 
            // lbl_Portrait
            // 
            this.lbl_Portrait.AutoSize = true;
            this.lbl_Portrait.Location = new System.Drawing.Point(327, 373);
            this.lbl_Portrait.Name = "lbl_Portrait";
            this.lbl_Portrait.Size = new System.Drawing.Size(43, 13);
            this.lbl_Portrait.TabIndex = 14;
            this.lbl_Portrait.Text = "Portrait:";
            // 
            // pb_Portrait
            // 
            this.pb_Portrait.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pb_Portrait.Location = new System.Drawing.Point(376, 373);
            this.pb_Portrait.Name = "pb_Portrait";
            this.pb_Portrait.Size = new System.Drawing.Size(169, 140);
            this.pb_Portrait.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_Portrait.TabIndex = 15;
            this.pb_Portrait.TabStop = false;
            this.pb_Portrait.Click += new System.EventHandler(this.pb_portrait_Click);
            // 
            // lbl_DarkSidePropensityValue
            // 
            this.lbl_DarkSidePropensityValue.AutoSize = true;
            this.lbl_DarkSidePropensityValue.Location = new System.Drawing.Point(442, 317);
            this.lbl_DarkSidePropensityValue.Name = "lbl_DarkSidePropensityValue";
            this.lbl_DarkSidePropensityValue.Size = new System.Drawing.Size(13, 13);
            this.lbl_DarkSidePropensityValue.TabIndex = 16;
            this.lbl_DarkSidePropensityValue.Text = "1";
            // 
            // btn_CreatMe
            // 
            this.btn_CreatMe.Location = new System.Drawing.Point(376, 530);
            this.btn_CreatMe.Name = "btn_CreatMe";
            this.btn_CreatMe.Size = new System.Drawing.Size(169, 37);
            this.btn_CreatMe.TabIndex = 17;
            this.btn_CreatMe.Text = "Create Me";
            this.btn_CreatMe.UseVisualStyleBackColor = true;
            this.btn_CreatMe.Click += new System.EventHandler(this.btn_createme_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(557, 579);
            this.Controls.Add(this.btn_CreatMe);
            this.Controls.Add(this.lbl_DarkSidePropensityValue);
            this.Controls.Add(this.pb_Portrait);
            this.Controls.Add(this.lbl_Portrait);
            this.Controls.Add(this.tb_DarkSidePropensity);
            this.Controls.Add(this.lbl_DarkSidePropensity);
            this.Controls.Add(this.pb_CapeColor);
            this.Controls.Add(this.lbl_CapeColor);
            this.Controls.Add(this.nud_YearsExperience);
            this.Controls.Add(this.lbl_YearsExperience);
            this.Controls.Add(this.gb_ImportantDates);
            this.Controls.Add(this.gb_SpeedStaminaStrength);
            this.Controls.Add(this.gb_PreferredTransport);
            this.Controls.Add(this.lb_OfficeLocations);
            this.Controls.Add(this.lbl_OfficeLocations);
            this.Controls.Add(this.gb_SpecialAbilities);
            this.Controls.Add(this.tb_HerosName);
            this.Controls.Add(this.lbl_HerosName);
            this.Name = "Form1";
            this.Text = "Hero Maker";
            this.gb_SpecialAbilities.ResumeLayout(false);
            this.gb_SpecialAbilities.PerformLayout();
            this.gb_PreferredTransport.ResumeLayout(false);
            this.gb_PreferredTransport.PerformLayout();
            this.gb_SpeedStaminaStrength.ResumeLayout(false);
            this.gb_SpeedStaminaStrength.PerformLayout();
            this.gb_ImportantDates.ResumeLayout(false);
            this.gb_ImportantDates.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_YearsExperience)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_CapeColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb_DarkSidePropensity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Portrait)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_HerosName;
        private System.Windows.Forms.TextBox tb_HerosName;
        private System.Windows.Forms.GroupBox gb_SpecialAbilities;
        private System.Windows.Forms.CheckBox cb_TimeChanger;
        private System.Windows.Forms.CheckBox cb_WaterBreathing;
        private System.Windows.Forms.CheckBox cb_InsectControl;
        private System.Windows.Forms.CheckBox cb_ExplosiveFarts;
        private System.Windows.Forms.CheckBox cb_ForceField;
        private System.Windows.Forms.CheckBox cb_ExtremeLuck;
        private System.Windows.Forms.CheckBox cb_Telepathy;
        private System.Windows.Forms.CheckBox cb_AbsorbEnergy;
        private System.Windows.Forms.CheckBox cb_Invisibility;
        private System.Windows.Forms.CheckBox cb_XrayVision;
        private System.Windows.Forms.CheckBox cb_Fly;
        private System.Windows.Forms.CheckBox cb_SuperStrength;
        private System.Windows.Forms.Label lbl_OfficeLocations;
        private System.Windows.Forms.ListBox lb_OfficeLocations;
        private System.Windows.Forms.GroupBox gb_PreferredTransport;
        private System.Windows.Forms.RadioButton rb_Batmobile;
        private System.Windows.Forms.RadioButton rb_Teleport;
        private System.Windows.Forms.RadioButton rb_LandSpeeder;
        private System.Windows.Forms.RadioButton rb_JetPack;
        private System.Windows.Forms.GroupBox gb_SpeedStaminaStrength;
        private System.Windows.Forms.HScrollBar sb_Strength;
        private System.Windows.Forms.HScrollBar sb_Stamina;
        private System.Windows.Forms.HScrollBar sb_Speed;
        private System.Windows.Forms.Label lbl_Strength;
        private System.Windows.Forms.Label lbl_Stamina;
        private System.Windows.Forms.Label lbl_Speed;
        private System.Windows.Forms.Label lbl_StrengthValue;
        private System.Windows.Forms.Label lbl_StaminaValue;
        private System.Windows.Forms.Label lbl_SpeedValue;
        private System.Windows.Forms.GroupBox gb_ImportantDates;
        private System.Windows.Forms.DateTimePicker dtp_FatefulDay;
        private System.Windows.Forms.DateTimePicker dtp_SuperPowerDiscovery;
        private System.Windows.Forms.DateTimePicker dtp_Birthday;
        private System.Windows.Forms.Label lbl_FatefulDay;
        private System.Windows.Forms.Label lbl_SuperPowerDiscovery;
        private System.Windows.Forms.Label lbl_Birthday;
        private System.Windows.Forms.Label lbl_YearsExperience;
        private System.Windows.Forms.NumericUpDown nud_YearsExperience;
        private System.Windows.Forms.Label lbl_CapeColor;
        private System.Windows.Forms.PictureBox pb_CapeColor;
        private System.Windows.Forms.Label lbl_DarkSidePropensity;
        private System.Windows.Forms.TrackBar tb_DarkSidePropensity;
        private System.Windows.Forms.Label lbl_Portrait;
        private System.Windows.Forms.PictureBox pb_Portrait;
        private System.Windows.Forms.Label lbl_DarkSidePropensityValue;
        private System.Windows.Forms.Button btn_CreatMe;
    }
}

