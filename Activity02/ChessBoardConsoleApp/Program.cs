/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-08-17
 * Class     : CST-227 Enterprise Computer Promgramming II
 * Professor : James Shinevar
 * Assignment: Activity 2
 * ---------------------------------------------------------------
 * Description:
 * 1. Create Class Libary: ChessBoardModel (Cell, Board)
 * 2. Create Console Application to use the ChessBoardModel
 * 3. Create GUI Application to use the ChessBoardModel
 * Objective: Create applications that both utilize the same
 * underlying classes (library) to simulate a Chess Board and 
 * supply valid moves for main pieces
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChessBoardModel;

namespace ChessBoardConsoleApp
{
    class Program
    {
        static Board myBoard = new Board(8);
        static void Main(string[] args)
        {
            // Show the empty chess board
            //printGrid(myBoard);

            // Get the location of the chess piece
            Cell myLocation = setCurrentCell();

            // Calculate and mark the cells where legal moves are possible.
            string piece = getChessPiece();
            myBoard.MarkNextLegalMoves(myLocation, piece);

            // Show the chess board. Use . for an empty square, X for the piece location and + for a possible legal move.
            //printGrid(myBoard);
            printGridUpgrade(myBoard, myLocation, piece);
            
            // Wait for another return key to end the program.
            Console.ReadLine();
        }

        static public void printGrid(Board myBoard)
        {
            // Print the board on the console. Use "X" for current location, "+" for legal move and "." for empty square.
            for (int i = 0; i < myBoard.Size; i++)
            {
                for (int j = 0; j < myBoard.Size; j++)
                {
                    if (myBoard.theGrid[i,j].CurrentlyOccupied )
                    {
                        Console.Write("X");
                    }
                    else if (myBoard.theGrid[i,j].LegalNextMove)
                    {
                        Console.Write("+");
                    }
                    else
                    {
                        Console.Write(".");
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine("===============================");
        }

        // Upgraded Print Grid for displaying the chess board and valid moves.
        static void printGridUpgrade(Board myBoard, Cell location, string piece)
        {
            // Console.Clear();
            Console.Out.WriteLine();
            Console.Out.WriteLine("You have chosen to put a {0} at ({1} , {2}).", piece, location.RowNumber, location.ColumnNumber);
            Console.Out.WriteLine("Here are the legal moves you can make.");

            // Print Top Separation Line
            Console.Out.Write("+");
            for (int col = 0; col < myBoard.Size; col++)
                Console.Out.Write("---+");
            Console.Out.WriteLine();

            // Double Loop to print board contents
            for (int row = 0; row < myBoard.Size; row++)
            {
                Console.Out.Write("|");
                for (int col = 0; col < myBoard.Size; col++)
                {
                    if (myBoard.theGrid[row, col].CurrentlyOccupied)
                    {
                        Console.Out.Write(" X |");
                    }
                    else if (myBoard.theGrid[row, col].LegalNextMove)
                    {
                        Console.Out.Write(" + |");
                    }
                    else
                    {
                        Console.Out.Write("   |");
                    }
                }

                // Print Row Header
                Console.Out.WriteLine();

                // Print Body Separation Lines
                Console.Out.Write("+");
                for (int col = 0; col < myBoard.Size; col++)
                    Console.Out.Write("---+");
                Console.Out.WriteLine();
            }
        }

        // Return chosen location to place chess piece
        static public Cell setCurrentCell()
        {
            // Get user selected row and column
            int currentRow = getValidNumericEntry(String.Format("Enter your current row number (0-{0}): ", myBoard.Size - 1), 0, myBoard.Size - 1);
            int currentCol = getValidNumericEntry(String.Format("Enter your current column number (0-{0}): ", myBoard.Size - 1), 0, myBoard.Size - 1);

            myBoard.theGrid[currentRow, currentCol].CurrentlyOccupied = true;

            return myBoard.theGrid[currentRow, currentCol];
        }

        // Return chosen chess piece
        static public string getChessPiece()
        {
            int answer = getValidNumericEntry("Choose Chess Piece: 0.Bishop  1.King  2.Knight  3.Queen  4.Rook: ", 0, 4);
            string[] pieces = { "Bishop", "King", "Knight", "Queen", "Rook" };
            return pieces[answer];
        }

        // Validate numeric (int) entry: present appropriate warning error messages if outside of range/invalid entry
        // Replaces chooseAction method
        static public int getValidNumericEntry(string msg, int low, int high)
        {
            int answer = low - 1;

            // Ensure user chooses a valid option
            while (answer < low || answer > high)
            {
                Console.Out.Write(msg);
                try
                {
                    answer = int.Parse(Console.ReadLine());
                    if (answer < low || answer > high)
                    {
                        Console.Out.WriteLine("Warning: Valid entry must be between {0} and {1}.", low, high);
                    }
                }
                catch (FormatException)
                {
                    Console.Out.WriteLine("Error: Entry must be an integer between {0} and {1}.", low, high);
                }
            }
            return answer;
        }
    }
}
