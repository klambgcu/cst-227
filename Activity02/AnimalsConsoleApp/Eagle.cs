﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-08-17
 * Class     : CST-227 Enterprise Computer Promgramming II
 * Professor : James Shinevar
 * Assignment: Activity 2-2
 * ---------------------------------------------------------------
 * Description:
 * 1. Create Console Application to demonstrate inheritance,
 * abstract classes, and interfaces along with keywords
 * abstrat, override, virtual, and new.
 * Objective: Understand and use inheritance through abstract
 * classes and interfaces.
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimalsConsoleApp
{
    class Eagle: Animal, IFlyable, IPredator
    {
        public Eagle()
        {
            Console.Out.WriteLine("Magisterial Eagle Constructor");
        }

        public new void Talk()
        {
            Console.Out.WriteLine("Screech Screech");
        }

        public override void Sing()
        {
            Console.Out.WriteLine("Eagle sings: Flying like an Eagle to the sea...");
        }

        public void Attack()
        {
            Console.Out.WriteLine("Eagle dive bombs and scoops up a fish from the river");
        }

        public void Land()
        {
            Console.Out.WriteLine("Eagle glides in on its wide wings a lands softly on the tree branch");
        }

        public void Soaring()
        {
            Console.Out.WriteLine("Eagle soaring majestically");
        }

        public void Stalking()
        {
            Console.Out.WriteLine("Eagle circles in closer to its prey");
        }

        public void TakeOff()
        {
            Console.Out.WriteLine("Eagle Flaps its wings and rises powerfully into the air");
        }
    }
}
