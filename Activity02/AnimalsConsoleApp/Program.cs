﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-08-17
 * Class     : CST-227 Enterprise Computer Promgramming II
 * Professor : James Shinevar
 * Assignment: Activity 2-2
 * ---------------------------------------------------------------
 * Description:
 * 1. Create Console Application to demonstrate inheritance,
 * abstract classes, and interfaces along with keywords
 * abstrat, override, virtual, and new.
 * Objective: Understand and use inheritance through abstract
 * classes and interfaces.
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimalsConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Animal beast = new Animal();
            beast.Talk();
            beast.Greet();
            beast.Sing();
            Console.Out.WriteLine(); // Separate output
            */

            Dog bowser = new Dog();
            bowser.Talk();
            bowser.Greet();
            bowser.Sing();
            bowser.Fetch("stick");
            bowser.FeedMe();
            bowser.TouchMe();
            Console.Out.WriteLine(); // Separate output

            Robin red = new Robin();
            red.Talk();
            red.Greet();
            red.Sing();
            //red.Fetch("worm");
            //red.FeedMe();
            //red.TouchMe();
            Console.Out.WriteLine("===========================================\n"); // Separate output

            //
            // 1. Tiger - Extends Animal, Implements IPredator
            //
            Tiger tigger = new Tiger();
            tigger.Talk();
            tigger.Greet();
            tigger.Sing();
            tigger.Stalking();
            tigger.Attack();
            Console.Out.WriteLine(); // Separate output

            //
            // 2. Eagle - Extends Animal, Implements IPredator, IFlyable
            //
            Eagle bigBird = new Eagle();
            bigBird.Talk();
            bigBird.Sing();
            bigBird.TakeOff();
            bigBird.Soaring();
            bigBird.Stalking();
            bigBird.Attack();
            bigBird.Land();
            Console.Out.WriteLine(); // Separate output

            //
            // 3. Hummingbird - Extends Animal, Implements IFlyable
            //
            Hummingbird zippy = new Hummingbird();
            zippy.Sing();
            zippy.TakeOff();
            zippy.Soaring();
            zippy.Land();

            Console.ReadLine();
        }
    }
}
