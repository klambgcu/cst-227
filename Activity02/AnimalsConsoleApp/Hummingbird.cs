﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-08-17
 * Class     : CST-227 Enterprise Computer Promgramming II
 * Professor : James Shinevar
 * Assignment: Activity 2-2
 * ---------------------------------------------------------------
 * Description:
 * 1. Create Console Application to demonstrate inheritance,
 * abstract classes, and interfaces along with keywords
 * abstrat, override, virtual, and new.
 * Objective: Understand and use inheritance through abstract
 * classes and interfaces.
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimalsConsoleApp
{
    class Hummingbird : Animal, IFlyable
    {
        public Hummingbird()
        {
            Console.Out.WriteLine("Delightful Hummingbird Constructor");
        }

        public override void Sing()
        {
            Console.Out.WriteLine("Worble Chirp Chirp Worble");
        }
        public void Land()
        {
            Console.Out.WriteLine("Hummingbird alights gracefully on the rose branch");
        }

        public void Soaring()
        {
            Console.Out.WriteLine("Hummingbird zipping quickly from here to there");
        }

        public void TakeOff()
        {
            Console.Out.WriteLine("Hummingbird flashes into the sky");
        }
    }
}
