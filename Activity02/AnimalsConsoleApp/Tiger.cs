﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-08-17
 * Class     : CST-227 Enterprise Computer Promgramming II
 * Professor : James Shinevar
 * Assignment: Activity 2-2
 * ---------------------------------------------------------------
 * Description:
 * 1. Create Console Application to demonstrate inheritance,
 * abstract classes, and interfaces along with keywords
 * abstrat, override, virtual, and new.
 * Objective: Understand and use inheritance through abstract
 * classes and interfaces.
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimalsConsoleApp
{
    class Tiger: Animal, IPredator
    {
        public Tiger()
        {
            Console.Out.WriteLine("Powerful Tiger Constructor");
        }

        public new void Greet()
        {
            Console.WriteLine("Tiger says I am happy to meet you");
        }

        public new void Talk()
        {
            Console.Out.WriteLine("Tiger says RRROOOOOAAAAARRR!!!");
        }

        public override void Sing()
        {
            Console.Out.WriteLine("Tiger sings Wonderful thing about Tiggers...");
        }

        public void Attack()
        {
            Console.Out.WriteLine("Tiger bounces on his tail and tackles Pooh-Bear");
        }

        public void Stalking()
        {
            Console.Out.WriteLine("Tiger is tip-toeing towards its prey");
        }
    }
}
