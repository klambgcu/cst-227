﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-08-17
 * Class     : CST-227 Enterprise Computer Promgramming II
 * Professor : James Shinevar
 * Assignment: Activity 2-2
 * ---------------------------------------------------------------
 * Description:
 * 1. Create Console Application to demonstrate inheritance,
 * abstract classes, and interfaces along with keywords
 * abstrat, override, virtual, and new.
 * Objective: Understand and use inheritance through abstract
 * classes and interfaces.
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimalsConsoleApp
{
    abstract class Animal
    {
        public Animal()
        {
            Console.Out.WriteLine("Animal constructor");
        }

        public void Greet()
        {
            Console.WriteLine("Animal says Hello");
        }

        public void Talk()
        {
            Console.Out.WriteLine("Animal talking");
        }

        public virtual void Sing()
        {
            Console.Out.WriteLine("Animal Song");
        }
    }
}
