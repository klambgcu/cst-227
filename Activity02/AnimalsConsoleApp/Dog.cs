﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-08-17
 * Class     : CST-227 Enterprise Computer Promgramming II
 * Professor : James Shinevar
 * Assignment: Activity 2-2
 * ---------------------------------------------------------------
 * Description:
 * 1. Create Console Application to demonstrate inheritance,
 * abstract classes, and interfaces along with keywords
 * abstrat, override, virtual, and new.
 * Objective: Understand and use inheritance through abstract
 * classes and interfaces.
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimalsConsoleApp
{
    class Dog: Animal, IDomesticated
    {
        public Dog()
        {
            Console.Out.WriteLine("Dog constructor. Good puppy.");
        }

        public new void Talk()
        {
            Console.Out.WriteLine("Bark Bark Bark");
        }

        public override void Sing()
        {
            Console.Out.WriteLine("Hooowwwl!");
        }

        public void Fetch(String thing)
        {
            Console.Out.WriteLine("Oh boy. Here is your " + thing + ". Let's do it again!");
        }

        public void TouchMe()
        {
            Console.Out.WriteLine("Please scratch behind my ears.");
        }

        public void FeedMe()
        {
            Console.Out.WriteLine("It's suppertime. The very best time of the day!!!");
        }
    }
}
