/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-08-17
 * Class     : CST-227 Enterprise Computer Promgramming II
 * Professor : James Shinevar
 * Assignment: Activity 2
 * ---------------------------------------------------------------
 * Description:
 * 1. Create Class Libary: ChessBoardModel (Cell, Board)
 * 2. Create Console Application to use the ChessBoardModel
 * 3. Create GUI Application to use the ChessBoardModel
 * Objective: Create applications that both utilize the same
 * underlying classes (library) to simulate a Chess Board and 
 * supply valid moves for main pieces
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChessBoardGUIApp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
