/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-08-17
 * Class     : CST-227 Enterprise Computer Promgramming II
 * Professor : James Shinevar
 * Assignment: Activity 2
 * ---------------------------------------------------------------
 * Description:
 * 1. Create Class Libary: ChessBoardModel (Cell, Board)
 * 2. Create Console Application to use the ChessBoardModel
 * 3. Create GUI Application to use the ChessBoardModel
 * Objective: Create applications that both utilize the same
 * underlying classes (library) to simulate a Chess Board and 
 * supply valid moves for main pieces
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ChessBoardModel;

namespace ChessBoardGUIApp
{
    public partial class Form1 : Form
    {
        static public Board myBoard = new Board(8);
        public Button[,] btnGrid = new Button[myBoard.Size, myBoard.Size];
        public string chessPiece = "";

        public Form1()
        {
            InitializeComponent();
            populateGrid();
        }

        public void populateGrid()
        {
            // This function will fill the panel1 control with buttons.
            int buttonSize = panel1.Width / myBoard.Size; // calcuate the width of each button on the grid.
            panel1.Height = panel1.Width; // Set the grid to be square.

            // Nested loop. Create buttons and place them in the Panel
            for (int r = 0; r < myBoard.Size; r++)
            {
                for (int c = 0; c < myBoard.Size; c++)
                {
                    btnGrid[r, c] = new Button();

                    // Make each button square
                    btnGrid[r, c].Width = buttonSize;
                    btnGrid[r, c].Height = buttonSize;

                    btnGrid[r, c].Click += Grid_Button_Click; // Add the same click event to each button.
                    panel1.Controls.Add(btnGrid[r, c]); // Place the button on the Panel
                    btnGrid[r, c].Location = new Point(buttonSize * r, buttonSize * c); // Position it in x,y

                    // For testing purposes. Remove later.
                    // btnGrid[r, c].Text = r.ToString() + "|" + c.ToString();

                    // The Tag attribute will old the row and column number in a string
                    btnGrid[r, c].Tag = r.ToString() + "|" + c.ToString();
                }
            }
        }

        private void Grid_Button_Click(object sender, EventArgs e)
        {
            // Get the row and column number of the button just clicked.
            string[] strArr = (sender as Button).Tag.ToString().Split('|');
            int r = int.Parse(strArr[0]);
            int c = int.Parse(strArr[1]);

            // Run a helper function to label all legal moves for this piece.
            Cell currentCell = myBoard.theGrid[r, c];
            myBoard.MarkNextLegalMoves(currentCell, chessPiece);
            updateButtonLabels();

            // Reset the background color of all buttons to the default (original) color.
            for (int i = 0; i < myBoard.Size; i++)
            {
                for (int j = 0; j < myBoard.Size; j++)
                {
                    btnGrid[i, j].BackColor = default(Color);
                }
            }

            // Set the background color of the clicked button to something different.
            (sender as Button).BackColor = Color.Cornsilk;
        }

        public void updateButtonLabels()
        {
            for (int r = 0; r < myBoard.Size; r++)
            {
                for (int c = 0; c < myBoard.Size; c++)
                {
                    btnGrid[r, c].Text = "";
                    if (myBoard.theGrid[r, c].CurrentlyOccupied) btnGrid[r, c].Text = chessPiece;
                    if (myBoard.theGrid[r, c].LegalNextMove) btnGrid[r, c].Text = "Legal";
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Set the combo box to the first item in the list and assign value to chess piece
            comboBox1.SelectedIndex = 0;
            chessPiece = comboBox1.SelectedItem.ToString();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Capture value for chess piece when new item is selected in combo box
            chessPiece = comboBox1.SelectedItem.ToString();
        }
    }
}
