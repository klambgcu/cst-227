/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-08-17
 * Class     : CST-227 Enterprise Computer Promgramming II
 * Professor : James Shinevar
 * Assignment: Activity 2
 * ---------------------------------------------------------------
 * Description:
 * 1. Create Class Libary: ChessBoardModel (Cell, Board)
 * 2. Create Console Application to use the ChessBoardModel
 * 3. Create GUI Application to use the ChessBoardModel
 * Objective: Create applications that both utilize the same
 * underlying classes (library) to simulate a Chess Board and 
 * supply valid moves for main pieces
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessBoardModel
{
    public class Cell
    {
        // Row and Col are the cell's location on the grid.
        public int RowNumber { get; set; }
        public int ColumnNumber { get; set; }

        // T/F = Is the chess piece on this cell?
        public bool CurrentlyOccupied { get; set; }

        // Is this square a legal move for the chess piece?
        public bool LegalNextMove { get; set; }

        // Constructor
        public Cell(int r, int c)
        {
            RowNumber = r;
            ColumnNumber = c;
        }
    }
}
