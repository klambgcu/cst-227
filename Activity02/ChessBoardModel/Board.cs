/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-08-17
 * Class     : CST-227 Enterprise Computer Promgramming II
 * Professor : James Shinevar
 * Assignment: Activity 2
 * ---------------------------------------------------------------
 * Description:
 * 1. Create Class Libary: ChessBoardModel (Cell, Board)
 * 2. Create Console Application to use the ChessBoardModel
 * 3. Create GUI Application to use the ChessBoardModel
 * Objective: Create applications that both utilize the same
 * underlying classes (library) to simulate a Chess Board and 
 * supply valid moves for main pieces
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessBoardModel
{
    public class Board
    {
        // THe board is alwas square. Usually 8x8
        public int Size { get; set; }

        // 2D Array of Cell objects
        public Cell[,] theGrid;

        // Constructor
        public Board(int s)
        {
            Size = s;

            // We must initialize the array to avoid Null Exception errors.
            theGrid = new Cell[Size, Size];
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    theGrid[i, j] = new Cell(i, j);
                }
            }
        }

        public void MarkNextLegalMoves(Cell currentCell, String chessPiece)
        {
            int row = currentCell.RowNumber;
            int col = currentCell.ColumnNumber;

            // Step 1 - Clear all LegalMoves/Currently Occupied from previous turn
            for (int r = 0; r < Size; r++)
            {
                for (int c = 0; c < Size; c++)
                {
                    theGrid[r, c].LegalNextMove = false;
                    theGrid[r, c].CurrentlyOccupied = false;
                }
            }

            // Step 1.1 - Assign Currently Occupied for the current cell
            theGrid[row, col].CurrentlyOccupied = true;

            // Step 2 - Find all legal moves and mark the square
            switch (chessPiece)
            {
                // The Knight can move 1 vert/2 hor or 2 vert/1 hor for each of the 4 compass directions N,E,S,W
                case "Knight":
                    SetValidCellNextMove(currentCell, -2, -1);
                    SetValidCellNextMove(currentCell, -2,  1);
                    SetValidCellNextMove(currentCell, -1,  2);
                    SetValidCellNextMove(currentCell,  1,  2);
                    SetValidCellNextMove(currentCell,  2,  1);
                    SetValidCellNextMove(currentCell,  2, -1);
                    SetValidCellNextMove(currentCell,  1, -2);
                    SetValidCellNextMove(currentCell, -1, -2);
                    break;

                // The King can move 1 position in each of the 8 directions N,NE,E,SE,S,SW,W,NW
                case "King":
                    SetValidCellNextMove(currentCell, -1, -1);
                    SetValidCellNextMove(currentCell, -1,  0);
                    SetValidCellNextMove(currentCell, -1,  1);
                    SetValidCellNextMove(currentCell,  0, -1);
                    SetValidCellNextMove(currentCell,  0,  1);
                    SetValidCellNextMove(currentCell,  1, -1);
                    SetValidCellNextMove(currentCell,  1,  0);
                    SetValidCellNextMove(currentCell,  1,  1);
                    break;

                // The Rook can move any number for each of the 4 compass directions N,E,S,W
                case "Rook":
                    for (int i = 0; i < Size; i++)
                    {
                        // Mark Vertical
                        SetValidCellNextMove(currentCell,  -i, 0);
                        SetValidCellNextMove(currentCell,   i, 0);

                        // Mark Horizontal
                        SetValidCellNextMove(currentCell,  0, -i);
                        SetValidCellNextMove(currentCell,  0,  i);
                    }
                    theGrid[row, col].LegalNextMove = false; // Reset current position
                    break;

                // The Bishop can move any number for each of the 4 diagonal directions NE,NW,SE,SW
                case "Bishop":
                    for (int i = 0; i < Size; i++)
                    {
                        // Mark Diagonals UL..LR
                        SetValidCellNextMove(currentCell,  i,  i);
                        SetValidCellNextMove(currentCell, -i, -i);

                        // Mark Diagonals LL..UR
                        SetValidCellNextMove(currentCell,  i, -i);
                        SetValidCellNextMove(currentCell, -i,  i);
                    }
                    theGrid[row, col].LegalNextMove = false; // Reset current position
                    break;

                // The Queen can move any number in each of the 8 directions N,NE,E,SE,S,SW,W,NW
                case "Queen":
                    for (int i = 0; i < Size; i++)
                    {
                        // Mark Vertical
                        SetValidCellNextMove(currentCell, -i, 0);
                        SetValidCellNextMove(currentCell, i, 0);

                        // Mark Horizontal
                        SetValidCellNextMove(currentCell, 0, -i);
                        SetValidCellNextMove(currentCell, 0, i);

                        // Mark Diagonals UL..LR
                        SetValidCellNextMove(currentCell, i, i);
                        SetValidCellNextMove(currentCell, -i, -i);

                        // Mark Diagonals LL..UR
                        SetValidCellNextMove(currentCell, i, -i);
                        SetValidCellNextMove(currentCell, -i, i);
                    }
                    theGrid[row, col].LegalNextMove = false; // Reset current position
                    break;

                default:
                    break;
            }
        }

        // Validate that an intended move lands within the board grid based upon cell position and offsets
        // If valid, set the legal next move
        private void SetValidCellNextMove(Cell cell, int rowOffset, int colOffset)
        {
            int row = cell.RowNumber + rowOffset;
            int col = cell.ColumnNumber + colOffset;
            if (row >= 0 && row < Size && col >= 0 && col < Size)
            {
                theGrid[row, col].LegalNextMove = true;
            }
        }
    }
}
