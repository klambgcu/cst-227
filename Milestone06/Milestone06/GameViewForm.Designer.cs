/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-14
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Milestone 6
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create GUI forms for the beginning of Minesweeper game
 * 2. Main Form - Select Game options 
 * 3. Game Form - Form with button grid based upon chosen level 
 * 4. Merge Cell & Board classes
 * 5. Introduce flags, counter & timer
 * Objective:
 * Fully functional game play
 * ---------------------------------------------------------------
 */

namespace Milestone06
{
    partial class GameViewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GameViewForm));
            this.panelButtonGrid = new System.Windows.Forms.Panel();
            this.panelStatus = new System.Windows.Forms.Panel();
            this.pb_Smiley = new System.Windows.Forms.PictureBox();
            this.lblSecondCount = new System.Windows.Forms.Label();
            this.lblFlagCount = new System.Windows.Forms.Label();
            this.timerSeconds = new System.Windows.Forms.Timer(this.components);
            this.GameMenu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemOptions = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemHighScore = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuItemExit = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.panelStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Smiley)).BeginInit();
            this.GameMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelButtonGrid
            // 
            this.panelButtonGrid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelButtonGrid.Location = new System.Drawing.Point(0, 85);
            this.panelButtonGrid.Name = "panelButtonGrid";
            this.panelButtonGrid.Size = new System.Drawing.Size(334, 250);
            this.panelButtonGrid.TabIndex = 0;
            // 
            // panelStatus
            // 
            this.panelStatus.BackColor = System.Drawing.Color.Gainsboro;
            this.panelStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelStatus.Controls.Add(this.pb_Smiley);
            this.panelStatus.Controls.Add(this.lblSecondCount);
            this.panelStatus.Controls.Add(this.lblFlagCount);
            this.panelStatus.Location = new System.Drawing.Point(0, 25);
            this.panelStatus.Name = "panelStatus";
            this.panelStatus.Size = new System.Drawing.Size(334, 60);
            this.panelStatus.TabIndex = 1;
            // 
            // pb_Smiley
            // 
            this.pb_Smiley.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb_Smiley.Image = global::Milestone06.Properties.Resources.status_smilenew;
            this.pb_Smiley.Location = new System.Drawing.Point(150, 12);
            this.pb_Smiley.Name = "pb_Smiley";
            this.pb_Smiley.Size = new System.Drawing.Size(33, 33);
            this.pb_Smiley.TabIndex = 2;
            this.pb_Smiley.TabStop = false;
            this.pb_Smiley.Click += new System.EventHandler(this.pb_Smiley_Click);
            // 
            // lblSecondCount
            // 
            this.lblSecondCount.AutoSize = true;
            this.lblSecondCount.BackColor = System.Drawing.Color.Black;
            this.lblSecondCount.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSecondCount.Font = new System.Drawing.Font("Goudy Old Style", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSecondCount.ForeColor = System.Drawing.Color.Red;
            this.lblSecondCount.Location = new System.Drawing.Point(263, 12);
            this.lblSecondCount.Name = "lblSecondCount";
            this.lblSecondCount.Size = new System.Drawing.Size(58, 33);
            this.lblSecondCount.TabIndex = 1;
            this.lblSecondCount.Text = "000";
            // 
            // lblFlagCount
            // 
            this.lblFlagCount.AutoSize = true;
            this.lblFlagCount.BackColor = System.Drawing.Color.Black;
            this.lblFlagCount.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFlagCount.Font = new System.Drawing.Font("Goudy Old Style", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFlagCount.ForeColor = System.Drawing.Color.Red;
            this.lblFlagCount.Location = new System.Drawing.Point(12, 12);
            this.lblFlagCount.Name = "lblFlagCount";
            this.lblFlagCount.Size = new System.Drawing.Size(58, 33);
            this.lblFlagCount.TabIndex = 0;
            this.lblFlagCount.Text = "010";
            // 
            // timerSeconds
            // 
            this.timerSeconds.Interval = 1000;
            this.timerSeconds.Tick += new System.EventHandler(this.secondTimer_Tick);
            // 
            // GameMenu
            // 
            this.GameMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.MenuItemAbout});
            this.GameMenu.Location = new System.Drawing.Point(0, 0);
            this.GameMenu.Name = "GameMenu";
            this.GameMenu.Size = new System.Drawing.Size(334, 24);
            this.GameMenu.TabIndex = 2;
            this.GameMenu.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItemOptions,
            this.MenuItemHighScore,
            this.toolStripMenuItem1,
            this.MenuItemExit});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // MenuItemOptions
            // 
            this.MenuItemOptions.Name = "MenuItemOptions";
            this.MenuItemOptions.Size = new System.Drawing.Size(180, 22);
            this.MenuItemOptions.Text = "&Options ...";
            this.MenuItemOptions.Click += new System.EventHandler(this.MenuItemOptions_Click);
            // 
            // MenuItemHighScore
            // 
            this.MenuItemHighScore.Name = "MenuItemHighScore";
            this.MenuItemHighScore.Size = new System.Drawing.Size(180, 22);
            this.MenuItemHighScore.Text = "&Highscores ...";
            this.MenuItemHighScore.Click += new System.EventHandler(this.MenuItemHighScores_Click_1);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(177, 6);
            // 
            // MenuItemExit
            // 
            this.MenuItemExit.Name = "MenuItemExit";
            this.MenuItemExit.Size = new System.Drawing.Size(180, 22);
            this.MenuItemExit.Text = "&Exit";
            this.MenuItemExit.Click += new System.EventHandler(this.MenuItemExit_Click);
            // 
            // MenuItemAbout
            // 
            this.MenuItemAbout.Name = "MenuItemAbout";
            this.MenuItemAbout.Size = new System.Drawing.Size(52, 20);
            this.MenuItemAbout.Text = "&About";
            this.MenuItemAbout.Click += new System.EventHandler(this.MenuItemAbout_Click);
            // 
            // GameViewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 339);
            this.Controls.Add(this.panelStatus);
            this.Controls.Add(this.panelButtonGrid);
            this.Controls.Add(this.GameMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.GameMenu;
            this.MaximizeBox = false;
            this.Name = "GameViewForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Minesweeper";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.GameViewForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.GameViewForm_FormClosed);
            this.panelStatus.ResumeLayout(false);
            this.panelStatus.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Smiley)).EndInit();
            this.GameMenu.ResumeLayout(false);
            this.GameMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelButtonGrid;
        private System.Windows.Forms.Panel panelStatus;
        private System.Windows.Forms.Label lblSecondCount;
        private System.Windows.Forms.Label lblFlagCount;
        private System.Windows.Forms.Timer timerSeconds;
        private System.Windows.Forms.PictureBox pb_Smiley;
        private System.Windows.Forms.MenuStrip GameMenu;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MenuItemOptions;
        private System.Windows.Forms.ToolStripMenuItem MenuItemHighScore;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem MenuItemExit;
        private System.Windows.Forms.ToolStripMenuItem MenuItemAbout;
    }
}