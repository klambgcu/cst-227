/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-14
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Milestone 6
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create GUI forms for the beginning of Minesweeper game
 * 2. Main Form - Select Game options 
 * 3. Game Form - Form with button grid based upon chosen level 
 * 4. Merge Cell & Board classes
 * 5. Introduce flags, counter & timer
 * Objective:
 * Fully functional game play
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milestone06
{
    public partial class OptionsViewForm : Form
    {
        public int level = 0;

        // Constructor
        public OptionsViewForm(int level)
        {
            InitializeComponent();
            // Display the form in the center of the screen.
            StartPosition = FormStartPosition.CenterScreen;
            this.level = level;

            switch (level)
            {
                case 0: rb_EasyLevel.Checked = true; break;
                case 1: rb_ModerateLevel.Checked = true; break;
                case 2: rb_DifficultLevel.Checked = true; break;
                default: rb_EasyLevel.Checked = true; break;
            }
        }

        //
        // Obtain player game preference & call game form (Form2) with choice (level)
        //
        private void btn_PlayGame_Click(object sender, EventArgs e)
        {
            level = 0;

            if (rb_EasyLevel.Checked) level = 0;
            if (rb_ModerateLevel.Checked) level = 1;
            if (rb_DifficultLevel.Checked) level = 2;

            //GameViewForm gameForm = new GameViewForm(level);
            //gameForm.Show();
            this.Hide();
        }
    }
}
