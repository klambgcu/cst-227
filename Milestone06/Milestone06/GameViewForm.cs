/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-14
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Milestone 6
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create GUI forms for the beginning of Minesweeper game
 * 2. Main Form - Select Game options 
 * 3. Game Form - Form with button grid based upon chosen level 
 * 4. Merge Cell & Board classes
 * 5. Introduce flags, counter & timer
 * Objective:
 * Fully functional game play
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using Milestone06.Models.Implementation;
using Milestone06.Models.Interfaces;
using Milestone06.Services.Implementations;

namespace Milestone06
{
    public partial class GameViewForm : Form
    {
        // Define states for game loop logic
        private enum GameState
        {
            GamePlaying,
            GameOverWin,
            GameOverLose
        }

        //
        // Initialize Form variables
        //
        private GameState GameStatus = GameState.GamePlaying;

        private int[,] gameLevels = { { 10, 10 },
                                      { 15, 18 },
                                      { 20, 25 } }; // Board Size, Bomb Percentage
        private Color[] gameColors = { Color.White,  Color.Blue,   Color.Green,
                                       Color.Red,    Color.Orange, Color.Yellow,
                                       Color.Purple, Color.Cyan,   Color.Brown, Color.White};
        private int bombPercentage = 10;
        private int buttonSize = 30;
        private int size = 10;
        private int gameLevel = 0;
        private Button[,] btnGrid;
        private Board board;
        private Point LastButton = new Point(0, 0);
        private static Stopwatch watch = new Stopwatch();
        PlayerStatsManager manager = new PlayerStatsManager(new FileAccessService(@".\", "MineSweeperScores.txt"));


        // Constructor
        // level determines size/difficulty sent from Form1
        public GameViewForm(int level)
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
            bool readResult = manager.Read();
            if (readResult == false) displayFileFormatErrors();
            gameLevel = level;
            InitializeGame(level, true);
        }

        // Inform player that highscore file contained formatting errors
        // Basically, read process skips those records but we inform player anyway.
        private void displayFileFormatErrors()
        {
            String msgs = "Highscore file contains formatting errors:" + Environment.NewLine;
            IList<string> errors = manager.GetErrorMessages();
            foreach (string error in errors)
            {
                msgs += error + Environment.NewLine;
            }
            MessageBox.Show(msgs, "Highscore File Format Errors", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        //
        // Define game board and setup mines based upon size/difficulty
        //
        private void InitializeGame(int level, bool newGame)
        {
            this.Hide();
            GameStatus = GameState.GamePlaying;
            size = gameLevels[level, 0];
            bombPercentage = gameLevels[level, 1];

            // Dynamically set interior of form to hold game board
            // Make playfield square, add height for status panel 
            // and menu bar
            int height = panelStatus.Height + panelStatus.Location.Y;
            ClientSize = new Size(size * buttonSize + 1, size * buttonSize + height + 1);            

            board = new Board(size);
            board.Difficulty = bombPercentage;
            board.setupLiveNeighbors();
            board.calculateLiveNeighbors();
            if (newGame) InitializeBoardGrid();
            InitializeStatusPanel();
            CenterForm();
            this.Show();
        }

        //
        // Define game status panel: flag and second counter
        //
        private void InitializeStatusPanel()
        {
            // Adjust panel width and second label placement
            panelStatus.Width = panelButtonGrid.Width;
            lblSecondCount.Left = panelStatus.Width - (lblSecondCount.Width + lblFlagCount.Left);
            pb_Smiley.Left = (panelStatus.Width / 2) - 16;
            watch.Reset();
            timerSeconds.Enabled = false;
            pb_Smiley.Image = Properties.Resources.status_smilenew;
            lblSecondCount.Text = "000";
            statusPanelFlagsUpdate();
        }

        //
        // Position form in center of scren (useful when resized)
        //
        private void CenterForm()
        {
            // Reposition in center of screen
            Screen myScreen = Screen.FromControl(this);
            Rectangle area = myScreen.WorkingArea;

            this.Top = (area.Height - this.Height) / 2;
            this.Left = (area.Width - this.Width) / 2;
        }

        //
        // Define the game board button grid
        //
        public void InitializeBoardGrid()
        {
            panelButtonGrid.Visible = false; // hide to avoid seeing redraw
            panelButtonGrid.Controls.Clear();
            btnGrid = new Button[size, size];

            // Resize panel to a square - could set panel1.autosize
            panelButtonGrid.Height = (size * buttonSize);
            panelButtonGrid.Width = (size * buttonSize);

            // Nested loop. Create buttons and place them in the Panel
            for (int r = 0; r < size; r++)
            {
                for (int c = 0; c < size; c++)
                {
                    Button tmpButton = new Button();
                    //btnGrid[r, c] = new Button();

                    // Make each button square
                    tmpButton.Width = buttonSize;
                    tmpButton.Height = buttonSize;

                    // Define default button behaviors
                    tmpButton.Text = "";
                    tmpButton.BackColor = Color.Silver;
                    tmpButton.MouseDown += Grid_Button_MouseDown;
                    tmpButton.MouseUp += Grid_Button_MouseUp;
                    tmpButton.Location = new Point(buttonSize * r, buttonSize * c); // Position it in x,y
                    tmpButton.Font = new Font(tmpButton.Font.FontFamily, 14, FontStyle.Bold);
                    tmpButton.Padding = new Padding(0);
                    tmpButton.TextAlign = ContentAlignment.TopCenter;
                    tmpButton.BackgroundImageLayout = ImageLayout.Zoom;

                    // The Tag attribute will hold the row and column number in a string
                    tmpButton.Tag = r.ToString() + "|" + c.ToString();

                    // Add to the game board button panel
                    panelButtonGrid.Controls.Add(tmpButton);
                    btnGrid[r, c] = tmpButton;
                }
            }
            panelButtonGrid.Visible = true; // show to avoid seeing redraw
        }

        //
        // Update game board button grid 
        //
        private void gridUpdateDisplay()
        {
            for (int row = 0; row < size; row++)
            {
                for (int col = 0; col < size; col++)
                {
                    if (board.grid[row, col].Flagged)
                    {
                        btnGrid[row, col].BackgroundImage = Properties.Resources.cell_flag;
                    }
                    else if (board.grid[row, col].Visited)
                    {
                        int value = board.grid[row, col].Neighbors;
                        if (value > 0)
                        {
                            btnGrid[row, col].Text = value.ToString();
                            btnGrid[row, col].ForeColor = gameColors[value];
                        }
                        btnGrid[row, col].BackColor = Color.Gainsboro;
                    }
                }
            }
        }

        //
        // Show bombs and hit bomb at Game Over Lose
        //
        private void gridExposeBombs()
        {
            for (int row = 0; row < size; row++)
            {
                for (int col = 0; col < size; col++)
                {
                    if (LastButton.X == col && LastButton.Y == row && board.grid[row, col].Live)
                    {
                        btnGrid[row, col].BackgroundImage = Properties.Resources.cell_bombhit;
                        btnGrid[row, col].Text = "";
                    }
                    else if (board.grid[row, col].Flagged && !board.grid[row, col].Live)
                    {
                        btnGrid[row, col].BackgroundImage = Properties.Resources.cell_bombmiss;
                    }
                    else if (board.grid[row, col].Live)
                    {
                        btnGrid[row, col].BackgroundImage = Properties.Resources.cell_bomb;
                    }
                }
            }
        }

        //
        // Helper: Put smile on smiley's face
        //
        private void Grid_Button_MouseUp(object sender, MouseEventArgs me)
        {
            // Continue only if game is not over (i.e. Playing)
            if (GameStatus != GameState.GamePlaying) return;

            pb_Smiley.Image = Properties.Resources.status_smilenew;

        }
        //
        // Main button click/down reaction method
        // 
        private void Grid_Button_MouseDown(object sender, MouseEventArgs me)
        {
            // Continue only if game is not over (i.e. Playing)
            if (GameStatus != GameState.GamePlaying) return;

            // Enable stop watch / timer on first click
            if (timerSeconds.Enabled == false)
            {
                watch.Start();
                timerSeconds.Enabled = true;
            }

            // Get the row and column number of the button just clicked.
            string[] strArr = (sender as Button).Tag.ToString().Split('|');
            int row = int.Parse(strArr[0]);
            int col = int.Parse(strArr[1]);

            // Save the Last Button Clicked
            LastButton = new Point(col, row);

            // Check Right Click to toggle flag placement
            if (me.Button == MouseButtons.Right)
            {
                board.toggleFlag(row, col);
                statusPanelFlagsUpdate();
                btnGrid[row, col].BackgroundImage = null;
                gridUpdateDisplay();
            }
            else
            {
                // Ignore click if grid is flagged.
                if (board.grid[row, col].Flagged) return;

                // Set smiley face to ohoh.
                pb_Smiley.Image = Properties.Resources.status_smileohoh;

                // Set cell chosen/visited
                if (board.grid[row, col].Neighbors == 0)
                {
                    board.floodFill(row, col);
                    gridUpdateDisplay();
                }
                else
                {
                    board.setCellVisited(row, col);
                    gridUpdateDisplay();
                }

                // Check game completed - Bomb Chosen
                if (board.grid[row, col].Live)
                {
                    GameStatus = GameState.GameOverLose;
                    GameOver();
                }
            }

            // Check game completed - Board cleared
            if (GameStatus == GameState.GamePlaying && board.boardCompleted())
            {
                GameStatus = GameState.GameOverWin;
                GameOver();
            }
        }

        //
        // Perform game over procedures
        //
        private void GameOver()
        {
            secondTimerStop();
            string msg;
            int position = 0;
            bool madeHighScore = false;
            PlayerStats newEntry = new PlayerStats();

            if (GameStatus == GameState.GameOverWin)
            {
                msg = "    Congratulations! " + Environment.NewLine + "           You Win! ";
                pb_Smiley.Image = Properties.Resources.status_smilehappy;

                // Inform player they won
                msg = msg + Environment.NewLine + "Seconds Elapsed: " + lblSecondCount.Text;
                MessageBox.Show(msg, "Game Over", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                // Check if score is good enough for hall of fame                
                newEntry = new PlayerStats("", gameLevel, int.Parse(lblSecondCount.Text));
                position = manager.GetInsertPosition((Levels)gameLevel, 5, newEntry);
                if (position > -1)
                {
                    madeHighScore = true;
                }
                else
                {
                    position = 0;
                    madeHighScore = false;
                }
            }
            else
            {
                gridExposeBombs();
                msg = "          Oh no!     " + Environment.NewLine + "     Sorry you lose.";
                pb_Smiley.Image = Properties.Resources.status_smilesad;

                // Inform player they lost
                msg = msg + Environment.NewLine + "Seconds Elapsed: " + lblSecondCount.Text;
                MessageBox.Show(msg, "Game Over", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            // Display Highscore view after every game
            // If player wins the round, determine if score is in top five and allow name entry
            // Otherwise, just display 
            HighscoreViewForm highscoreForm = new HighscoreViewForm(manager, madeHighScore, newEntry, position, gameLevel);
            highscoreForm.ShowDialog();



        }

        //
        // Helper: Update status panel flag counter
        //
        private void statusPanelFlagsUpdate()
        {
            int amount = board.BombCount - board.FlagCount;
            lblFlagCount.Text = String.Format("{0:D3}", amount);
        }

        //
        // Helper: Stop second elapsed counter
        //
        private void secondTimerStop()
        {
            watch.Stop();
            timerSeconds.Enabled = false;
        }

        //
        // Timed event to update seconds elasped in game play
        //
        private void secondTimer_Tick(object sender, EventArgs e)
        {
            // Apply a TimeSpan to format the output 
            TimeSpan ts = watch.Elapsed;

            int elapsed = (ts.Minutes * 60) + ts.Seconds;

            // Use String.Format option for easy reading
            string elapsedTime = String.Format("{0:D3}", elapsed);

            lblSecondCount.Text = elapsedTime;

            // Time Limit Exceeded - End Game
            if (elapsed == 999)
            {
                GameStatus = GameState.GameOverLose;
                GameOver();
            }
        }

        //
        // Click smiley to reset the game
        //
        private void pb_Smiley_Click(object sender, EventArgs e)
        {
            // Nested loop. Create buttons and place them in the Panel
            resetBoard();
            InitializeGame(gameLevel, false);
        }

        //
        // Reset button grid for new game/same size
        //
        private void resetBoard()
        {
            // Nested loop. Create buttons and place them in the Panel
            for (int r = 0; r < size; r++)
            {
                for (int c = 0; c < size; c++)
                {
                    btnGrid[r, c].BackgroundImage = null;
                    btnGrid[r, c].Text = null;
                    btnGrid[r, c].BackColor = Color.Silver;
                }
            }
        }

        //
        // Menu About
        //
        private void MenuItemAbout_Click(object sender, EventArgs e)
        {
            string title = "Minesweeper - About";
            string msg1 = "Minesweeper - Milestone Project";
            string msg2 = "Class:        GCU CST-227";
            string msg3 = "Name:       Kelly Lamb";
            string msg4 = "Professor: James Shinevar";
            string msgs = msg1 + Environment.NewLine +
                          msg2 + Environment.NewLine +
                          msg3 + Environment.NewLine +
                          msg4;

            MessageBox.Show(msgs, title, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        //
        // Menu Exit - Get confirmation
        //
        private void MenuItemExit_Click(object sender, EventArgs e)
        {
            // User click exit application - confirm closing
            Application.Exit();
        }

        //
        // Menu Options - Set level/difficulty,etc.
        //
        private void MenuItemOptions_Click(object sender, EventArgs e)
        {
            OptionsViewForm optionsForm = new OptionsViewForm(gameLevel);
            optionsForm.ShowDialog();
            gameLevel = optionsForm.level;

            InitializeGame(gameLevel, true);
        }

        private void MenuItemHighScores_Click_1(object sender, EventArgs e)
        {
            HighscoreViewForm highscoreForm = new HighscoreViewForm(manager, false, (PlayerStats)null, 0, (int)Levels.All);
            highscoreForm.ShowDialog();
        }

        //
        // Form Closed -  Get confirmation
        //
        private void GameViewForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            // User click exit application - confirm closing
            Application.Exit();
        }

        //
        // Prior to closing the form, we request confirmation
        // If yes, save highscores
        //
        private void GameViewForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult result = MessageBox.Show("Please Confirm ?",
                                                  "Exit Application",
                                                  MessageBoxButtons.YesNo,
                                                  MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                // User confirms close application - write highscore data
                manager.Write();
            }
            else { e.Cancel = true; }
        }
    }
}
