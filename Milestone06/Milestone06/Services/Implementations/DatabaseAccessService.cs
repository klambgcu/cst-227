﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-14
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Milestone 6
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create GUI forms for the beginning of Minesweeper game
 * 2. Main Form - Select Game options 
 * 3. Game Form - Form with button grid based upon chosen level 
 * 4. Merge Cell & Board classes
 * 5. Introduce flags, counter & timer
 * Objective:
 * Fully functional game play
 * ---------------------------------------------------------------
 */
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Milestone06.Models;
using Milestone06.Models.Interfaces;
using Milestone06.Services.Interfaces;

namespace Milestone06.Services.Implementations
{

    // Service to retrieve data from database
    public class DatabaseAccessService : IDataAccessService
    {
        private string server;
        private string catalog;
        private string user;
        private string password;
        
        public DatabaseAccessService(string server,
                                     string catalog,
                                     string user,
                                     string password)
        {
            this.server = server;
            this.catalog = catalog;
            this.user = user;
            this.password = password;
        }

        public bool Read(IList<IPlayerStats> list, IList<string> errMsgs)
        {
            throw new NotImplementedException();
        }

        public bool Write(IList<IPlayerStats> list, IList<string> errMsgs)
        {
            throw new NotImplementedException();
        }
    }
}
