﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-14
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Milestone 6
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create GUI forms for the beginning of Minesweeper game
 * 2. Main Form - Select Game options 
 * 3. Game Form - Form with button grid based upon chosen level 
 * 4. Merge Cell & Board classes
 * 5. Introduce flags, counter & timer
 * Objective:
 * Fully functional game play
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Milestone06.Models.Interfaces;

namespace Milestone06.Models.Implementation
{
    public class Level : ILevel
    {
		public int Id { get; set; }

		public string Description()
		{
			return Description(Id);
		}

		public string Description(int id)
		{
			string[] levelNames = { "Easy", "Moderate", "Difficult", "All Levels" };
			return levelNames[id];
		}

		public int ConvertLevelToInt(Levels level)
		{
			return (int)level;
		}

		public Levels ConvertIntToLevel(int level)
		{
			return (Levels)level;
		}
	}
}
