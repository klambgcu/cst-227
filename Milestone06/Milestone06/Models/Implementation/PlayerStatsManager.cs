﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-14
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Milestone 6
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create GUI forms for the beginning of Minesweeper game
 * 2. Main Form - Select Game options 
 * 3. Game Form - Form with button grid based upon chosen level 
 * 4. Merge Cell & Board classes
 * 5. Introduce flags, counter & timer
 * Objective:
 * Fully functional game play
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Milestone06.Services.Interfaces;
using Milestone06.Models.Interfaces;

namespace Milestone06.Models.Implementation
{
    public class PlayerStatsManager : IPlayerStatsManager
    {
        private IList<IPlayerStats> list = new List<IPlayerStats>();
        private IList<String> errMessages = new List<String>();
        IDataAccessService data;


        public PlayerStatsManager(IDataAccessService data)
        {
            this.data = data;
        }

        //
        // Read scores from file, if no file, then create one with default data
        // Returns true = success, false (default list) with error messages
        //
        public bool Read()
        {
            bool results = true;
            results = data.Read(list, errMessages);

            // Create blank entries if first time running (ie. no high score file)
            if (results == false && errMessages.Count == 1 && errMessages[0].Contains("File not found"))
            {
                // Create a default list and save it
                results = createDefaultFile();
            }
            else
            {
                // Examine list for any level containing less than five entries - add item if needed
                for (int i = (int)Levels.Easy; i <= (int)Levels.Difficult; i++)
                {
                    var amount = list.Where(p => p.Level == i).Count();
                    for (int j = amount; j < 5; j++)
                        list.Add(new PlayerStats($"No Name{j}", i, 999));
                }

            }

            // Sort list after read/create
            Sort();

            return results;
        }

        //
        // Write list to file - count, then data
        //
        public bool Write()
        {
            bool results = true;
            // Sort list before writing
            //((List<IPlayerStats>)list).Sort();
            Sort();

            results = data.Write(list, errMessages);

            return results;
        }

        //
        // Return complete list of scores
        //
        public IList<IPlayerStats> GetList()
        {
            return list;
        }

        //
        // Returns top amount of highscores in list sorted upon adjusted score
        //
        public List<IPlayerStats> GetHighScores(int amount, Levels queryLevel)
        {
            IEnumerable<IPlayerStats> highscores;
            if (queryLevel == Levels.All)
            {
                highscores = list.OrderBy(stat => stat.AdjustedScore)
                                 .Take(amount)
                                 .ToList();
            }
            else
            {
                highscores = list.Where(stat => stat.Level == (int)queryLevel)
                                 .OrderBy(stat => stat.AdjustedScore)
                                 .Take(amount)
                                 .ToList();
            }
            return (List<IPlayerStats>)highscores;
        }

        //
        // Adds a new PlayerStat record to the end of the list
        //
        public void Add(string name, int level, int score)
        {
            list.Add(new PlayerStats(name, level, score));
        }

        //
        // Adds a new PlayerStat record to the end of the list
        //
        public void Add(IPlayerStats stat)
        {
            list.Add(stat);
        }

        //
        // Creates a new PlayerStat record at position in list (0 based)
        //
        public void Insert(int position, string name, int level, int score)
        {
            list.Insert(position, new PlayerStats(name, level, score));
        }

        //
        // Creates a new PlayerStat record at position in list (0 based)
        //
        public void Insert(int position, IPlayerStats stat)
        {
            list.Insert(position, stat);
        }

        //
        // Return a list of error messages
        //
        public IList<String> GetErrorMessages()
        {
            return errMessages;
        }

        public void Sort()
        {
            //((List<PlayerStats>)list).Sort();
            list = list.OrderBy(stat => stat.AdjustedScore)
                       .ThenBy(stat => stat.Level)
                       .ThenBy(stat => stat.Score)
                       .ThenBy(stat => stat.PlayerName)
                       .ToList();

            //result = x.AdjustedScore.CompareTo(y.AdjustedScore);
            //result = (result == 0) ? x.Level.CompareTo(y.Level) : result;
            //result = (result == 0) ? x.Score.CompareTo(y.Score) : result;
            //result = (result == 0) ? x.PlayerName.CompareTo(y.PlayerName) : result;
        }

        // Return a position to insert based upon level and score
        public int GetInsertPosition(Levels queryLevel, int highScoreAmount, IPlayerStats player)
        {
            int results = -1;
            List<IPlayerStats> templist = GetHighScores(highScoreAmount, queryLevel);

            for (int i = 0; i < templist.Count(); i++)
            {
                if (player.AdjustedScore < templist[i].AdjustedScore)
                {
                    results = i;
                    break;
                }
            }
            return results;
        }

        public string getLevelDescription(int level)
        {
            string[] levelNames = { "Easy", "Moderate", "Difficult", "All Levels" };
            return levelNames[level];
        }

        //
        // Create a default file with five records
        //
        private bool createDefaultFile()
        {
            // Add 5 player stats for each level (0-2)
            for (int i = 0; i < 5; i++)
            {
                IPlayerStats p0 = new PlayerStats($"No Name{i}", 0, 999);
                IPlayerStats p1 = new PlayerStats($"No Name{i}", 1, 999);
                IPlayerStats p2 = new PlayerStats($"No Name{i}", 2, 999);
                list.Add(p0);
                list.Add(p1);
                list.Add(p2);
            }

            return Write();
        }
    }
}
