﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-14
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Milestone 6
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create GUI forms for the beginning of Minesweeper game
 * 2. Main Form - Select Game options 
 * 3. Game Form - Form with button grid based upon chosen level 
 * 4. Merge Cell & Board classes
 * 5. Introduce flags, counter & timer
 * Objective:
 * Fully functional game play
 * ---------------------------------------------------------------
 */
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Milestone06.Models.Interfaces;

namespace Milestone06.Models.Implementation
{
    public class PlayerStats : IPlayerStats, IComparable<IPlayerStats>, IComparer<IPlayerStats>
    {
        public PlayerStats() : this("No Name", 0, 999) { }

        public PlayerStats(string name, int level, int score)
        {
            this.PlayerName = name;
            this.Level = level;
            this.Score = score;
        }
        public string PlayerName { get; set; }

        public int Score { get; set; }

        public string LevelDescription
        {
            get
            {
                string description = (Level == 0) ? "Easy" : (Level == 1) ? "Moderate" : "Difficult";
                return description;
            }
        }

        public int Level { get; set; }

        public double AdjustedScore
        {
            get
            {
                // adjusted = score divided by bombs in level + 1.0 (convert double)
                int[] bombsPerLevel = { 10, 40, 100 };
                int divisor = bombsPerLevel[Level];
                return Score / (divisor + 1.0);
            }
        }

        public int Compare(IPlayerStats x, IPlayerStats y)
        {
            int result;

            result = x.AdjustedScore.CompareTo(y.AdjustedScore);
            result = (result == 0) ? x.Level.CompareTo(y.Level) : result;
            result = (result == 0) ? x.Score.CompareTo(y.Score) : result;
            result = (result == 0) ? x.PlayerName.CompareTo(y.PlayerName) : result;

            return result;
        }

        // Sorting routing
        // Compare adjusted, if same check level, 
        // if same check score, lastly if same check name
        public int CompareTo(IPlayerStats other)
        {
            int result;

            result = this.AdjustedScore.CompareTo(other.AdjustedScore);
            result = (result == 0) ? this.Level.CompareTo(other.Level) : result;
            result = (result == 0) ? this.Score.CompareTo(other.Score) : result;
            result = (result == 0) ? this.PlayerName.CompareTo(other.PlayerName) : result;

            return result;
        }

        // Display formatted contents - include adjusted score for testing
        public override string ToString()
        {
            return String.Format("{0,-20} {1,-10} {2:d3} {3,5:F2}", PlayerName, LevelDescription, Score, AdjustedScore);
        }

        // Convert to tab delimited string - ease writing/reading score file
        public string ToTabDelimitedString()
        {
            return PlayerName + "\t" + Level.ToString() + "\t" + Score.ToString();
        }
    }
}
