﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-14
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Milestone 6
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create GUI forms for the beginning of Minesweeper game
 * 2. Main Form - Select Game options 
 * 3. Game Form - Form with button grid based upon chosen level 
 * 4. Merge Cell & Board classes
 * 5. Introduce flags, counter & timer
 * Objective:
 * Fully functional game play
 * ---------------------------------------------------------------
 */

namespace Milestone06.Models.Interfaces
{
    public interface ICell
    {
        int Col { get; set; }
        bool Flagged { get; set; }
        bool Live { get; set; }
        int Neighbors { get; set; }
        int Row { get; set; }
        bool Visited { get; set; }

        void incrementNeighbors();
        void reset();
    }
}