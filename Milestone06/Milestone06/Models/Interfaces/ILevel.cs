﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-14
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Milestone 6
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create GUI forms for the beginning of Minesweeper game
 * 2. Main Form - Select Game options 
 * 3. Game Form - Form with button grid based upon chosen level 
 * 4. Merge Cell & Board classes
 * 5. Introduce flags, counter & timer
 * Objective:
 * Fully functional game play
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Milestone06.Models.Interfaces
{
	public enum Levels : int
	{
		Easy = 0,
		Moderate = 1,
		Difficult = 2,
		All = 3
	};

	public interface ILevel
    {
		int Id { get; set; }

		string Description();

		string Description(int id);

		int ConvertLevelToInt(Levels level);

		Levels ConvertIntToLevel(int level);
	}
}
