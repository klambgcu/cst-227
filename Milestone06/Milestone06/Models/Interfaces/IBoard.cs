﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-14
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Milestone 6
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create GUI forms for the beginning of Minesweeper game
 * 2. Main Form - Select Game options 
 * 3. Game Form - Form with button grid based upon chosen level 
 * 4. Merge Cell & Board classes
 * 5. Introduce flags, counter & timer
 * Objective:
 * Fully functional game play
 * ---------------------------------------------------------------
 */

namespace Milestone06.Models.Interfaces
{
    public interface IBoard
    {
        int BombCount { get; set; }
        int Difficulty { get; set; }
        int FlagCount { get; set; }
        int Size { get; set; }

        bool boardCompleted();
        void calculateLiveNeighbors();
        void floodFill(int row, int col);
        void reset();
        void setCellVisited(int row, int col);
        void setupLiveNeighbors();
        void toggleFlag(int row, int col);
    }
}