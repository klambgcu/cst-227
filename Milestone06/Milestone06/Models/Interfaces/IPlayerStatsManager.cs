﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-14
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Milestone 6
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create GUI forms for the beginning of Minesweeper game
 * 2. Main Form - Select Game options 
 * 3. Game Form - Form with button grid based upon chosen level 
 * 4. Merge Cell & Board classes
 * 5. Introduce flags, counter & timer
 * Objective:
 * Fully functional game play
 * ---------------------------------------------------------------
 */

using System.Collections.Generic;

namespace Milestone06.Models.Interfaces
{
    public interface IPlayerStatsManager
    {
        void Add(IPlayerStats stat);
        void Add(string name, int level, int score);
        IList<string> GetErrorMessages();
        List<IPlayerStats> GetHighScores(int amount, Levels queryLevel);
        int GetInsertPosition(Levels queryLevel, int highScoreAmount, IPlayerStats player);
        string getLevelDescription(int level);
        IList<IPlayerStats> GetList();
        void Insert(int position, IPlayerStats stat);
        void Insert(int position, string name, int level, int score);
        bool Read();
        void Sort();
        bool Write();
    }
}