/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-14
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Milestone 6
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create GUI forms for the beginning of Minesweeper game
 * 2. Main Form - Select Game options 
 * 3. Game Form - Form with button grid based upon chosen level 
 * 4. Merge Cell & Board classes
 * 5. Introduce flags, counter & timer
 * Objective:
 * Fully functional game play
 * ---------------------------------------------------------------
 */

namespace Milestone06
{
    partial class OptionsViewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OptionsViewForm));
            this.gb_SelectLevel = new System.Windows.Forms.GroupBox();
            this.rb_DifficultLevel = new System.Windows.Forms.RadioButton();
            this.rb_ModerateLevel = new System.Windows.Forms.RadioButton();
            this.rb_EasyLevel = new System.Windows.Forms.RadioButton();
            this.btn_PlayGame = new System.Windows.Forms.Button();
            this.gb_SelectLevel.SuspendLayout();
            this.SuspendLayout();
            // 
            // gb_SelectLevel
            // 
            this.gb_SelectLevel.Controls.Add(this.rb_DifficultLevel);
            this.gb_SelectLevel.Controls.Add(this.rb_ModerateLevel);
            this.gb_SelectLevel.Controls.Add(this.rb_EasyLevel);
            this.gb_SelectLevel.Location = new System.Drawing.Point(27, 24);
            this.gb_SelectLevel.Name = "gb_SelectLevel";
            this.gb_SelectLevel.Size = new System.Drawing.Size(207, 154);
            this.gb_SelectLevel.TabIndex = 0;
            this.gb_SelectLevel.TabStop = false;
            this.gb_SelectLevel.Text = " Select Level: ";
            // 
            // rb_DifficultLevel
            // 
            this.rb_DifficultLevel.AutoSize = true;
            this.rb_DifficultLevel.Location = new System.Drawing.Point(17, 109);
            this.rb_DifficultLevel.Name = "rb_DifficultLevel";
            this.rb_DifficultLevel.Size = new System.Drawing.Size(60, 17);
            this.rb_DifficultLevel.TabIndex = 2;
            this.rb_DifficultLevel.TabStop = true;
            this.rb_DifficultLevel.Text = "Difficult";
            this.rb_DifficultLevel.UseVisualStyleBackColor = true;
            // 
            // rb_ModerateLevel
            // 
            this.rb_ModerateLevel.AutoSize = true;
            this.rb_ModerateLevel.Location = new System.Drawing.Point(17, 72);
            this.rb_ModerateLevel.Name = "rb_ModerateLevel";
            this.rb_ModerateLevel.Size = new System.Drawing.Size(70, 17);
            this.rb_ModerateLevel.TabIndex = 1;
            this.rb_ModerateLevel.TabStop = true;
            this.rb_ModerateLevel.Text = "Moderate";
            this.rb_ModerateLevel.UseVisualStyleBackColor = true;
            // 
            // rb_EasyLevel
            // 
            this.rb_EasyLevel.AutoSize = true;
            this.rb_EasyLevel.Checked = true;
            this.rb_EasyLevel.Location = new System.Drawing.Point(17, 35);
            this.rb_EasyLevel.Name = "rb_EasyLevel";
            this.rb_EasyLevel.Size = new System.Drawing.Size(48, 17);
            this.rb_EasyLevel.TabIndex = 0;
            this.rb_EasyLevel.TabStop = true;
            this.rb_EasyLevel.Text = "Easy";
            this.rb_EasyLevel.UseVisualStyleBackColor = true;
            // 
            // btn_PlayGame
            // 
            this.btn_PlayGame.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_PlayGame.Location = new System.Drawing.Point(79, 200);
            this.btn_PlayGame.Name = "btn_PlayGame";
            this.btn_PlayGame.Size = new System.Drawing.Size(103, 23);
            this.btn_PlayGame.TabIndex = 1;
            this.btn_PlayGame.Text = "Play Game";
            this.btn_PlayGame.UseVisualStyleBackColor = true;
            this.btn_PlayGame.Click += new System.EventHandler(this.btn_PlayGame_Click);
            // 
            // OptionsViewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(261, 247);
            this.Controls.Add(this.btn_PlayGame);
            this.Controls.Add(this.gb_SelectLevel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OptionsViewForm";
            this.Text = "Minesweeper - Options";
            this.gb_SelectLevel.ResumeLayout(false);
            this.gb_SelectLevel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gb_SelectLevel;
        private System.Windows.Forms.RadioButton rb_DifficultLevel;
        private System.Windows.Forms.RadioButton rb_ModerateLevel;
        private System.Windows.Forms.RadioButton rb_EasyLevel;
        private System.Windows.Forms.Button btn_PlayGame;
    }
}

