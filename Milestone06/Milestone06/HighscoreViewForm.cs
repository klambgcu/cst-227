﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-14
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Milestone 6
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create GUI forms for the beginning of Minesweeper game
 * 2. Main Form - Select Game options 
 * 3. Game Form - Form with button grid based upon chosen level 
 * 4. Merge Cell & Board classes
 * 5. Introduce flags, counter & timer
 * Objective:
 * Fully functional game play
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Milestone06.Models;
using Milestone06.Models.Interfaces;

namespace Milestone06
{
    public partial class HighscoreViewForm : Form
    {
        private IPlayerStatsManager manager;
        private IPlayerStats highScoreEntry;
        private int position = 0;
        private int level = 0;
        public HighscoreViewForm(IPlayerStatsManager manager, bool showPanel, IPlayerStats newEntry, int position, int level)
        {
            InitializeComponent();
            panel_EnterName.Visible = showPanel;
            this.manager = manager;
            lbl_headerLevel.Text = manager.getLevelDescription(level);
            this.highScoreEntry = newEntry;
            this.position = position;
            this.level = level;
            BindHighScoreDataGrid();

            if (showPanel)
            {
                lbl_ScoreValue.Text = highScoreEntry.Score.ToString();
                lbl_LevelDescription.Text = highScoreEntry.LevelDescription;
                tb_PlayerName.Focus();
            }
        }

        private void btn_Okay_Click(object sender, EventArgs e)
        {
            highScoreEntry.PlayerName = tb_PlayerName.Text;
            if (tb_PlayerName.Text.Trim().Equals("")) highScoreEntry.PlayerName = "IForgot";
            panel_EnterName.Visible = false;
            manager.Add(highScoreEntry);
            manager.Sort();
            BindHighScoreDataGrid();
        }

        private void BindHighScoreDataGrid()
        {
            dgv_HighScores.DataSource = manager.GetHighScores(5, (Levels)level);
            dgv_HighScores.Columns["Level"].Visible = false;
            dgv_HighScores.Columns["AdjustedScore"].Visible = false;
            dgv_HighScores.Rows[position].Selected = true;
            dgv_HighScores.CurrentCell = dgv_HighScores.Rows[position].Cells[0];
            dgv_HighScores.Rows[position].Selected = true;
        }
    }
}
