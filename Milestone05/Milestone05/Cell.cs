﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-07
 * Class     : CST-227 Enterprise Computer Promgramming II
 * Professor : James Shinevar
 * Assignment: Milestone 5
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create GUI forms for the beginning of Minesweeper game
 * 2. Main Form - Select Game options 
 * 3. Game Form - Form with button grid based upon chosen level 
 * 4. Merge Cell & Board classes
 * 5. Introduce flags, counter & timer
 * Objective:
 * Fully functional game play
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Milestone05
{
    // Define Cell class used to represent cell in a Mine Sweeper game
    public class Cell
    {
        // Default constructor - chain to next constructor
        public Cell() : this(0, 0) { }

        // Constructor for setting row, col
        public Cell(int row, int col)
        {
            Row = row;
            Col = col;
            Live = false;
            Visited = false;
            Flagged = false;
            Neighbors = 0;
        }

        // Row Cell resides within container grid
        public int Row { get; set; }

        // Column Cell resides within container grid
        public int Col { get; set; }

        // Count of neighbors containing bombs
        public int Neighbors { get; set; }

        // Live bomb = true, false no a bomb
        public bool Live { get; set; }

        // Determine if this cell has been visited
        public bool Visited { get; set; }

        public bool Flagged { get; set; }

        // Helper routine to ease setting Neighbor count
        public void incrementNeighbors()
        {
            Neighbors++;
            if (Live) Neighbors = 9; // Restrict to nine
        }

        // Reset properties should we want to play again
        public void reset()
        {
            Neighbors = 0;
            Live = false;
            Visited = false;
        }
     }
}
