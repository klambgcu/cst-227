﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-07
 * Class     : CST-227 Enterprise Computer Promgramming II
 * Professor : James Shinevar
 * Assignment: Milestone 5
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create GUI forms for the beginning of Minesweeper game
 * 2. Main Form - Select Game options 
 * 3. Game Form - Form with button grid based upon chosen level 
 * 4. Merge Cell & Board classes
 * 5. Introduce flags, counter & timer
 * Objective:
 * Fully functional game play
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Milestone05
{
    public class Board
    {
        private Random random;
        private int cellCount = 0;
        private int neutralCount = 0;
        private int visitedCount = 0;

        public Cell[,] grid;
        public int Size { get; set; } // Size of square grid
        public int Difficulty { get; set; } // Percentage (0-100) of live bombs

        public int BombCount { get; set; }

        public int FlagCount { get; set; }

        // Default constructor - chain to next constructor
        // Default size = 10
        public Board() : this(10) { }

        // Constructor - main
        public Board(int size)
        {
            Size = size;
            grid = new Cell[size, size];
            for (int row = 0; row < Size; row++)
                for (int col = 0; col < Size; col++)
                {
                    grid[row, col] = new Cell(row, col);
                }
 
            Difficulty = 10; // Default to 10 percent
            cellCount = Size * Size;
            BombCount = (int)(cellCount * (Difficulty / 100.0));
            neutralCount = cellCount - BombCount;
            FlagCount = 0;

            // Create random number generator - this method of seeding for variety
            // is the only method on my system that worked appropriately.
            random = new Random((int)Guid.NewGuid().GetHashCode());
        }

        // Determine (randomly) location for bombs based upon size and difficulty
        public void setupLiveNeighbors()
        {
            cellCount = Size * Size;
            BombCount = (int) (cellCount * (Difficulty / 100.0));
            neutralCount = cellCount - BombCount;
            for (int i = 0; i < BombCount; i++)
            {
                int x = random.Next(Size);
                int y = random.Next(Size);
                while (grid[x,y].Live)
                {
                    x = random.Next(Size);
                    y = random.Next(Size);
                }
                grid[x, y].Live = true;
                grid[x, y].Neighbors = 9;
            }
        }

        // Calculate the number of adjacent cells contain bombs
        public void calculateLiveNeighbors()
        {
            int Size1 = Size - 1;
            for (int row = 0; row < Size; row++)
            {
                for (int col = 0; col < Size; col++)
                {
                    if (grid[row,col].Live)
                    {
                        if (row > 0 && col > 0) grid[row - 1, col - 1].incrementNeighbors();
                        if (row > 0) grid[row - 1, col].incrementNeighbors();
                        if (row > 0 && col < Size1) grid[row - 1, col + 1].incrementNeighbors();

                        if (col > 0) grid[row, col - 1].incrementNeighbors();
                        if (col < Size1) grid[row, col + 1].incrementNeighbors();

                        if (row < Size1 && col > 0) grid[row + 1, col - 1].incrementNeighbors();
                        if (row < Size1) grid[row + 1, col].incrementNeighbors();
                        if (row < Size1 && col < Size1) grid[row + 1, col + 1].incrementNeighbors();
                    }
                }
            }
        }

        // Accessor to set cell visitation and increment visited count
        public void setCellVisited(int row, int col)
        {
            // Cannot visit flagged cells
            if (grid[row, col].Flagged) return;

            if (!grid[row,col].Visited) 
            {
                grid[row, col].Visited = true;
                visitedCount++;
            }
        }

        // Accessor to toggle cell between flagged and not flagged
        public void toggleFlag(int row, int col)
        {
            // Cannot modify flag on visited cells
            if (grid[row, col].Visited) return;

            // Cannot flag more cells than number of bombs
            if (FlagCount == BombCount && !grid[row, col].Flagged) return;

            // Toggle cell flag and adjust flag count
            grid[row, col].Flagged = !grid[row, col].Flagged;
            if (grid[row, col].Flagged)
                FlagCount++;
            else
                FlagCount--;
        }

        // Accessor that determines if all non-bomb cells have been visited
        public bool boardCompleted()
        {
            return (FlagCount == BombCount && visitedCount == neutralCount);
        }

        // Reset game board should we want to play again
        public void reset()
        {
            for (int row = 0; row < Size; row++)
                for (int col = 0; col < Size; col++)
                    grid[row, col].reset();
            visitedCount = 0;
        }

        // Recursively check neighbors to expose empty cells (8 Direction)
        // I believe the real game utilized the 8 direction
        // flood fill but the example appears to use 4 direction
        public void floodFill(int row, int col)
        {
            setCellVisited(row, col);

            // Check Orthogonal Directions
            // Check Down
            if (row < Size - 1 && grid[row, col].Neighbors == 0 && 
                !grid[row + 1, col].Visited && 
                !grid[row + 1, col].Flagged) floodFill(row + 1, col);
            
            // Check Left
            if (col > 0 && grid[row, col].Neighbors == 0 &&
                !grid[row, col - 1].Visited &&
                !grid[row, col - 1].Flagged) floodFill(row, col - 1);

            // Check Up
            if (row > 0 && grid[row, col].Neighbors == 0 && 
                !grid[row - 1, col].Visited &&
                !grid[row - 1, col].Flagged) floodFill(row - 1, col);

            // Check Right
            if (col < Size - 1 && grid[row, col].Neighbors == 0 &&
                !grid[row, col + 1].Visited &&
                !grid[row, col + 1].Flagged) floodFill(row, col + 1);
       
            // Check Diagonal Directions 
            // Check Down Right
            if (row < Size - 1 && col < Size - 1 &&
                grid[row, col].Neighbors == 0 &&
                !grid[row + 1, col + 1].Visited &&
                !grid[row + 1, col + 1].Flagged) floodFill(row + 1, col + 1);

            // Check Up Left
            if (row > 0 && col > 0 &&
                grid[row, col].Neighbors == 0 &&
                !grid[row - 1, col - 1].Visited &&
                !grid[row - 1, col - 1].Flagged) floodFill(row - 1, col - 1);

            // Check Up Right
            if (row > 0 && col < Size - 1 &&
                grid[row, col].Neighbors == 0 &&
                !grid[row - 1, col + 1].Visited &&
                !grid[row - 1, col + 1].Flagged) floodFill(row - 1, col + 1);

            // Check Down Left
            if (row < Size - 1 && col > 0 &&
                grid[row, col].Neighbors == 0 &&
                !grid[row + 1, col - 1].Visited &&
                !grid[row + 1, col - 1].Flagged) floodFill(row + 1, col - 1);
        }
    }
}
