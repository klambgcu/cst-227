﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-07
 * Class     : CST-227 Enterprise Computer Promgramming II
 * Professor : James Shinevar
 * Assignment: Milestone 5
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create GUI forms for the beginning of Minesweeper game
 * 2. Main Form - Select Game options 
 * 3. Game Form - Form with button grid based upon chosen level 
 * 4. Merge Cell & Board classes
 * 5. Introduce flags, counter & timer
 * Objective:
 * Fully functional game play
 * ---------------------------------------------------------------
 */

namespace Milestone05
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.panelButtonGrid = new System.Windows.Forms.Panel();
            this.panelStatus = new System.Windows.Forms.Panel();
            this.pb_Smiley = new System.Windows.Forms.PictureBox();
            this.lblSecondCount = new System.Windows.Forms.Label();
            this.lblFlagCount = new System.Windows.Forms.Label();
            this.timerSeconds = new System.Windows.Forms.Timer(this.components);
            this.panelStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Smiley)).BeginInit();
            this.SuspendLayout();
            // 
            // panelButtonGrid
            // 
            this.panelButtonGrid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelButtonGrid.Location = new System.Drawing.Point(0, 60);
            this.panelButtonGrid.Name = "panelButtonGrid";
            this.panelButtonGrid.Size = new System.Drawing.Size(334, 250);
            this.panelButtonGrid.TabIndex = 0;
            // 
            // panelStatus
            // 
            this.panelStatus.BackColor = System.Drawing.Color.Gainsboro;
            this.panelStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelStatus.Controls.Add(this.pb_Smiley);
            this.panelStatus.Controls.Add(this.lblSecondCount);
            this.panelStatus.Controls.Add(this.lblFlagCount);
            this.panelStatus.Location = new System.Drawing.Point(0, 0);
            this.panelStatus.Name = "panelStatus";
            this.panelStatus.Size = new System.Drawing.Size(334, 60);
            this.panelStatus.TabIndex = 1;
            // 
            // pb_Smiley
            // 
            this.pb_Smiley.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb_Smiley.Image = global::Milestone05.Properties.Resources.status_smilenew;
            this.pb_Smiley.Location = new System.Drawing.Point(150, 12);
            this.pb_Smiley.Name = "pb_Smiley";
            this.pb_Smiley.Size = new System.Drawing.Size(33, 33);
            this.pb_Smiley.TabIndex = 2;
            this.pb_Smiley.TabStop = false;
            this.pb_Smiley.Click += new System.EventHandler(this.pb_Smiley_Click);
            // 
            // lblSecondCount
            // 
            this.lblSecondCount.AutoSize = true;
            this.lblSecondCount.BackColor = System.Drawing.Color.Black;
            this.lblSecondCount.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSecondCount.Font = new System.Drawing.Font("Goudy Old Style", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSecondCount.ForeColor = System.Drawing.Color.Red;
            this.lblSecondCount.Location = new System.Drawing.Point(263, 12);
            this.lblSecondCount.Name = "lblSecondCount";
            this.lblSecondCount.Size = new System.Drawing.Size(58, 33);
            this.lblSecondCount.TabIndex = 1;
            this.lblSecondCount.Text = "000";
            // 
            // lblFlagCount
            // 
            this.lblFlagCount.AutoSize = true;
            this.lblFlagCount.BackColor = System.Drawing.Color.Black;
            this.lblFlagCount.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFlagCount.Font = new System.Drawing.Font("Goudy Old Style", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFlagCount.ForeColor = System.Drawing.Color.Red;
            this.lblFlagCount.Location = new System.Drawing.Point(12, 12);
            this.lblFlagCount.Name = "lblFlagCount";
            this.lblFlagCount.Size = new System.Drawing.Size(58, 33);
            this.lblFlagCount.TabIndex = 0;
            this.lblFlagCount.Text = "010";
            // 
            // timerSeconds
            // 
            this.timerSeconds.Interval = 1000;
            this.timerSeconds.Tick += new System.EventHandler(this.secondTimer_Tick);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 311);
            this.Controls.Add(this.panelStatus);
            this.Controls.Add(this.panelButtonGrid);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form2";
            this.Text = "Minesweeper";
            this.panelStatus.ResumeLayout(false);
            this.panelStatus.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Smiley)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelButtonGrid;
        private System.Windows.Forms.Panel panelStatus;
        private System.Windows.Forms.Label lblSecondCount;
        private System.Windows.Forms.Label lblFlagCount;
        private System.Windows.Forms.Timer timerSeconds;
        private System.Windows.Forms.PictureBox pb_Smiley;
    }
}