﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-07
 * Class     : CST-227 Enterprise Computer Promgramming II
 * Professor : James Shinevar
 * Assignment: Milestone 5
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create GUI forms for the beginning of Minesweeper game
 * 2. Main Form - Select Game options 
 * 3. Game Form - Form with button grid based upon chosen level 
 * 4. Merge Cell & Board classes
 * 5. Introduce flags, counter & timer
 * Objective:
 * Fully functional game play
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milestone05
{
    public partial class Form1 : Form
    {
        // Constructor
        public Form1()
        {
            InitializeComponent();
            // Display the form in the center of the screen.
            StartPosition = FormStartPosition.CenterScreen;
        }

        //
        // Obtain player game preference & call game form (Form2) with choice (level)
        //
        private void btn_PlayGame_Click(object sender, EventArgs e)
        {
            int level = 0;

            if (rb_EasyLevel.Checked) level = 0;
            if (rb_ModerateLevel.Checked) level = 1;
            if (rb_DifficultLevel.Checked) level = 2;

            Form2 gameForm = new Form2(level);
            gameForm.Show();
            //this.Hide();
        }
    }
}
