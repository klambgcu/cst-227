﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-07
 * Class     : CST-227 Enterprise Computer Promgramming II
 * Professor : James Shinevar
 * Assignment: Milestone 5
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create GUI forms for the beginning of Minesweeper game
 * 2. Main Form - Select Game options 
 * 3. Game Form - Form with button grid based upon chosen level 
 * 4. Merge Cell & Board classes
 * 5. Introduce flags, counter & timer
 * Objective:
 * Fully functional game play
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace Milestone05
{
    public partial class Form2 : Form
    {
        // Define states for game loop logic
        private enum GameState
        {
            GamePlaying,
            GameOverWin,
            GameOverLose
        }

        //
        // Initialize Form variables
        //
        private GameState GameStatus = GameState.GamePlaying;

        private int[,] gameLevels = { { 10, 10 },
                                      { 15, 18 },
                                      { 20, 25 } }; // Board Size, Bomb Percentage
        private Color[] gameColors = { Color.White,  Color.Blue,   Color.Green,
                                       Color.Red,    Color.Orange, Color.Yellow,
                                       Color.Purple, Color.Cyan,   Color.Brown, Color.White};
        private int bombPercentage = 10;
        private int buttonSize = 30;
        private int size = 10;
        private int gameLevel = 0;
        private Button[,] btnGrid;
        private Board board;
        private Point LastButton = new Point(0, 0);
        private static Stopwatch watch = new Stopwatch();

        // Constructor
        // level determines size/difficulty sent from Form1
        public Form2(int level)
        {
            gameLevel = level;
            InitializeComponent();
            InitializeGame(level, true);
        }

        //
        // Define game board and setup mines based upon size/difficulty
        //
        private void InitializeGame(int level, bool newGame)
        {
            GameStatus = GameState.GamePlaying;
            size = gameLevels[level, 0];
            bombPercentage = gameLevels[level, 1];

            // Dynamically set interior of form to hold game board
            // Make playfield square, add height for status panel
            ClientSize = new Size(size * buttonSize + 1, (size + 2) * buttonSize + 1);

            // Display the form in the center of the screen.
            StartPosition = FormStartPosition.CenterScreen;

            board = new Board(size);
            board.Difficulty = bombPercentage;
            board.setupLiveNeighbors();
            board.calculateLiveNeighbors();
            if (newGame) InitializeBoardGrid();
            InitializeStatusPanel();
        }

        //
        // Define game status panel: flag and second counter
        //
        private void InitializeStatusPanel()
        {
            // Adjust panel width and second label placement
            panelStatus.Width = panelButtonGrid.Width;
            lblSecondCount.Left = panelStatus.Width - (lblSecondCount.Width + lblFlagCount.Left);
            pb_Smiley.Left = (panelStatus.Width / 2) - 16;
            watch.Reset();
            timerSeconds.Enabled = false;
            pb_Smiley.Image = Properties.Resources.status_smilenew;
            lblSecondCount.Text = "000";
            statusPanelFlagsUpdate();
        }

        //
        // Define the game board button grid
        //
        public void InitializeBoardGrid()
        {
            btnGrid = new Button[size, size];

            // Resize panel to a square - could set panel1.autosize
            panelButtonGrid.Height = (size * buttonSize);
            panelButtonGrid.Width = (size * buttonSize);

            // Nested loop. Create buttons and place them in the Panel
            for (int r = 0; r < size; r++)
            {
                for (int c = 0; c < size; c++)
                {
                    Button tmpButton = new Button();
                    //btnGrid[r, c] = new Button();

                    // Make each button square
                    tmpButton.Width = buttonSize;
                    tmpButton.Height = buttonSize;

                    // Define default button behaviors
                    tmpButton.Text = "";
                    tmpButton.BackColor = Color.Silver;
                    tmpButton.MouseDown += Grid_Button_MouseDown;
                    tmpButton.MouseUp += Grid_Button_MouseUp;
                    tmpButton.Location = new Point(buttonSize * r, buttonSize * c); // Position it in x,y
                    tmpButton.Font = new Font(tmpButton.Font.FontFamily, 14, FontStyle.Bold);
                    tmpButton.Padding = new Padding(0);
                    tmpButton.TextAlign = ContentAlignment.TopCenter;
                    tmpButton.BackgroundImageLayout = ImageLayout.Zoom;


                    // The Tag attribute will hold the row and column number in a string
                    tmpButton.Tag = r.ToString() + "|" + c.ToString();

                    // Add to the game board button panel
                    panelButtonGrid.Controls.Add(tmpButton);
                    btnGrid[r, c] = tmpButton;
                }
            }
        }

        //
        // Update game board button grid 
        //
        private void gridUpdateDisplay()
        {
            for (int row = 0; row < size; row++)
            {
                for (int col = 0; col < size; col++)
                {
                    if (board.grid[row, col].Flagged)
                    {
                        btnGrid[row, col].BackgroundImage = Properties.Resources.cell_flag;
                    }
                    else if (board.grid[row, col].Visited)
                    {
                        int value = board.grid[row, col].Neighbors;
                        if (value > 0)
                        {
                            btnGrid[row, col].Text = value.ToString();
                            btnGrid[row, col].ForeColor = gameColors[value];
                        }
                        btnGrid[row, col].BackColor = Color.Gainsboro;
                    }
                }
            }
        }

        //
        // Show bombs and hit bomb at Game Over Lose
        //
        private void gridExposeBombs()
        {
            for (int row = 0; row < size; row++)
            {
                for (int col = 0; col < size; col++)
                {
                    if (LastButton.X == col && LastButton.Y == row && board.grid[row, col].Live)
                    {
                        btnGrid[row, col].BackgroundImage = Properties.Resources.cell_bombhit;
                        btnGrid[row, col].Text = "";
                    }
                    else if (board.grid[row, col].Flagged && !board.grid[row, col].Live)
                    {
                        btnGrid[row, col].BackgroundImage = Properties.Resources.cell_bombmiss;
                    }
                    else if (board.grid[row, col].Live)
                    {
                        btnGrid[row, col].BackgroundImage = Properties.Resources.cell_bomb;
                    }
                }
            }
        }

        //
        // Helper: Put smile on smiley's face
        //
        private void Grid_Button_MouseUp(object sender, MouseEventArgs me)
        {
            // Continue only if game is not over (i.e. Playing)
            if (GameStatus != GameState.GamePlaying) return;

            pb_Smiley.Image = Properties.Resources.status_smilenew;

        }
        //
        // Main button click/down reaction method
        // 
        private void Grid_Button_MouseDown(object sender, MouseEventArgs me)
        {
            // Continue only if game is not over (i.e. Playing)
            if (GameStatus != GameState.GamePlaying) return;

            // Enable stop watch / timer on first click
            if (timerSeconds.Enabled == false)
            {
                watch.Start();
                timerSeconds.Enabled = true;
            }

            // Get the row and column number of the button just clicked.
            string[] strArr = (sender as Button).Tag.ToString().Split('|');
            int row = int.Parse(strArr[0]);
            int col = int.Parse(strArr[1]);

            // Save the Last Button Clicked
            LastButton = new Point(col, row);

            // Check Right Click to toggle flag placement
            if (me.Button == MouseButtons.Right)
            {
                board.toggleFlag(row, col);
                statusPanelFlagsUpdate();
                btnGrid[row, col].BackgroundImage = null;
                gridUpdateDisplay();
            }
            else
            {
                // Set smiley face to ohoh.
                pb_Smiley.Image = Properties.Resources.status_smileohoh;

                // Set cell chosen/visited
                if (board.grid[row, col].Neighbors == 0)
                {
                    board.floodFill(row, col);
                    gridUpdateDisplay();
                }
                else
                {
                    board.setCellVisited(row, col);
                    gridUpdateDisplay();
                }

                // Check game completed - Bomb Chosen
                if (board.grid[row, col].Live)
                {
                    GameStatus = GameState.GameOverLose;
                    GameOver();
                }
            }

            // Check game completed - Board cleared
            if (GameStatus == GameState.GamePlaying && board.boardCompleted())
            {
                GameStatus = GameState.GameOverWin;
                GameOver();
            }
        }

        //
        // Perform game over procedures
        //
        private void GameOver()
        {
            secondTimerStop();
            string msg;

            if (GameStatus == GameState.GameOverWin)
            {
                msg = "    Congratulations! " + Environment.NewLine + "           You Win! ";
                pb_Smiley.Image = Properties.Resources.status_smilehappy;
            }
            else
            {
                gridExposeBombs();
                msg = "          Oh no!     " + Environment.NewLine + "     Sorry you lose.";
                pb_Smiley.Image = Properties.Resources.status_smilesad;
            }

            msg = msg + Environment.NewLine + "Seconds Elapsed: " + lblSecondCount.Text;

            MessageBox.Show(msg, "Game Over", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        //
        // Helper: Update status panel flag counter
        //
        private void statusPanelFlagsUpdate()
        {
            int amount = board.BombCount - board.FlagCount;
            lblFlagCount.Text = String.Format("{0:D3}", amount);
        }

        //
        // Helper: Stop second elapsed counter
        //
        private void secondTimerStop()
        {
            watch.Stop();
            timerSeconds.Enabled = false;
        }

        //
        // Timed event to update seconds elasped in game play
        //
        private void secondTimer_Tick(object sender, EventArgs e)
        {
            // Apply a TimeSpan to format the output 
            TimeSpan ts = watch.Elapsed;

            int elapsed = (ts.Minutes * 60) + ts.Seconds;

            // Use String.Format option for easy reading
            string elapsedTime = String.Format("{0:D3}", elapsed);

            lblSecondCount.Text = elapsedTime;

            // Time Limit Exceeded - End Game
            if (elapsed == 999)
            {
                GameStatus = GameState.GameOverLose;
                GameOver();
            }
        }

        // Click smiley to reset the game
        private void pb_Smiley_Click(object sender, EventArgs e)
        {
            // Nested loop. Create buttons and place them in the Panel
            for (int r = 0; r < size; r++)
            {
                for (int c = 0; c < size; c++)
                {
                    btnGrid[r, c].BackgroundImage = null;
                    btnGrid[r, c].Text = null;
                    btnGrid[r, c].BackColor = Color.Silver;
                }
            }
            InitializeGame(gameLevel, false);
        }
    }
}
