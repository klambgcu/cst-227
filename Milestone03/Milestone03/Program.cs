﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-08-24
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Milestone 3
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * Create classes to represent Cells and Game Board for the 
 * Mine Sweeper game. (Recursive Interactive Version)
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Milestone03
{
    public static class BoxChars
    {
        // Define Box Characters
        public static char LineVertical   = '│'; // 2483
        public static char LineHorizontal = '─'; // 2500

        public static char CornerBR       = '┘'; // 2521
        public static char CornerTL       = '┌'; // 2522
        public static char CornerTR       = '┐'; // 2495
        public static char CornerBL       = '└'; // 2496

        public static char TeeBottom      = '┴'; // 2497
        public static char TeeRight       = '┤'; // 2484
        public static char TeeTop         = '┬'; // 2498
        public static char TeeLeft        = '├'; // 2499
        public static char TeeFull        = '┼'; // 2501
    }

    class Program
    {
        // Define states for game loop logic
        enum GameState
        {
            GamePlaying,
            GameOverWin,
            GameOverLose
        }

        static void Main(string[] args)
        {
            //
            // Prompt Size and Difficulty
            //
            int size = getValidNumericEntry("Enter size of board(5 - 20): ", 5, 20);
            int difficulty = getValidNumericEntry("Enter game difficulty percentage (1-100): ", 1, 100);

            //
            // Define game board and setup mines based upon size/difficulty
            //
            Board board = new Board(size);
            board.Difficulty = difficulty;
            board.setupLiveNeighbors();
            board.calculateLiveNeighbors();

            //
            // Initialize game status
            //
            GameState GameStatus = GameState.GamePlaying;

            // -----------------------------------------
            // Main Game Loop (Start)
            // -----------------------------------------
            while (GameStatus == GameState.GamePlaying)
            {
                // Display game board
                PrintBoardDuringGame(board);

                // Request choice from user
                int row = getValidNumericEntry("Enter a Row Number: ", 0, board.Size - 1);
                int col = getValidNumericEntry("Enter a Column Number: ", 0, board.Size - 1);

                // Set cell chosen/visited
                // Note: Should I check and inform if cell already visited?
                // board.setCellVisited(row, col);
                if (board.grid[row, col].Neighbors == 0)
                {
                    board.floodFill(row, col);
                }
                else
                {
                    board.setCellVisited(row, col);
                }

                // Check game completed - Bomb Chosen or Board Cleared
                if (board.grid[row, col].Live)
                {
                    GameStatus = GameState.GameOverLose;
                }
                else if (board.boardCompleted())
                {
                    GameStatus = GameState.GameOverWin;
                }
            }
            // -----------------------------------------
            // Main Game Loop (End)
            // -----------------------------------------

            //
            // Game Over - display whole board and messages
            //
            printBoard(board);

            //
            // Display Game Over Result Message
            //
            Console.Out.WriteLine();

            if (GameStatus == GameState.GameOverWin)
            {
                Console.Out.WriteLine("Congratulations! You WIN!!!");
            }
            else
            {
                Console.Out.WriteLine("Sorry! You Hit a Bomb! (Please Try Again)");
            }
            Console.Out.WriteLine();
            Console.Out.Write("\nPress Enter to Exit.");
            Console.ReadLine();
        }

        // Helper method to display game board (Show all values for grid)
        static public void printBoard(Board board)
        {
            // Clear the screen for consistent display
            Console.Clear();
            Console.Out.WriteLine();

            // Print Column Header
            Console.Out.Write(" ");

            for (int col = 0; col < board.Size; col++)
                Console.Out.Write(" {0,-2} ", col);

            Console.Out.WriteLine();

            // Print Top Separation Line
            Console.Out.Write(BoxChars.CornerTL);

            for (int col = 0; col < board.Size - 1; col++)
                Console.Out.Write("{0}{1}{2}{3}",
                    BoxChars.LineHorizontal,
                    BoxChars.LineHorizontal,
                    BoxChars.LineHorizontal,
                    BoxChars.TeeTop);

            Console.Out.WriteLine("{0}{1}{2}{3}",
                BoxChars.LineHorizontal,
                BoxChars.LineHorizontal,
                BoxChars.LineHorizontal,
                BoxChars.CornerTR);
 
            // Double Loop to print board contents
            for (int row = 0; row < board.Size; row++)
            {
                Console.Out.Write(BoxChars.LineVertical);
                for (int col = 0; col < board.Size; col++)
                {
                    if (board.grid[row, col].Live)
                    {
                        // Display live bombs as black on red asterisk
                        Console.Out.Write(" ");
                        ConsoleColor holdBack = Console.BackgroundColor;
                        ConsoleColor holdFore = Console.ForegroundColor;
                        Console.BackgroundColor = ConsoleColor.Red;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.Out.Write("*"); 
                        Console.BackgroundColor = holdBack;  // Restore previous colors
                        Console.ForegroundColor = holdFore;
                        Console.Out.Write(" {0}", BoxChars.LineVertical);
                    }
                    else
                    {
                        // Add color when displaying neighbor values
                        ConsoleColor hold = Console.ForegroundColor;
                        Console.ForegroundColor = (ConsoleColor)(board.grid[row, col].Neighbors + 8 & 15); // Max 16 colors
                        Console.Out.Write(" {0} ", board.grid[row, col].Neighbors);
                        Console.ForegroundColor = hold;
                        Console.Out.Write(BoxChars.LineVertical);
                    }
                }

                // Print Row Header
                Console.Out.WriteLine(" {0}", row);

                // Print Body Separation Lines
                Console.Out.Write("{0}", (row == board.Size - 1) ? BoxChars.CornerBL : BoxChars.TeeLeft);

                for (int col = 0; col < board.Size - 1; col++)
                    Console.Out.Write("{0}{1}{2}{3}",
                        BoxChars.LineHorizontal,
                        BoxChars.LineHorizontal,
                        BoxChars.LineHorizontal,
                        (row == board.Size - 1) ? BoxChars.TeeBottom : BoxChars.TeeFull);

                Console.Out.WriteLine("{0}{1}{2}{3}",
                    BoxChars.LineHorizontal,
                    BoxChars.LineHorizontal,
                    BoxChars.LineHorizontal,
                    (row == board.Size - 1) ? BoxChars.CornerBR : BoxChars.TeeRight);
            }

            Console.Out.WriteLine();
        }

        // Helper method to display game board during game (Display '?' for unvisited cells)
        static public void PrintBoardDuringGame(Board board)
        {
            // Clear the screen for consistent display
            Console.Clear();
            Console.Out.WriteLine();

            // Print Column Header
            Console.Out.Write(" ");

            for (int col = 0; col < board.Size; col++)
                Console.Out.Write(" {0,-2} ", col);

            Console.Out.WriteLine();

            // Print Top Separation Line
            Console.Out.Write(BoxChars.CornerTL);

            for (int col = 0; col < board.Size-1; col++)
                Console.Out.Write("{0}{1}{2}{3}",
                    BoxChars.LineHorizontal,
                    BoxChars.LineHorizontal,
                    BoxChars.LineHorizontal,
                    BoxChars.TeeTop);
 
            Console.Out.WriteLine("{0}{1}{2}{3}",
                BoxChars.LineHorizontal,
                BoxChars.LineHorizontal,
                BoxChars.LineHorizontal,
                BoxChars.CornerTR);

            // Double Loop to print board contents
            for (int row = 0; row < board.Size; row++)
            {
                Console.Out.Write(BoxChars.LineVertical);
                for (int col = 0; col < board.Size; col++)
                {
                    if (board.grid[row, col].Visited)
                    {
                        if (board.grid[row, col].Live)
                        {
                            Console.Out.Write(" * {0}", BoxChars.LineVertical); // Live bombs display an asterisk
                        }
                        else if (board.grid[row, col].Neighbors == 0)
                        {
                            Console.Out.Write("   {0}", BoxChars.LineVertical); // Empty cell - display a blank
                        }
                        else
                        {
                            // Add color when displaying neighbor values
                            ConsoleColor hold = Console.ForegroundColor;
                            Console.ForegroundColor = (ConsoleColor)(board.grid[row, col].Neighbors + 8 & 15); // Max 16 colors
                            Console.Out.Write(" {0} ", board.grid[row, col].Neighbors);
                            Console.ForegroundColor = hold;
                            Console.Out.Write(BoxChars.LineVertical);
                        }
                    }
                    else
                    {
                        Console.Out.Write(" ? {0}", BoxChars.LineVertical); // Not visited - display question mark
                    }
                }

                // Print Row Header
                Console.Out.WriteLine(" {0}", row);

                // Print Body Separation Lines
                Console.Out.Write("{0}", (row == board.Size - 1) ? BoxChars.CornerBL : BoxChars.TeeLeft);
 
                for (int col = 0; col < board.Size-1; col++)
                    Console.Out.Write("{0}{1}{2}{3}",
                        BoxChars.LineHorizontal,
                        BoxChars.LineHorizontal,
                        BoxChars.LineHorizontal, 
                        (row == board.Size - 1) ? BoxChars.TeeBottom : BoxChars.TeeFull);

                Console.Out.WriteLine("{0}{1}{2}{3}",
                    BoxChars.LineHorizontal,
                    BoxChars.LineHorizontal,
                    BoxChars.LineHorizontal,
                    (row == board.Size-1) ? BoxChars.CornerBR : BoxChars.TeeRight);
            }

            Console.Out.WriteLine();
        }

        // Validate numeric (int) entry: present appropriate warning error messages if outside of range/invalid entry
        // Replaces chooseAction method
        static public int getValidNumericEntry(string msg, int low, int high)
        {
            int answer = low - 1;

            // Ensure user chooses a valid option
            while (answer < low || answer > high)
            {
                Console.Out.Write(msg);
                try
                {
                    answer = int.Parse(Console.ReadLine());
                    if (answer < low || answer > high)
                    {
                        Console.Out.WriteLine("Warning: Valid entry must be between {0} and {1}.", low, high);
                    }
                }
                catch (FormatException)
                {
                    Console.Out.WriteLine("Error: Entry must be an integer between {0} and {1}.", low, high);
                }
            }
            return answer;
        }
    }
}
