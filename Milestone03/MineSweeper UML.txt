Cell

- row: int
- col: int
- visited: bool
- live: bool
- neighbors: int

+ Cell() 
+ Cell(row, col) 
+ setRow(int): void
+ setCol(int): void
+ setVisited(bool): void
+ setLive(bool): void
+ setNeighbors(int): void
+ getRow(): int
+ getCol(): int
+ getVisited(): bool
+ getLive(): bool
+ getNeighbors(): int
+ incrementNeighbors(): void
+ reset(): void 

===========================================

Board

- size: int
- grid: Cell[,]
- difficulty: int
- cellCount: int
- bombCount: int
- neutralCount: int
- visitedCount: int


+ Board() 
+ Board(int) 
+ setSize(int): void
+ setDifficulty(int): void
+ getSize(): int
+ getDifficulty(): int
+ setupLiveNeighbors(): void
+ calculateLiveNeighbors(): void
+ reset(): void 
+ setCellVisited(int,int): void
+ boardCompleted(): bool
+ floodFill(int, int): void

===========================================

Program

- board: Board
- GameStatus: GameState

+ main(string[]): int
	Board
	Board.setupLiveNeighbors()
	Board.calculateLiveNeighbors()
	printBoard()
+ printBoard(Board): void
+ PrintBoardDuringGame(Board): void
+ getValidNumericEntry(string, int, int): int
===========================================

<<enumeration>>
GameState

GamePlaying
GameOverWin
GameOverLose

===========================================

<<static>>
BoxChars

+ LineVertical: char
+ LineHorizontal: char
+ CornerBR: char
+ CornerTL: char
+ CornerTR: char
+ CornerBL: char
+ TeeBottom: char
+ TeeRight: char
+ TeeTop: char
+ TeeLeft: char
+ TeeFull: char

