﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-08-10
 * Class     : CST-227 Enterprise Computer Promgramming II
 * Professor : James Shinevar
 * Assignment: Activity 1
 * ---------------------------------------------------------------
 * Description:
 * 1. Create CarClassLibrary: Car, Store Classes
 * 2. Create Console Application to use the CarClassLibrary
 * 3. Create GUI Application to use the CarClassLibrary
 * Objective: Create applications that both utilize the same
 * underlying classes (library) to simulate a car store and 
 * shopping experience.
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarClassLibrary
{
    public class Car
    {
        public string Make { get; set; }
        public string Model { get; set; }
        public decimal Price { get; set; }
        public string Color { get; set; }
        public int Miles { get; set; }
        public string Display
        {
            get
            {
                return string.Format("{0} {1} {2} {3} ${4}", Make, Model, Color, Miles, Price);
            }
        }

        // Car construcot with 5 parameters.
        public Car(string make, string model, string color, int miles, decimal price)
        {
            Make = make;
            Model = model;
            Color = color;
            Miles = miles;
            Price = price;
        }
        // Car constructor with 0 parameters. Provide default values.
        public Car()
        {
            Make = "Nothing yet";
            Model = "Nothing yet";
            Color = "Primer";
            Miles = 0;
            Price = 0;
        }
    }
}
