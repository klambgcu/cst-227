﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-08-10
 * Class     : CST-227 Enterprise Computer Promgramming II
 * Professor : James Shinevar
 * Assignment: Activity 1
 * ---------------------------------------------------------------
 * Description:
 * 1. Create CarClassLibrary: Car, Store Classes
 * 2. Create Console Application to use the CarClassLibrary
 * 3. Create GUI Application to use the CarClassLibrary
 * Objective: Create applications that both utilize the same
 * underlying classes (library) to simulate a car store and 
 * shopping experience.
 * ---------------------------------------------------------------
 */

using CarClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarShopConsoleApp
{
    class Program
    {
        static Store CarStore = new Store();
        static void Main(string[] args)
        {
            // Display header information
            Console.Out.WriteLine("=========================");
            Console.Out.WriteLine("Welcome to the car store.");
            Console.Out.WriteLine("=========================");
            Console.Out.WriteLine("First you must create some cars and put them into the store inventory.");
            Console.Out.WriteLine("Then you may add cars to the cart.");
            Console.Out.WriteLine("Finally, you may checkout, which will calculate your total bill.");
            
            int action = getValidNumericEntry("Choose an action: (0) quit (1) add a car (2) add item to cart (3) checkout: ", 0, 3);

            while (action != 0)
            {
                // Determine handler based upon chosen action.
                switch (action)
                {
                    case 1:
                        // You chose add a car
                        Console.Out.WriteLine("You chose to add a new car to the store:");

                        // Ask for three property details
                        String carMake = "";
                        String carModel = "";
                        String carColor = "";
                        int carMiles = 0;
                        Decimal carPrice = 0;

                        Console.Out.Write("What is the car make? Ford, GM, Toyota, etc.: ");
                        carMake = Console.ReadLine();

                        Console.Out.Write("What is the car model? Corvette, Focus, Ranger: ");
                        carModel = Console.ReadLine();

                        Console.Out.Write("What is the car color? White, Red, Blue: ");
                        carColor = Console.ReadLine();

                        carMiles = getValidNumericEntry("What is the car mileage? Only numbers please: ", 0, int.MaxValue);

                        carPrice = getValidNumericEntry("What is the car price? Only numbers please: ", 0, int.MaxValue);

                        // Create a new car object and add it to the list
                        Car newCar = new Car();
                        newCar.Make = carMake;
                        newCar.Model = carModel;
                        newCar.Color = carColor;
                        newCar.Miles = carMiles;
                        newCar.Price = carPrice;
                        CarStore.CarList.Add(newCar);
                        printStoreInventory(CarStore);
                        break;

                    case 2:
                        // You chose buy a car

                        // Verify there is inventory to purchase
                        if (CarStore.CarList.Count == 0)
                        {
                            Console.Out.WriteLine("No Cars to choose - please add inventory.");
                            break;
                        }

                        printStoreInventory(CarStore);

                        // Ask for a car number to purchase - ensure cannot exceed list count
                        int choice = 0;
                        choice = getValidNumericEntry("Which car woud you like to add to the cart? (number): ", 0, CarStore.CarList.Count-1);

                        // Add the car tot the shopping cart
                        CarStore.ShoppingList.Add(CarStore.CarList[choice]);

                        printShoppingCart(CarStore);
                        break;

                    case 3:
                        // Checkout
                        printShoppingCart(CarStore);
                        Console.Out.WriteLine("Your total cost is ${0}", CarStore.checkout());
                        break;

                    default:
                        break;
                }
                action = getValidNumericEntry("Choose an action: (0) quit (1) add a car (2) add item to cart (3) checkout: ", 0, 3);
            }
        }

        // Validate numeric (int) entry: present appropriate warning error messages if outside of range/invalid entry
        // Replaces chooseAction method
        static public int getValidNumericEntry(string msg, int low, int high)
        {
            int answer = low - 1;

            // Ensure user chooses a valid option
            while (answer < low || answer > high)
            {
                Console.Out.Write(msg);
                try
                {
                    answer = int.Parse(Console.ReadLine());
                    if (answer < low || answer > high)
                    {
                        Console.Out.WriteLine("Warning: Valid entry must be between {0} and {1}.", low, high);
                    }
                }
                catch (FormatException)
                {
                    Console.Out.WriteLine("Error: Entry must be an integer between {0} and {1}.", low, high);
                }
            }
                return answer;
        }
        static public void printStoreInventory(Store carStore)
        {
            Console.Out.WriteLine("These are the cars in the store inventory:");
            int i = 0;
            foreach (var c in carStore.CarList)
            {
                Console.Out.WriteLine(String.Format("Car # = {0} {1}", i, c.Display));
                i++;
            }
            return;
        }

        static public void printShoppingCart(Store carStore)
        {
            Console.Out.WriteLine("These are the cars in the shopping cart:");
            int i = 0;
            foreach (var c in carStore.CarList)
            {
                Console.Out.WriteLine(String.Format("Car # = {0} {1}", i, c.Display));
                i++;
            }
            return;
        }
    }
}
