﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-08-10
 * Class     : CST-227 Enterprise Computer Promgramming II
 * Professor : James Shinevar
 * Assignment: Activity 1
 * ---------------------------------------------------------------
 * Description:
 * 1. Create CarClassLibrary: Car, Store Classes
 * 2. Create Console Application to use the CarClassLibrary
 * 3. Create GUI Application to use the CarClassLibrary
 * Objective: Create applications that both utilize the same
 * underlying classes (library) to simulate a car store and 
 * shopping experience.
 * ---------------------------------------------------------------
 */
 
 namespace CarShopGUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gb_CreateACar = new System.Windows.Forms.GroupBox();
            this.btn_CreateCar = new System.Windows.Forms.Button();
            this.tb_CarPrice = new System.Windows.Forms.TextBox();
            this.tb_CarMiles = new System.Windows.Forms.TextBox();
            this.tb_CarColor = new System.Windows.Forms.TextBox();
            this.tb_CarModel = new System.Windows.Forms.TextBox();
            this.tb_CarMake = new System.Windows.Forms.TextBox();
            this.lbl_CarPrice = new System.Windows.Forms.Label();
            this.lbl_CarMiles = new System.Windows.Forms.Label();
            this.lbl_CarColor = new System.Windows.Forms.Label();
            this.lbl_CarModel = new System.Windows.Forms.Label();
            this.lbl_CarMake = new System.Windows.Forms.Label();
            this.gb_CarInventory = new System.Windows.Forms.GroupBox();
            this.lb_CarInventory = new System.Windows.Forms.ListBox();
            this.gb_ShoppingCart = new System.Windows.Forms.GroupBox();
            this.lb_ShoppingCart = new System.Windows.Forms.ListBox();
            this.btn_AddToCart = new System.Windows.Forms.Button();
            this.btn_Checkout = new System.Windows.Forms.Button();
            this.lbl_TotalCost = new System.Windows.Forms.Label();
            this.lbl_TotalPrice = new System.Windows.Forms.Label();
            this.gb_CreateACar.SuspendLayout();
            this.gb_CarInventory.SuspendLayout();
            this.gb_ShoppingCart.SuspendLayout();
            this.SuspendLayout();
            // 
            // gb_CreateACar
            // 
            this.gb_CreateACar.Controls.Add(this.btn_CreateCar);
            this.gb_CreateACar.Controls.Add(this.tb_CarPrice);
            this.gb_CreateACar.Controls.Add(this.tb_CarMiles);
            this.gb_CreateACar.Controls.Add(this.tb_CarColor);
            this.gb_CreateACar.Controls.Add(this.tb_CarModel);
            this.gb_CreateACar.Controls.Add(this.tb_CarMake);
            this.gb_CreateACar.Controls.Add(this.lbl_CarPrice);
            this.gb_CreateACar.Controls.Add(this.lbl_CarMiles);
            this.gb_CreateACar.Controls.Add(this.lbl_CarColor);
            this.gb_CreateACar.Controls.Add(this.lbl_CarModel);
            this.gb_CreateACar.Controls.Add(this.lbl_CarMake);
            this.gb_CreateACar.Location = new System.Drawing.Point(22, 23);
            this.gb_CreateACar.Name = "gb_CreateACar";
            this.gb_CreateACar.Size = new System.Drawing.Size(270, 268);
            this.gb_CreateACar.TabIndex = 0;
            this.gb_CreateACar.TabStop = false;
            this.gb_CreateACar.Text = "Create a Car:";
            // 
            // btn_CreateCar
            // 
            this.btn_CreateCar.Location = new System.Drawing.Point(99, 219);
            this.btn_CreateCar.Name = "btn_CreateCar";
            this.btn_CreateCar.Size = new System.Drawing.Size(75, 23);
            this.btn_CreateCar.TabIndex = 6;
            this.btn_CreateCar.Text = "Create Car";
            this.btn_CreateCar.UseVisualStyleBackColor = true;
            this.btn_CreateCar.Click += new System.EventHandler(this.btn_CreateCar_Click);
            // 
            // tb_CarPrice
            // 
            this.tb_CarPrice.Location = new System.Drawing.Point(59, 179);
            this.tb_CarPrice.Name = "tb_CarPrice";
            this.tb_CarPrice.Size = new System.Drawing.Size(186, 20);
            this.tb_CarPrice.TabIndex = 5;
            // 
            // tb_CarMiles
            // 
            this.tb_CarMiles.Location = new System.Drawing.Point(59, 142);
            this.tb_CarMiles.Name = "tb_CarMiles";
            this.tb_CarMiles.Size = new System.Drawing.Size(186, 20);
            this.tb_CarMiles.TabIndex = 4;
            // 
            // tb_CarColor
            // 
            this.tb_CarColor.Location = new System.Drawing.Point(59, 106);
            this.tb_CarColor.Name = "tb_CarColor";
            this.tb_CarColor.Size = new System.Drawing.Size(186, 20);
            this.tb_CarColor.TabIndex = 3;
            // 
            // tb_CarModel
            // 
            this.tb_CarModel.Location = new System.Drawing.Point(59, 69);
            this.tb_CarModel.Name = "tb_CarModel";
            this.tb_CarModel.Size = new System.Drawing.Size(186, 20);
            this.tb_CarModel.TabIndex = 2;
            // 
            // tb_CarMake
            // 
            this.tb_CarMake.Location = new System.Drawing.Point(59, 34);
            this.tb_CarMake.Name = "tb_CarMake";
            this.tb_CarMake.Size = new System.Drawing.Size(186, 20);
            this.tb_CarMake.TabIndex = 1;
            // 
            // lbl_CarPrice
            // 
            this.lbl_CarPrice.AutoSize = true;
            this.lbl_CarPrice.Location = new System.Drawing.Point(19, 179);
            this.lbl_CarPrice.Name = "lbl_CarPrice";
            this.lbl_CarPrice.Size = new System.Drawing.Size(34, 13);
            this.lbl_CarPrice.TabIndex = 0;
            this.lbl_CarPrice.Text = "Price:";
            // 
            // lbl_CarMiles
            // 
            this.lbl_CarMiles.AutoSize = true;
            this.lbl_CarMiles.Location = new System.Drawing.Point(19, 142);
            this.lbl_CarMiles.Name = "lbl_CarMiles";
            this.lbl_CarMiles.Size = new System.Drawing.Size(34, 13);
            this.lbl_CarMiles.TabIndex = 0;
            this.lbl_CarMiles.Text = "Miles:";
            // 
            // lbl_CarColor
            // 
            this.lbl_CarColor.AutoSize = true;
            this.lbl_CarColor.Location = new System.Drawing.Point(19, 106);
            this.lbl_CarColor.Name = "lbl_CarColor";
            this.lbl_CarColor.Size = new System.Drawing.Size(34, 13);
            this.lbl_CarColor.TabIndex = 0;
            this.lbl_CarColor.Text = "Color:";
            // 
            // lbl_CarModel
            // 
            this.lbl_CarModel.AutoSize = true;
            this.lbl_CarModel.Location = new System.Drawing.Point(19, 69);
            this.lbl_CarModel.Name = "lbl_CarModel";
            this.lbl_CarModel.Size = new System.Drawing.Size(39, 13);
            this.lbl_CarModel.TabIndex = 0;
            this.lbl_CarModel.Text = "Model:";
            // 
            // lbl_CarMake
            // 
            this.lbl_CarMake.AutoSize = true;
            this.lbl_CarMake.Location = new System.Drawing.Point(19, 34);
            this.lbl_CarMake.Name = "lbl_CarMake";
            this.lbl_CarMake.Size = new System.Drawing.Size(37, 13);
            this.lbl_CarMake.TabIndex = 0;
            this.lbl_CarMake.Text = "Make:";
            // 
            // gb_CarInventory
            // 
            this.gb_CarInventory.Controls.Add(this.lb_CarInventory);
            this.gb_CarInventory.Location = new System.Drawing.Point(321, 23);
            this.gb_CarInventory.Name = "gb_CarInventory";
            this.gb_CarInventory.Size = new System.Drawing.Size(311, 268);
            this.gb_CarInventory.TabIndex = 1;
            this.gb_CarInventory.TabStop = false;
            this.gb_CarInventory.Text = "Car Inventory:";
            // 
            // lb_CarInventory
            // 
            this.lb_CarInventory.FormattingEnabled = true;
            this.lb_CarInventory.Location = new System.Drawing.Point(22, 30);
            this.lb_CarInventory.Name = "lb_CarInventory";
            this.lb_CarInventory.Size = new System.Drawing.Size(266, 212);
            this.lb_CarInventory.TabIndex = 7;
            // 
            // gb_ShoppingCart
            // 
            this.gb_ShoppingCart.Controls.Add(this.lb_ShoppingCart);
            this.gb_ShoppingCart.Location = new System.Drawing.Point(757, 23);
            this.gb_ShoppingCart.Name = "gb_ShoppingCart";
            this.gb_ShoppingCart.Size = new System.Drawing.Size(311, 268);
            this.gb_ShoppingCart.TabIndex = 2;
            this.gb_ShoppingCart.TabStop = false;
            this.gb_ShoppingCart.Text = "Shopping Cart:";
            // 
            // lb_ShoppingCart
            // 
            this.lb_ShoppingCart.FormattingEnabled = true;
            this.lb_ShoppingCart.Location = new System.Drawing.Point(22, 30);
            this.lb_ShoppingCart.Name = "lb_ShoppingCart";
            this.lb_ShoppingCart.Size = new System.Drawing.Size(266, 212);
            this.lb_ShoppingCart.TabIndex = 0;
            this.lb_ShoppingCart.TabStop = false;
            // 
            // btn_AddToCart
            // 
            this.btn_AddToCart.Location = new System.Drawing.Point(653, 127);
            this.btn_AddToCart.Name = "btn_AddToCart";
            this.btn_AddToCart.Size = new System.Drawing.Size(83, 23);
            this.btn_AddToCart.TabIndex = 8;
            this.btn_AddToCart.Text = "Add to Cart ->";
            this.btn_AddToCart.UseVisualStyleBackColor = true;
            this.btn_AddToCart.Click += new System.EventHandler(this.btn_AddToCart_Click);
            // 
            // btn_Checkout
            // 
            this.btn_Checkout.Location = new System.Drawing.Point(873, 312);
            this.btn_Checkout.Name = "btn_Checkout";
            this.btn_Checkout.Size = new System.Drawing.Size(75, 23);
            this.btn_Checkout.TabIndex = 9;
            this.btn_Checkout.Text = "Checkout";
            this.btn_Checkout.UseVisualStyleBackColor = true;
            this.btn_Checkout.Click += new System.EventHandler(this.btn_Checkout_Click);
            // 
            // lbl_TotalCost
            // 
            this.lbl_TotalCost.AutoSize = true;
            this.lbl_TotalCost.Location = new System.Drawing.Point(754, 366);
            this.lbl_TotalCost.Name = "lbl_TotalCost";
            this.lbl_TotalCost.Size = new System.Drawing.Size(58, 13);
            this.lbl_TotalCost.TabIndex = 0;
            this.lbl_TotalCost.Text = "Total Cost:";
            // 
            // lbl_TotalPrice
            // 
            this.lbl_TotalPrice.AutoSize = true;
            this.lbl_TotalPrice.Location = new System.Drawing.Point(870, 366);
            this.lbl_TotalPrice.Name = "lbl_TotalPrice";
            this.lbl_TotalPrice.Size = new System.Drawing.Size(13, 13);
            this.lbl_TotalPrice.TabIndex = 0;
            this.lbl_TotalPrice.Text = "0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1091, 411);
            this.Controls.Add(this.lbl_TotalPrice);
            this.Controls.Add(this.lbl_TotalCost);
            this.Controls.Add(this.btn_Checkout);
            this.Controls.Add(this.btn_AddToCart);
            this.Controls.Add(this.gb_ShoppingCart);
            this.Controls.Add(this.gb_CarInventory);
            this.Controls.Add(this.gb_CreateACar);
            this.Name = "Form1";
            this.Text = "Car Shop GUI";
            this.gb_CreateACar.ResumeLayout(false);
            this.gb_CreateACar.PerformLayout();
            this.gb_CarInventory.ResumeLayout(false);
            this.gb_ShoppingCart.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gb_CreateACar;
        private System.Windows.Forms.Label lbl_CarMake;
        private System.Windows.Forms.Label lbl_CarMiles;
        private System.Windows.Forms.Label lbl_CarColor;
        private System.Windows.Forms.Label lbl_CarModel;
        private System.Windows.Forms.Button btn_CreateCar;
        private System.Windows.Forms.TextBox tb_CarPrice;
        private System.Windows.Forms.TextBox tb_CarMiles;
        private System.Windows.Forms.TextBox tb_CarColor;
        private System.Windows.Forms.TextBox tb_CarModel;
        private System.Windows.Forms.TextBox tb_CarMake;
        private System.Windows.Forms.Label lbl_CarPrice;
        private System.Windows.Forms.GroupBox gb_CarInventory;
        private System.Windows.Forms.ListBox lb_CarInventory;
        private System.Windows.Forms.GroupBox gb_ShoppingCart;
        private System.Windows.Forms.ListBox lb_ShoppingCart;
        private System.Windows.Forms.Button btn_AddToCart;
        private System.Windows.Forms.Button btn_Checkout;
        private System.Windows.Forms.Label lbl_TotalCost;
        private System.Windows.Forms.Label lbl_TotalPrice;
    }
}

