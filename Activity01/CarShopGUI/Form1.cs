﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-08-10
 * Class     : CST-227 Enterprise Computer Promgramming II
 * Professor : James Shinevar
 * Assignment: Activity 1
 * ---------------------------------------------------------------
 * Description:
 * 1. Create CarClassLibrary: Car, Store Classes
 * 2. Create Console Application to use the CarClassLibrary
 * 3. Create GUI Application to use the CarClassLibrary
 * Objective: Create applications that both utilize the same
 * underlying classes (library) to simulate a car store and 
 * shopping experience.
 * ---------------------------------------------------------------
 */

using CarClassLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarShopGUI
{
    public partial class Form1 : Form
    {
        Store store = new Store();

        BindingSource carListBinding = new BindingSource();
        BindingSource shoppingListBinding = new BindingSource();

        public Form1()
        {
            InitializeComponent();
            SetBindings();
        }

        private void SetBindings()
        {
            carListBinding.DataSource = store.CarList;
            lb_CarInventory.DataSource = carListBinding;
            lb_CarInventory.DisplayMember = "Display";
            //lb_CarInventory.ValueMember = "Display";

            shoppingListBinding.DataSource = store.ShoppingList;
            lb_ShoppingCart.DataSource = shoppingListBinding;
            lb_ShoppingCart.DisplayMember = "Display";
            //lb_ShoppingCart.ValueMember = "Display";
        }

        private void btn_CreateCar_Click(object sender, EventArgs e)
        {
            bool valid = true;
            int tmp_miles = 0;
            decimal tmp_price = 0;

            // Validate entry for miles
            try
            {
                tmp_miles = int.Parse(tb_CarMiles.Text);
                if (tmp_miles < 0)
                {
                    MessageBox.Show("Invalid entry for miles (positive numbers only)", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    valid = false;
                }
            }
            catch(FormatException)
            {
                // Invalid entry for miles - parsing failed
                MessageBox.Show("Invalid entry for miles (positive numbers only)", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                valid = false;
            }

            // Validate entry for price
            try
            {
                tmp_price = Decimal.Parse(tb_CarPrice.Text);
                if (tmp_price < 0)
                {
                    MessageBox.Show("Invalid entry for price (positive numbers only)", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    valid = false;
                }
            }
            catch (FormatException)
            {
                // Invalid entry for price - parsing failed
                MessageBox.Show("Invalid entry for price (positive numbers only)", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                valid = false;
            }

            // Create and add Car if data is valid
            if (valid)
            {
                Car newCar = new Car();
                newCar.Make = tb_CarMake.Text;
                newCar.Model = tb_CarModel.Text;
                newCar.Color = tb_CarColor.Text;
                newCar.Miles = tmp_miles;
                newCar.Price = tmp_price;
                store.CarList.Add(newCar);
                carListBinding.ResetBindings(false);
            }
        }

        private void btn_AddToCart_Click(object sender, EventArgs e)
        {
            store.ShoppingList.Add((Car)lb_CarInventory.SelectedItem);
            shoppingListBinding.ResetBindings(false);
        }

        private void btn_Checkout_Click(object sender, EventArgs e)
        {
            decimal total = store.checkout();
            lbl_TotalPrice.Text = total.ToString();
            //shoppingListBinding.ResetBindings(false); // Should call since checkout() clears list
        }
    }
}
