﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-08-10
 * Class     : CST-227 Enterprise Computer Promgramming II
 * Professor : James Shinevar
 * Assignment: Activity 1
 * ---------------------------------------------------------------
 * Description:
 * 1. Create CarClassLibrary: Car, Store Classes
 * 2. Create Console Application to use the CarClassLibrary
 * 3. Create GUI Application to use the CarClassLibrary
 * Objective: Create applications that both utilize the same
 * underlying classes (library) to simulate a car store and 
 * shopping experience.
 * ---------------------------------------------------------------
 */


using CarClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarShopGUI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
