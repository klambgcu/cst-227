﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-08-17
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Milestone 2
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * Create classes to represent Cells and Game Board for the 
 * Mine Sweeper game. (Simple Interactive Version)
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Milestone02
{
    class Program
    {
        // Define states for game loop logic
        enum GameState
        {
            GamePlaying,
            GameOverWin,
            GameOverLose
        }

        static void Main(string[] args)
        {
            //
            // Prompt Size and Difficulty
            //
            int size = getValidNumericEntry("Enter size of board(5 - 20): ", 5, 20);
            int difficulty = getValidNumericEntry("Enter game difficulty percentage (1-100): ", 1, 100);

            //
            // Define game board and setup mines based upon size/difficulty
            //
            Board board = new Board(size);
            board.Difficulty = difficulty;
            board.setupLiveNeighbors();
            board.calculateLiveNeighbors();

            //
            // Initialize game status
            //
            GameState GameStatus = GameState.GamePlaying;

            // -----------------------------------------
            // Main Game Loop (Start)
            // -----------------------------------------
            while (GameStatus == GameState.GamePlaying)
            {
                // Display game board
                PrintBoardDuringGame(board);

                // Request choice from user
                int row = getValidNumericEntry("Enter a Row Number: ", 0, board.Size - 1);
                int col = getValidNumericEntry("Enter a Column Number: ", 0, board.Size - 1);

                // Set cell chosen/visited
                // Note: Should I check and inform if cell already visited?
                board.setCellVisited(row, col);

                // Check game completed - Bomb Chosen or Board Cleared
                if (board.grid[row,col].Live)
                {
                    GameStatus = GameState.GameOverLose;
                }
                else if (board.boardCompleted())
                {
                    GameStatus = GameState.GameOverWin;
                }
            } 
            // -----------------------------------------
            // Main Game Loop (End)
            // -----------------------------------------

            //
            // Game Over - display whole board and messages
            //
            printBoard(board);

            //
            // Display Game Over Result Message
            //
            Console.Out.WriteLine();

            if (GameStatus == GameState.GameOverWin)
            {
                Console.Out.WriteLine("Congratulations! You WIN!!!");
            }
            else
            {
                Console.Out.WriteLine("Sorry! You Hit a Bomb! (Please Try Again)");
            }
            Console.Out.WriteLine();
            Console.Out.Write("\nPress Enter to Exit.");
            Console.ReadLine();
        }

        // Helper method to display game board
        static public void printBoard(Board board)
        {
            // Clear the screen for consistent display
            Console.Clear();
            Console.Out.WriteLine();

            // Print Column Header
            Console.Out.Write("+");
            for (int col = 0; col < board.Size; col++)
                Console.Out.Write(" {0,-2}+", col);
            Console.Out.WriteLine();

            // Print Top Separation Line
            Console.Out.Write("+");
            for (int col = 0; col < board.Size; col++)
                Console.Out.Write("---+");
            Console.Out.WriteLine();

            // Double Loop to print board contents
            for (int row = 0; row < board.Size; row++)
            {
                Console.Out.Write("|");
                for (int col = 0; col < board.Size; col++)
                {
                    if (board.grid[row, col].Live)
                        Console.Out.Write(" * |"); // Live bombs display an asterisk
                    else
                        Console.Out.Write(" {0} |", board.grid[row, col].Neighbors);
                }

                // Print Row Header
                Console.Out.WriteLine(" {0}", row);

                // Print Body Separation Lines
                Console.Out.Write("+");
                for (int col = 0; col < board.Size; col++)
                    Console.Out.Write("---+");
                Console.Out.WriteLine();
            }

            Console.Out.WriteLine();
        }

        // Helper method to display game board
        static public void PrintBoardDuringGame(Board board)
        {
            // Clear the screen for consistent display
            Console.Clear();
            Console.Out.WriteLine();

            // Print Column Header
            Console.Out.Write("+");
            for (int col = 0; col < board.Size; col++)
                Console.Out.Write(" {0,-2}+", col);
            Console.Out.WriteLine();

            // Print Top Separation Line
            Console.Out.Write("+");
            for (int col = 0; col < board.Size; col++)
                Console.Out.Write("---+");
            Console.Out.WriteLine();

            // Double Loop to print board contents
            for (int row = 0; row < board.Size; row++)
            {
                Console.Out.Write("|");
                for (int col = 0; col < board.Size; col++)
                {
                    if (board.grid[row, col].Visited)
                    {
                        if (board.grid[row, col].Live)
                        {
                            Console.Out.Write(" * |"); // Live bombs display an asterisk
                        }
                        else if (board.grid[row, col].Neighbors == 0)
                        {
                            Console.Out.Write("   |"); // Empty cell - display a blank
                        }
                        else
                        {
                            Console.Out.Write(" {0} |", board.grid[row, col].Neighbors);
                        }
                    }
                    else
                    {
                        Console.Out.Write(" ? |"); // Not visited - display question mark
                    }
                }

                // Print Row Header
                Console.Out.WriteLine(" {0}", row);

                // Print Body Separation Lines
                Console.Out.Write("+");
                for (int col = 0; col < board.Size; col++)
                    Console.Out.Write("---+");
                Console.Out.WriteLine();
            }

            Console.Out.WriteLine();
        }

        // Validate numeric (int) entry: present appropriate warning error messages if outside of range/invalid entry
        // Replaces chooseAction method
        static public int getValidNumericEntry(string msg, int low, int high)
        {
            int answer = low - 1;

            // Ensure user chooses a valid option
            while (answer < low || answer > high)
            {
                Console.Out.Write(msg);
                try
                {
                    answer = int.Parse(Console.ReadLine());
                    if (answer < low || answer > high)
                    {
                        Console.Out.WriteLine("Warning: Valid entry must be between {0} and {1}.", low, high);
                    }
                }
                catch (FormatException)
                {
                    Console.Out.WriteLine("Error: Entry must be an integer between {0} and {1}.", low, high);
                }
            }
            return answer;
        }
    }
}
