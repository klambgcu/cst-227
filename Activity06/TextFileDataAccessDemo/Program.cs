﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-14
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Activity 6
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * FIle IO: Read and Write contents to a text file.
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace TextFileDataAccessDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            string filePath = @".\DataFile.txt";

            List<Person> people = new List<Person>();
            List<String> lines = File.ReadAllLines(filePath).ToList();
            List<String> outputLines = new List<String>();

            foreach (string line in lines)
            {
                string[] entries = line.Split(',');

                // Error checking
                if (entries.Count() == 3)
                {
                    Person p = new Person();
                    p.firstName = entries[0].Trim(); // Remove blanks from each end
                    p.lastName = entries[1].Trim();
                    p.Url = entries[2].Trim();
                    people.Add(p); // Add to the list of people
                }
                else
                {
                    // Notify user of formatting error
                    Console.WriteLine("Format Error: Incorrect number of fields (commas): " + line);
                }
            }

            // Print a list of people
            Console.WriteLine("Here is a list of people I have:");

            foreach(Person p in people)
            {
                // First print to the console
                Console.WriteLine("First Name: " + p.firstName + " Last Name: " + p.lastName + " URL: " + p.Url);

                outputLines.Add("First Name: " + p.firstName + " Last Name: " + p.lastName + " URL: " + p.Url);
            }

            // Write to another file
            string outPath = @".\peopleOut.txt";
            File.WriteAllLines(outPath, outputLines);

            // ---------------------------------------------------------
            // Code from the first part of section 1.
            // ---------------------------------------------------------
            //List<String> lines = File.ReadAllLines(filePath).ToList();
            //foreach(string line in lines)
            //    Console.WriteLine(line);
            //lines.Add("Steve, Jobs, www.apple.com");
            //File.WriteAllLines(filePath, lines);

            Console.ReadLine();
        }
    }
}
