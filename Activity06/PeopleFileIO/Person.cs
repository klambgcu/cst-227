/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-14
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Activity 6
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * FIle IO: Read and Write contents to a text file.
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeopleFileIO
{
    public class Person
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string Url { get; set; }

        public override string ToString()
        {
            return firstName + " " + lastName + " " + Url;
        }

    }
}
