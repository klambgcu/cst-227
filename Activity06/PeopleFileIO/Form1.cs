﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-14
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Activity 6
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * FIle IO: Read and Write contents to a text file.
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PeopleFileIO
{
    public partial class Form1 : Form
    {
        TextFileAccess fileManager;
        BindingSource peopleListBinding = new BindingSource();
        List<Person> peopleList = new List<Person>();

        public Form1()
        {
            InitializeComponent();
            fileManager = new TextFileAccess(@".\PeopleList.txt");
            SetBindings();
        }

        // Set List bindings for People Listbox
        private void SetBindings()
        {
            peopleListBinding.DataSource = peopleList;
            lb_PeopleList.DataSource = peopleListBinding;
            lb_PeopleList.DisplayMember = "ToString";
        }

        // Load data from file
        private void btn_LoadFromFile_Click(object sender, EventArgs e)
        {
            bool results = fileManager.LoadPeopleFile();

            if (results == false)
            {
                string msgs = "";
                foreach (string err in fileManager.GetErrorMessages())
                {
                    msgs += err + Environment.NewLine;
                }
                MessageBox.Show(msgs, "Data File Formatting Errors", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            // Append file to existing list
            foreach (Person p in fileManager.GetList())
                peopleList.Add(p);

            peopleListBinding.ResetBindings(false);
        }

        // Save list
        private void btn_SaveToFile_Click(object sender, EventArgs e)
        {
            fileManager.SavePeopleFile(peopleList);
        }

        // Add entry to list
        private void btn_AddToList_Click(object sender, EventArgs e)
        {
            string first = tb_FirstName.Text;
            string last = tb_LastName.Text;
            string url = tb_URL.Text;
            if (first.Contains(",") || last.Contains(",") || url.Contains(","))
            {
                MessageBox.Show("Commas are not allowed. Please correct.",
                                "Data Format Error",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            else
            {
                // Create and add person object
                Person person = new Person();
                person.firstName = first;
                person.lastName = last;
                person.Url = url;
                peopleList.Add(person);

                // Update listbox
                peopleListBinding.ResetBindings(false);

                // Clear entry fields
                tb_FirstName.Text = "";
                tb_LastName.Text = "";
                tb_URL.Text = "";
            }
        }
    }
}
