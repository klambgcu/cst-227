﻿namespace PeopleFileIO
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_FirstName = new System.Windows.Forms.TextBox();
            this.tb_LastName = new System.Windows.Forms.TextBox();
            this.tb_URL = new System.Windows.Forms.TextBox();
            this.btn_AddToList = new System.Windows.Forms.Button();
            this.btn_SaveToFile = new System.Windows.Forms.Button();
            this.btn_LoadFromFile = new System.Windows.Forms.Button();
            this.lb_PeopleList = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "First Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Last Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(47, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "URL:";
            // 
            // tb_FirstName
            // 
            this.tb_FirstName.Location = new System.Drawing.Point(88, 26);
            this.tb_FirstName.MaxLength = 20;
            this.tb_FirstName.Name = "tb_FirstName";
            this.tb_FirstName.Size = new System.Drawing.Size(156, 20);
            this.tb_FirstName.TabIndex = 1;
            // 
            // tb_LastName
            // 
            this.tb_LastName.Location = new System.Drawing.Point(88, 60);
            this.tb_LastName.MaxLength = 20;
            this.tb_LastName.Name = "tb_LastName";
            this.tb_LastName.Size = new System.Drawing.Size(156, 20);
            this.tb_LastName.TabIndex = 2;
            // 
            // tb_URL
            // 
            this.tb_URL.Location = new System.Drawing.Point(88, 94);
            this.tb_URL.MaxLength = 50;
            this.tb_URL.Name = "tb_URL";
            this.tb_URL.Size = new System.Drawing.Size(156, 20);
            this.tb_URL.TabIndex = 3;
            // 
            // btn_AddToList
            // 
            this.btn_AddToList.Location = new System.Drawing.Point(22, 136);
            this.btn_AddToList.Name = "btn_AddToList";
            this.btn_AddToList.Size = new System.Drawing.Size(222, 23);
            this.btn_AddToList.TabIndex = 4;
            this.btn_AddToList.Text = "Add to List ->";
            this.btn_AddToList.UseVisualStyleBackColor = true;
            this.btn_AddToList.Click += new System.EventHandler(this.btn_AddToList_Click);
            // 
            // btn_SaveToFile
            // 
            this.btn_SaveToFile.Location = new System.Drawing.Point(88, 199);
            this.btn_SaveToFile.Name = "btn_SaveToFile";
            this.btn_SaveToFile.Size = new System.Drawing.Size(107, 23);
            this.btn_SaveToFile.TabIndex = 5;
            this.btn_SaveToFile.Text = "Save to File";
            this.btn_SaveToFile.UseVisualStyleBackColor = true;
            this.btn_SaveToFile.Click += new System.EventHandler(this.btn_SaveToFile_Click);
            // 
            // btn_LoadFromFile
            // 
            this.btn_LoadFromFile.Location = new System.Drawing.Point(88, 241);
            this.btn_LoadFromFile.Name = "btn_LoadFromFile";
            this.btn_LoadFromFile.Size = new System.Drawing.Size(107, 23);
            this.btn_LoadFromFile.TabIndex = 6;
            this.btn_LoadFromFile.Text = "Load from File";
            this.btn_LoadFromFile.UseVisualStyleBackColor = true;
            this.btn_LoadFromFile.Click += new System.EventHandler(this.btn_LoadFromFile_Click);
            // 
            // lb_PeopleList
            // 
            this.lb_PeopleList.FormattingEnabled = true;
            this.lb_PeopleList.Location = new System.Drawing.Point(268, 26);
            this.lb_PeopleList.Name = "lb_PeopleList";
            this.lb_PeopleList.Size = new System.Drawing.Size(517, 238);
            this.lb_PeopleList.TabIndex = 7;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(809, 285);
            this.Controls.Add(this.lb_PeopleList);
            this.Controls.Add(this.btn_LoadFromFile);
            this.Controls.Add(this.btn_SaveToFile);
            this.Controls.Add(this.btn_AddToList);
            this.Controls.Add(this.tb_URL);
            this.Controls.Add(this.tb_LastName);
            this.Controls.Add(this.tb_FirstName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "People I Know";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb_FirstName;
        private System.Windows.Forms.TextBox tb_LastName;
        private System.Windows.Forms.TextBox tb_URL;
        private System.Windows.Forms.Button btn_AddToList;
        private System.Windows.Forms.Button btn_SaveToFile;
        private System.Windows.Forms.Button btn_LoadFromFile;
        private System.Windows.Forms.ListBox lb_PeopleList;
    }
}

