﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-14
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Activity 6
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * FIle IO: Read and Write contents to a text file.
 * ---------------------------------------------------------------
 */
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace PeopleFileIO
{
    public class TextFileAccess
    {
        private string filePath;
        private List<Person> list = new List<Person>();
        private List<String> errorMessages = new List<String>();

        public TextFileAccess(string filePath)
        {
            this.filePath = filePath;
        }

        public List<Person> GetList()
        {
            return list;
        }

        public List<String> GetErrorMessages()
        {
            return errorMessages;
        }

        // Load data file
        // Returns: true = no errors, false errors
        // Call GetList to get list of people regardless of results
        // Call GetErrorMessages in event of false results
        public bool LoadPeopleFile()
        {
            list.Clear();
            errorMessages.Clear();

            bool results = true;

            List<String> lines = File.ReadAllLines(filePath).ToList();

            foreach (string line in lines)
            {
                string[] entries = line.Split(',');

                // Error checking
                if (entries.Count() == 3)
                {
                    Person p = new Person();
                    // Remove blanks from each end
                    p.firstName = entries[0].Trim(); 
                    p.lastName = entries[1].Trim();
                    p.Url = entries[2].Trim();

                    // Add to the list of people
                    list.Add(p); 
                }
                else
                {
                    // Notify user of formatting error
                    errorMessages.Add("Format Error: " + line);
                    results = false;
                }
            }
            return results;
        }

        public void SavePeopleFile(List<Person> peopleList)
        {
            // Convert people list to a comma delimited list of strings
            List<String> people = new List<String>();
            foreach (Person p in peopleList)
            {
                string person = p.firstName.Trim() + "," + p.lastName.Trim() + "," + p.Url.Trim();
                people.Add(person);
            }

            // Write list of string out to file
            File.WriteAllLines(filePath, people);
        }
    }
}
