﻿/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-14
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Activity 6
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * LINQ and List exercises
 * ---------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQdemo
{
    class Program
    {
        static void Main(string[] args)
        {
            // Make some test scores
            int[] scores = new[] { 50, 66, 90, 81, 77, 45, 0, 100, 99, 72, 87, 85, 81, 80, 77, 74, 95, 97 };            // ------------------------------------------------------

            // ------------------------------------------------------
            // Figure 2-1a.
            // ------------------------------------------------------
            // Print the scores 
            foreach (var individualScore in scores )
            {
                Console.WriteLine("One of the scores was {0}", individualScore);
            }

            // Pauses to see the output before closing
            Console.ReadLine();

            // ------------------------------------------------------
            // Figure 2-1b.
            // ------------------------------------------------------
            // Use a LINQ statement to filter the list.
            var theBestStudents =
                from individualScore in scores
                where individualScore > 90
                select individualScore;

            // Print only the best scores
            foreach( var individualScore in theBestStudents)
            {
                Console.WriteLine("One of the BEST scores was {0}", individualScore);
            }

            // Pauses to see the output before closing
            Console.ReadLine();

            // ------------------------------------------------------
            // Figure 2-1c.
            // ------------------------------------------------------
            // Use LINQ to sort the results
            var sortedScores =
                from individualScore in scores
                orderby individualScore
                select individualScore;

            // Print the sorted list
            foreach(var individualScore in sortedScores)
            {
                Console.WriteLine("One of the scores was {0}", individualScore);
            }

            // Pauses to see the output before closing
            Console.ReadLine();

            // ------------------------------------------------------
            // Figure 2-1d. (Challenge 1)
            // ------------------------------------------------------
            // Print list of B students (80-89) ascending order using LINQ
            var sortedBScores =
                from individualScore in scores
                where individualScore >= 80 && individualScore < 90
                orderby individualScore
                select individualScore;

            // Print the sorted list
            foreach (var individualScore in sortedBScores)
            {
                Console.WriteLine("One of the B scores was {0}", individualScore);
            }

            // Pauses to see the output before closing
            Console.ReadLine();

            // ------------------------------------------------------
            // Figure 2-1e. (Challenge 2)
            // ------------------------------------------------------
            // Print list of Student Class (name, age, grade)
            // Use IComparable/CompareTo to sort list
            List<Student> students = GetListOfStudents();

            // Sort list of students
            students.Sort();

            // Print the sorted list
            Console.WriteLine("Student List Sorted in Ascending Order by Grade, Name, Age");
            foreach (Student student in students)
            {
                Console.WriteLine("Sorted Students: " + student.ToString());
            }

            // Pauses to see the output before closing
            Console.ReadLine();

            // ------------------------------------------------------
            // Figure 2-1f. (Challenge 2)
            // ------------------------------------------------------
            // Print list of Student Class (name, age, grade)
            // Use IComparable/CompareTo to sort list
            List<Student> students2 = GetListOfStudents();

            // Sort list of students
            var sortedStudents = students2.OrderBy(a => a.Grade).ThenBy(a => a.Name).ThenBy(a => a.Age);

            // Print the sorted list
            Console.WriteLine("Student List Sorted (LINQ)in Ascending Order by Grade, Name, Age");
            foreach (Student student in sortedStudents)
            {
                Console.WriteLine("Sorted Students: " + student.ToString());
            }

            // Pauses to see the output before closing
            Console.ReadLine();


        }

        private static List<Student> GetListOfStudents()
        {
            List<Student> students = new List<Student>();
            students.Add(new Student("Kelly Lamb",       55, 100)); // Me :)
            students.Add(new Student("Josiah Elliott",   20, 100)); // Random list
            students.Add(new Student("Hung Fletcher",    21, 90));
            students.Add(new Student("Roderick Padilla", 30, 70));
            students.Add(new Student("Paul Kelley",      40, 60));
            students.Add(new Student("Pasquale Phelps",  55, 90));
            students.Add(new Student("Tammi Harvey",     25, 100));
            students.Add(new Student("Liza Singh",       19, 95));
            students.Add(new Student("Tamara Vaughan",   21, 85));
            students.Add(new Student("Barry Pruitt",     20, 95));
            students.Add(new Student("Abigail Gonzales", 22, 95));

            return students;
        }
    }

    class Student : IComparable<Student>
    {
        public Student(string name, int age, int grade)
        {
            Name = name;
            Age = age;
            Grade = grade;
        }

        public string Name { get; set; }
        public int Age { get; set; }
        public int Grade { get; set; }

        public int CompareTo(Student other)
        {
            // Sort by Name, then Grade, then Age
            int result = Grade.CompareTo(other.Grade);
            if (result == 0) result = Name.CompareTo(other.Name);
            if (result == 0) result = Age.CompareTo(other.Age);
            return result;
        }

        public override string ToString()
        {
            return String.Format("Name: {0,-20} Age: {1,3:d} Grade: {2,3:d}", Name, Age, Grade);
        }
    }
}
